#!/bin/bash

echo "Executing cci-update-quota..."
message="Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL . Please check Rundeck's log."
cci-update-quota -vv --"$RD_OPTION_BEHAVIOUR" \
--instance "$RD_OPTION_INSTANCE" --ticket-number "$RD_OPTION_SNOW_TICKET" \
execute --worknote "$message" --resolver "$RD_JOB_USER_NAME"

CCI_UPDATE_EXIT=$?

if [ $CCI_UPDATE_EXIT -eq 0 ]; then
  if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
    echo "[INFO] $RD_OPTION_SNOW_TICKET worknote updated"
  else
    echo "[DRYRUN] Please verify ticket values before running it in perform mode."
  fi
fi

exit $CCI_UPDATE_EXIT