#!/bin/bash

echo "Executing cci-enable-project..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-enable-project -vv \
    --enable-project "$RD_OPTION_ENABLE_PROJECT" \
    from-snow \
        --ticket-number "$RD_OPTION_SNOW_TICKET" \
        --resolver "$RD_JOB_USER_NAME" \
        --instance "$RD_OPTION_INSTANCE"

exit $?
