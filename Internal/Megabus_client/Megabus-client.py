#!/usr/bin/python

from ConfigParser import ConfigParser
from stompest.config import StompConfig
from stompest.protocol import StompSpec
from stompest.sync import Stomp

config = ConfigParser()
config.readfp(open('@option.config_file@'))

config.get("megabus", "server") # tcp://mco1.cern.ch:61123
config.get("megabus", "user") # 'aibatchers'
config.get("megabus", "pass") # XXXXXXX
config.get("megabus", "ttl") # 172800s (1 day)

CONFIG = StompConfig(config.get("megabus", "server"), login=config.get("megabus", "user"),
                                passcode=config.get("megabus", "pass"))

QUEUE = '/queue/consumer.aibatchers.ai.hypervisor'

if __name__ == '__main__':
    client = Stomp(CONFIG)
    client.connect()
    client.subscribe(QUEUE, {StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL})
    while True:
        frame = client.receiveFrame()
        print 'Got %s' % frame.info()
        client.ack(frame)
    client.disconnect()