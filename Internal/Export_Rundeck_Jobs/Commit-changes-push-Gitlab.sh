#!/bin/bash

# Move to folder
cd "$RD_OPTION_OUTPUT" || exit

# Add new data
git config --global user.name "Rundeck"
git config --global user.email svc.rdeck@cern.ch

# setup git pull to rebase instead of merge
git config --global branch.autosetuprebase always
# Enable colors during the output of the git-diff to the terminal
git config --global color.diff auto
# Enable colors during the output of the git-status to the terminal
git config --global color.status auto
# Enable colors during the output of the git-branch to the terminal
git config --global color.branch auto
# Default push bahaviour in git 2.0
git config --global push.default simple

# Git Add & Commit
git add --no-all .
git diff --cached
git commit -m "Rundeck jobs backup $(date +'%H:%M %d/%m/%Y')"

# Push to central repo
GIT_SSH=/tmp/svccigit_ssh git push

echo "Done !"