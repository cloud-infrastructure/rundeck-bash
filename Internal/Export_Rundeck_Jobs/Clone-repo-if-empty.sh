#!/bin/bash

if [ ! -d "$RD_OPTION_OUTPUT" ]; then
  echo "There is no local copy if the repo. Trying to clone it from $RD_OPTION_GIT_REPO..."
  GIT_SSH=/tmp/svccigit_ssh git clone "$RD_OPTION_GIT_REPO" "$RD_OPTION_OUTPUT"
  cd "$RD_OPTION_OUTPUT" || exit
  echo "Creating .gitignore..."
  echo "*.zip" > .gitignore

else
  cd "$RD_OPTION_OUTPUT" || exit
  echo "The repo is already cloned. Fetching from origin."
  GIT_SSH=/tmp/svccigit_ssh git fetch --all
  git reset --hard origin/master
fi

# Remove all xml files so the jobs that do not exist in the export will be removed
git rm -r ./*.xml