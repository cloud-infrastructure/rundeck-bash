#!/bin/bash

echo "Running this flow from $HOSTNAME"
# Configure ssh script
echo "Creating the svccigit_ssh wrapper file..."
cat > /tmp/svccigit_ssh << EOF

#!/bin/bash

ssh -i /var/lib/rundeck/.ssh/svccigit_rsa -o StrictHostKeyChecking=no -p ${RD_OPTION_GIT_REPO_PORT} \$1 \$2
EOF

chmod 700 /tmp/svccigit_ssh
