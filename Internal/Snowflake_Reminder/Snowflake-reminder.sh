#!/bin/bash

echo "Executing cci-snowflake-reminder..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-snowflake-reminder -vv \
  --"$RD_OPTION_BEHAVIOUR" \
  --mm_url "$RD_OPTION_MATTERMOST_URL"


  exit $?
