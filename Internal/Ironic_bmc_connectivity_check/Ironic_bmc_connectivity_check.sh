#!/bin/bash

echo "Executing ironic_bmc_connectivity_check..."
export OS_CLOUD=ironic
cci-ironic-bmc-connectivity-check --mail-to "$RD_OPTION_MAIL_TO" -vv
exit $?
