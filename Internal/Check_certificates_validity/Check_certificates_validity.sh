#!/bin/bash

# Get the hostname and path to certificates
hostname=$RD_OPTION_HOST
cert_path=$RD_OPTION_PATH

# Check if the certificate path exists
ssh -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 root@"$hostname" "ls $cert_path > /dev/null 2>&1"
if [[ $? -ne 0 ]]; then
  echo "Certificate path '$cert_path' does not exist on the remote machine."
  exit 1
fi

# Get the list of certificate files with a .pem extension
cert_files=$(ssh -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 root@"$hostname" "ls $cert_path/*.pem 2> /dev/null")

# Check if any certificate files were found
if [[ -z "$cert_files" ]]; then
  echo "No certificate files with '.pem' extension found in '$cert_path' on the remote machine."
  exit 1
fi

# Loop through the certificate files and check their validity
for cert_file in $cert_files; do
  # Remove extra slash from the certificate path
  # shellcheck disable=SC2001
  cert_file=$(echo "$cert_file" | sed 's,//,/,g')

  # Check the certificate validity using the openssl command
  enddate=$(ssh -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 root@"$hostname" "openssl x509 -in \"$cert_file\" -noout -enddate" 2> /dev/null)

  # Check if the certificate is valid or has expired
  current_date=$(date +%s)
  expiry_date=$(date -d "${enddate#*=}" +%s)
  if [[ $current_date -gt $expiry_date ]]; then
    echo "$cert_file"
    echo "Expired"
    echo "${enddate#*=}"
  else
    echo "$cert_file"
    echo "Valid"
    echo "${enddate#*=}"
  fi
  echo # Print a blank line for better readability
done