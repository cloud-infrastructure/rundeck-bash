#!/bin/bash

echo "Running this flow from $HOSTNAME"

templates_repo="$1"
json_dir="$2"
tmp_dir="/tmp/cci-notification-templates"

git clone https://gitlab.cern.ch/cloud-infrastructure/cci-notification-templates.git $tmp_dir > /dev/null
cd $tmp_dir || exit

echo "Writting opening tag in $json_dir."
printf '[\n' > "$json_dir"

for template_name in $( git ls-tree -r master --name-only );
  do
    if [ "${template_name}" != 'README.md' ]; then
      echo "Appending $template_name to the json..."
      printf '{"name":"%s","value":"%s"},\n' "$template_name" "$templates_repo$template_name" >> "$json_dir"
    fi
  done

echo "Writting closing tag in $json_dir."
printf ']' >> "$json_dir"
echo "Deleting temporary files..."
rm -rf $tmp_dir
echo "Finished."