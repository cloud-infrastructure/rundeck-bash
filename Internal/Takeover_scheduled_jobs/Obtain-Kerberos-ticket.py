#!/usr/bin/python

import logging
import warnings
import sys

from argparse import ArgumentParser
from ccitools.common import add_logging_arguments, configure_logging, cern_get_sso_cookie
from ccitools.errors import RundeckAPIError
from ccitools.rundeck import RundeckAPIClient
from ConfigParser import ConfigParser


def main_takeover(rdeckclient):
    try:
        logging.info("Taking over all scheduled jobs...")
        print rdeckclient.takeover_schedule()
    except RundeckAPIError, err:
        logging.error(err)

def main():
    parser = ArgumentParser(description="Rundeck takeover for scheduled jobs")
    add_logging_arguments(parser)
    parser.add_argument("--auth-method", choices=['token', 'sso'])
    args = parser.parse_args()

    config = ConfigParser()
    config.readfp(open('/etc/cci/ccitools.cfg'))

    configure_logging(args)

    server = config.get("rundeck", "alias")
    port = config.get("rundeck", "port")

    logging.info("Instantiating Rundeck client...")
    if args.auth_method.lower() == 'token':
        api_key = config.get("rundeck", "api_token")
        rdeckclient = RundeckAPIClient(server, port, token=api_key)
    elif args.auth_method.lower() == 'sso':
        logging.info("Getting SSO cookie...")
        cookie = cern_get_sso_cookie("https://%s" % server, "/tmp/rundeck_sso_cookie")
        rdeckclient = RundeckAPIClient(server, port, sso_cookie=cookie)
    else:
        raise Exception("Auth method '%s' not supported" % args.auth_method)

    main_takeover(rdeckclient)


if __name__ == "__main__":
    try:
        logging.basicConfig(level=logging.INFO)
        warnings.filterwarnings("ignore")
        main()
    except Exception, e:
        logging.error(e)
        sys.exit(-1)