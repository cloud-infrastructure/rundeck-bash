#!/usr/bin/env python

import requests
import xml.etree.ElementTree as ET
import time

API_KEY='upYAyNiqqZ2oUvfglIdHkIuRerDBPhgt'

MILISECONDS_IN_THREE_MONTHS = 7889000000
TODAY = int(round(time.time() * 1000))

rundeck_servers = ['cci-rundeck-01.cern.ch', 'cci-rundeck-02.cern.ch', 'cci-rundeck-03.cern.ch']
RUNDECKSERVER = ""

def heath_check(RUNDECKSERVER):
    url = 'https://'+ RUNDECKSERVER +':4443/api/1/system/info'
    headers = {'Content-Type': 'application/json','X-RunDeck-Auth-Token': API_KEY }
    try:
        r = requests.put(url, headers=headers, verify=False)
        if r.status_code == requests.codes.ok:
                return True
    except requests.exceptions.RequestException:
        return False
    return False


# API call to get the list of the existing projects on the server.
def listProjects():
    url = 'https://'+ RUNDECKSERVER +':4443/api/1/projects'
    headers = {'Content-Type': 'application/json','X-RunDeck-Auth-Token': API_KEY }
    r = requests.get(url, headers=headers, verify=False)
    return r.text

# Returns list of all the project names
def getProjectNames(projectsinfo_xml):
    project_names = []
    root = ET.fromstring(projectsinfo_xml)
    for projects in root:
        for name in projects.findall('project'):
            project_names.append(name.find('name').text)
    return project_names

# API call to get the list of the jobs that exist for a project.
def listJobsForProject(project_name):
    url = 'https://'+ RUNDECKSERVER +':4443/api/1/jobs'
    payload = { 'project':  project_name }
    headers = {'Content-Type': 'application/json','X-RunDeck-Auth-Token': API_KEY }
    r = requests.get(url, params=payload, headers=headers, verify=False)
    return r.text

# Returns list of all the jobids
def getJobIDs(jobsinfo_xml):
    job_ids = []
    root = ET.fromstring(jobsinfo_xml)
    for jobs in root:
        for job in jobs:
            job_ids.append(job.attrib['id'])
    return job_ids

# API call to get the list of the executions for a Job.
def getExecutionsForAJob(job_id):
    url = 'https://'+ RUNDECKSERVER +':4443/api/1/job/'+job_id+'/executions'
    headers = {'Content-Type': 'application/json','X-RunDeck-Auth-Token': API_KEY }
    r = requests.get(url, headers=headers, verify=False)
    return r.text

# Returns a dict {'execution_id01': 'execution_date01', 'execution_id02': 'execution_date02', ... }
def getExecutionDate(executionsinfo_xml):
    execid_dates = {}
    root = ET.fromstring(executionsinfo_xml)
    for executions in root:
        for execution in executions.findall('execution'):
            execution_id = execution.get('id')
            for date in execution.findall('date-ended'):
                execution_date = date.get('unixtime')
	    execid_dates[execution_id] = execution_date
    return execid_dates

#API call to delete an execution by ID
def deleteExecution(execution_id):
    url = 'https://'+ RUNDECKSERVER +':4443/api/12/execution/'+execution_id
    headers = {'Content-Type': 'application/json','X-RunDeck-Auth-Token': API_KEY }
    r = requests.delete(url, headers=headers, verify=False)

def deleteAllExecution(job_id):
    url = 'https://'+ RUNDECKSERVER +':4443/api/12/job/'+ job_id +'/executions'
    headers = {'Content-Type': 'application/json','X-RunDeck-Auth-Token': API_KEY }
    r = requests.delete(url, headers=headers, verify=False)

def isOlderThan90days(execution_date, today):
    if ((today - execution_date) > MILISECONDS_IN_THREE_MONTHS):
        return True
    return False

def checkDeletion(execid_dates):
    for exec_id, exec_date in execid_dates.iteritems():
	if isOlderThan90days (int(exec_date), TODAY):
	    deleteExecution(exec_id)

#Check if there is one healthy rundeck server...
for server in rundeck_servers:
    if heath_check(server):
        RUNDECKSERVER = server
        break

projects = getProjectNames(listProjects())
for project in projects:
    jobids = getJobIDs(listJobsForProject(project))
    for jobid in jobids:
        checkDeletion(getExecutionDate(getExecutionsForAJob(jobid)))