#!/bin/bash

set -e
source /var/lib/rundeck/data/openrc

echo "[INFO] Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

echo "[INFO] Getting ID for VM '$RD_OPTION_INSTANCE' ..."
INSTANCE_ID=$(openstack server list --all-projects --name "^${RD_OPTION_INSTANCE}$" -n -c ID -f value)

if [ -z "$INSTANCE_ID" ]; then
  echo "[ERROR] No instance ID found with the name '$RD_OPTION_INSTANCE'. \
Please check that the name is correct. \
If the instance expired more than 15 days ago, it may have been deleted permanently and cannot be unexpired."
  exit 1
fi
echo "[INFO] OK"

echo "[INFO] Checking that workflow to unexpire instances is available..."
OUTPUT=$(openstack workflow show instance_expiration_global.unexpire_instance)
if [[ $OUTPUT == *"Workflow not found"* ]]; then
  echo "[ERROR] Workflow not found"
  exit 1
fi
echo "[INFO] OK"

echo "[INFO] Unexpiring instance $RD_OPTION_INSTANCE..."
CMD="openstack workflow execution create instance_expiration_global.unexpire_instance '{\"instance_id\":\"$INSTANCE_ID\"}'"
eval "$CMD"

if [ $? -ne 0 ]; then
  echo "[ERROR] Workflow execution failed"
  exit 1
fi
echo "[INFO] OK"
echo "[INFO] Instance unexpired successfully"
#NEW_DATE=$(openstack server show $INSTANCE_ID -c properties -f 'value' | awk -F= '/expire_at/{print $2}' | cut -d 'T' -f 1 | tr -d "'")

exit 0