#!/bin/bash

source /var/lib/rundeck/data/openrc

echo "[INFO] Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

echo "[INFO] Getting ID for VM '$RD_OPTION_INSTANCE' ..."
INSTANCE_ID=$(openstack server list --all-projects --name "^${RD_OPTION_INSTANCE}$" -n -c ID -f value)

if [ -z "$INSTANCE_ID" ]; then
  echo "[ERROR] No instance ID found with the name '$RD_OPTION_INSTANCE'. \
Please check that the name is correct."
  exit 1
fi
echo "[INFO] OK"

echo "[INFO] Getting console url for $RD_OPTION_INSTANCE..."
CONSOLE_URL=$(openstack console url show "${INSTANCE_ID}" -c url -f value)
ret=$?
echo "[INFO] Console URL: ""${CONSOLE_URL}"""

exit "$ret"