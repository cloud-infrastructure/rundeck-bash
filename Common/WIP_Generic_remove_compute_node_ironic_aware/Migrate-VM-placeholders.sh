#!/bin/bash

# source the openrc
echo "INFO:rundeck:Migrating VM placeholders..."
echo "INFO:rundeck:Exporting env variables..."
source /var/lib/rundeck/data/openrc

echo "INFO:rundeck:Executing embedded perl script..."

if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
  OUTPUT=$(/usr/bin/perl -x "$0" --username "$OS_USERNAME" --password "$OS_PASSWORD" --host "$RD_OPTION_HOSTS")
else
  OUTPUT=$(/usr/bin/perl -x "$0" --dryrun --username "$OS_USERNAME" --password "$OS_PASSWORD" --host "$RD_OPTION_HOSTS")
fi

# shellcheck disable=SC2217
echo -n << '__END__' > /dev/null 2>&1
#!/usr/bin/perl -wl

use strict;
use diagnostics;
use SOAP::Lite; # +trace => 'debug';
use Data::Dumper;
use Getopt::Long;

sub IsPlaceholder($);
sub Usage();

my $host = my $vm = my $dryrun = my $verbose = my $help = my $target = undef;
my $username = getlogin();
my $password = undef;

my %opts = (dryrun  => \$dryrun,
	    verbose => \$verbose,
            help    => \$help,
            host    => \$host,
            vm      => \$vm,
    );

my $options = GetOptions(\%opts,
                        "dryrun"          => \$dryrun,
                        "help"            => \$help,
                        "verbose"         => \$verbose,
			            "host"            => \$host,
			            "vm",             => \$vm,
			            "target=s"        => \$target,
                        "username=s"      => \$username,
                        "password=s"      => \$password,
    );

if (not $host and not $vm){
    Usage();
    exit 1;
}
if ($host and $vm){
    Usage();
    exit 1;
}
if (not @ARGV){
    Usage();
    exit 1;
}

#
# Verify the input
#
my @todo = ();
for my $host (map {lc} @ARGV){
    if (not gethostbyname($host)){
	print "[WARN] Ignoring non-existent server \"$host\"\n";
    }else{
	push(@todo,(split(/\./,$host))[0]);
    }
}
my @host = @todo;
if (not @host){
    print "[ERROR] No valid servers specified.\n";
    exit 1;
}

#exit;

#
#  Go for it!
############## https://network.cern.ch/sc/soap/4/description.html
#

my $client = SOAP::Lite
    -> uri('http://network.cern.ch/NetworkService')
    -> proxy('https://network.cern.ch/sc/soap/soap.fcgi?v=x');

my $call = $client->getAuthToken($username,$password,'CERN');

my $result = $call->result;
if ($call->fault) {
    print "[ERROR] Cannot authenticate against LANdb: " . $call->faultstring . "\n";;
    exit 1;
}

my $auth  = SOAP::Header->name('Auth' => { "token" => $result });

my %VMGuestList = my %ServiceName = ();

#
# which VMs are associated to these hosts?
#
if ($host){
    for my $host (sort @host){
	my $call = $client->vmGetInfo($auth,$host);
	$result = $call->result;
	my %host = %$result;
	#print Dumper(\%host);#exit;

	if ($host{IsVM} != 0){
	    print "[INFO] Machine \"$host\" is not a physical server according to LANdb, skipping it.\n";
	    next;
	}

	my @VMGuestList = @{$host{VMGuestList}};
	if (not scalar @VMGuestList){
	    print "[VERB] No VMs associated with host \"$host\", so nothing to do!\n";
	    next;
	}
	#print ">> $host  - @VMGuestList\n";

	#
	# Are the VMs placeholder entries, or genuine user-owned VMs?
	#
	my @err = ();
	for my $vm (@VMGuestList){
	    if (IsPlaceholder($vm)){
		push(@{$VMGuestList{$host}},$vm);
	    }else{
		push(@err,"Device \"$vm\" is a genuine user VM");
	    }
	}
	if (@err){
	    map {print "[ERROR] $_\n"} @err;
	    #exit;
	}

    }
}
if ($vm){
    my @err = ();
    for my $vm (@host){
	my $call = $client->vmGetInfo($auth,$vm);
	$result = $call->result;
	my %vm = %$result;
	#print Dumper(\%vm);exit;

	if ($vm{IsVM} == 0){
	    print "[INFO] Machine \"$vm\" is not a virtual machine according to LANdb, skipping it.\n";
	    next;
	}

	if (IsPlaceholder($vm)){
	    my $host = $vm{VMParent};
	    push(@{$VMGuestList{$host}},$vm);
	}else{
	    push(@err,"Device \"$vm\" is a genuine user VM");
	}
    }
    if (@err){
	map {print "[ERROR] $_\n"} @err;
	#exit;
    }
}

#
# In which IP services are our hosts located?
#
my $ServiceName = undef;

for my $host (keys %VMGuestList){
    $call = $client->getDeviceInfo($auth,$host);
    $result = $call->result;
    my %device = %$result;
    for (@{$device{Interfaces}}){
	my %interface = %$_;
	if (lc($interface{Name}) eq lc($host) . ".cern.ch"){
	    $ServiceName{$host} = $interface{ServiceName};
	    last;
	}
    }
}


if ($verbose){
    print Dumper(\%VMGuestList);
    print Dumper(\%ServiceName);
}

#
# Which other compute nodes are in the same primary IP service as our host?
#
my %target = ();
for my $host (keys %ServiceName){
    $call = $client->getDevicesFromService($auth,$ServiceName{$host});
    $result = $call->result;
	for (sort map {lc} @$result){
	    $call = $client->getDeviceInfo($auth,$_);
	    $result = $call->result;
	    next if $call->fault;
	    #print Dumper($result);#exit;
	    push(@{$target{$host}},$_) if lc($result->{ResponsiblePerson}->{Name}) eq "ai-openstack-admin";
	}
}

print Dumper(\%target) if $verbose;

for my $host (keys %VMGuestList){
    undef my $done;
    for my $vm (@{$VMGuestList{$host}}){
	for my $target (@{$target{$host}}){ # try any target, until we find out that works :)
	    next if grep {$target eq $_} keys %ServiceName; # do not try to use one of our input hosts :)
	    if ($dryrun){
		$done = "[DRYRUN] Would migrate \"$vm\" to \"$target\"";
		last;
	    }
	    my $call = $client->vmMigrate($auth,$vm,$target);
	    $result = $call->result;
	    if ($call->fault) {
		print "[ERR] Could not migrate Virtual Machine \"$vm\" to compute node \"$target\": " . $call->faultstring . "\n";
	    }else{
		$done = "Virtual Machine \"$vm\" migrated to compute node \"$target\"";
		last;
	    }
	}
	if ($done){
	    print "[INFO] $done\n";
	}else{
	    print "[ERR] Could not vmMigrate \"$vm\"\n";
	}
    }
}

exit 0;

sub IsPlaceholder($){
    my $name = shift(@_);
    #return 1; # XXX
    return 0 if $name !~ /^z\S{10}$/i;

    return 1;
}

sub Usage(){
    print STDOUT <<EOF;

Usage: $0 [--dryrun] [--verbose] [--username <username>] [--password <password>] [--target <server name>] --host <server name> [<server name>]
       $0 [--dryrun] [--verbose] [--username <username>] [--password <password>] [--target <server name>] --vm <virtual machine name> [<virtual machine name>]

EOF
    return 0
}

__END__

if [ -n "$OUTPUT" ]; then
    echo "$OUTPUT"
    echo
else
  echo "[ERROR] Perl script returned empty output. Please check logs"
  exit 1
fi
