#!/bin/bash

echo "EXECUTING MIGRATION CYCLE"

params=()

if [ -n "$RD_OPTION_SKIP_VMS_DISK_SIZE" ]
then
   params+=("--skip-vms-disk-size $RD_OPTION_SKIP_VMS_DISK_SIZE")
fi

if [ -n "$RD_OPTION_EXCLUSIVE_VMS_LIST" ]
then
   params+=("--exclusive-vms-list $RD_OPTION_EXCLUSIVE_VMS_LIST")
fi

if [ -n "$RD_OPTION_SKIP_VMS_LIST" ]
then
   params+=("--skip-vms-list $RD_OPTION_SKIP_VMS_LIST")
fi

if [ -n "$RD_OPTION_TARGET_COMPUTE_NODE" ]
then
   params+=("--target-compute-node $RD_OPTION_TARGET_COMPUTE_NODE")
fi

if [ "$RD_OPTION_FORCE_COLD" == "true" ]
then
   params+=("--force-cold")
fi

if [ -n "$RD_OPTION_ABORT_ON_IOWAIT" ]
then
   params+=("--abort-on-iowait $RD_OPTION_ABORT_ON_IOWAIT")
fi

if [ -n "$RD_OPTION_IOWAIT_THRESHOLD" ]
then
   params+=("--iowait-threshold $RD_OPTION_IOWAIT_THRESHOLD")
fi

echo "--hosts $RD_OPTION_HOSTS --cloud $RD_OPTION_CLOUD --stop_at_migration_failure False --power-operation $RD_OPTION_POWER_OPERATION --compute-enable $RD_OPTION_COMPUTE_ENABLE --roger-enable $RD_OPTION_ROGER_ENABLE --disable-reason \"$RD_OPTION_DISABLE_REASON\" --skip-shutdown-vms $RD_OPTION_SKIP_SHUTDOWN_VMS --skip-disabled-compute-nodes $RD_OPTION_SKIP_DISABLED_COMPUTE_NODES --max-threads $RD_OPTION_PARALLEL_COMPUTE_NODES --no-logfile --skip-large-vm-node $RD_OPTION_SKIP_LARGE_VM_NODE --abort-on-ping-loss $RD_OPTION_ABORT_ON_PING_LOSS --ping-loss-threshold $RD_OPTION_PING_LOSS_THRESHOLD --abort-on-ping-latency $RD_OPTION_ABORT_ON_PING_LATENCY --ping-latency-threshold $RD_OPTION_PING_LATENCY_THRESHOLD   ${params[*]}"

# shellcheck disable=SC2068
/usr/bin/migration_cycle --hosts "$RD_OPTION_HOSTS" --cloud "$RD_OPTION_CLOUD" --stop_at_migration_failure "False" --power-operation "$RD_OPTION_POWER_OPERATION" --compute-enable "$RD_OPTION_COMPUTE_ENABLE" --disable-reason "$RD_OPTION_DISABLE_REASON" --skip-shutdown-vms "$RD_OPTION_SKIP_SHUTDOWN_VMS" --skip-disabled-compute-nodes "$RD_OPTION_SKIP_DISABLED_COMPUTE_NODES" --max-threads "$RD_OPTION_PARALLEL_COMPUTE_NODES" --roger-enable "$RD_OPTION_ROGER_ENABLE" --no-logfile --skip-large-vm-node "$RD_OPTION_SKIP_LARGE_VM_NODE" --abort-on-ping-loss "$RD_OPTION_ABORT_ON_PING_LOSS" --ping-loss-threshold "$RD_OPTION_PING_LOSS_THRESHOLD" --abort-on-ping-latency "$RD_OPTION_ABORT_ON_PING_LATENCY" --ping-latency-threshold "$RD_OPTION_PING_LATENCY_THRESHOLD" ${params[@]}
ret=$?

echo "MIGRATION CYCLE FINISHED"

exit "$ret"
