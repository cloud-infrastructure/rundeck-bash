#!/bin/bash

echo "Executing cci-get-hosts-uptime..."
echo "hosts : $RD_OPTION_HOSTS"
cci-get-hosts-uptime --hosts "$RD_OPTION_HOSTS" --"$RD_OPTION_BEHAVIOUR" -v
exit $?