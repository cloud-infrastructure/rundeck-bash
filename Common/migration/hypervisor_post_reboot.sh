#!/bin/bash

echo "Executing cci-hypervisor-state-post-reboot..."
echo "hosts : $RD_OPTION_HOSTS"
#echo "hosts uptims : $RD_OPTION_HOSTS_UPTIME"
echo "uptime : @data._@"
cci-hypervisor-state-post-reboot --hypervisor "$RD_OPTION_HOSTS" --"$RD_OPTION_BEHAVIOUR" --uptime "@data._@" -vv
exit $?
