#!/bin/bash

ERR=0
for HOST in $RD_OPTION_HOSTS
do
    RESULT=$(cci-hypervisor-has-vms --hypervisors "$HOST" 2>&1)
    if [[ $RESULT == *"does not have VMs in any region"* ]]; then
        echo "Compute node $HOST: migration operations successful"
    else
        ERR=$((ERR+1))
        echo "compute node $HOST still has Vms: $RESULT"
    fi
done

if [ $ERR -gt 0 ]; then
    exit 127
fi
