#!/usr/bin/env bash

#set -x

HOSTNAME=$(echo "${RD_OPTION_HOSTNAME}" | xargs -n1 | cut -f1 -d. | tr '[:upper:]' '[:lower:]')
OS=${RD_OPTION_OS:-rhel9}
CUSTOM_IMAGE=${RD_OPTION_CUSTOM_IMAGE:-none}
FORCE_RAID=${RD_OPTION_FORCE_RAID:-false}
ADDITIONAL_PUPPET_RUN=${RD_OPTION_ADDITIONAL_PUPPET_RUN:-false}
ADDITIONAL_REBOOT=${RD_OPTION_ADDITIONAL_REBOOT:-false}
CHECK_EMPTY=${RD_OPTION_CHECK_EMPTY:-false}
FORCE_HOST=${RD_OPTION_FORCE_HOST:-false}
ROGER_STATE=${RD_OPTION_ROGER_STATE:-false}
USERNAME=${RD_OPTION_USERNAME:-svcrdeck}
IGNORE_SSH_VM_ERROR=${RD_OPTION_IGNORE_SSH_VM_ERROR:-false}

export OS_CLOUD=cern
export OS_PLACEMENT_API_VERSION=1.22
export OS_COMPUTE_API_VERSION=2.79
export OS_IDENTITY_API_VERSION=3

if [ "$OS" == "custom" ] ; then
    IMAGE=$CUSTOM_IMAGE
else
    IMAGE=$OS
fi

echo
echo "Reinstalling $HOSTNAME ... - $(date) with $IMAGE"
echo "This will take up to several hours, so please be patient :)"

# get the UUID of the server
ID=$(openstack server list -n --all --no-name-lookup --name "^${HOSTNAME}$" -f value -c ID)
FLAVOR_NAME=$(openstack server show "${ID}" -f json -c flavor | jq -r '.flavor.original_name')
FLAVOR=$(openstack flavor show "${FLAVOR_NAME}" -f value -c id)

# get and log all server details from nova
echo
echo "Getting server details ..."
SHOW=$(openstack server show --format yaml "${ID}")
echo "${SHOW}"

# get the tenant name and id from the properties
PROJECT_ID=$(openstack server show -f value -c project_id "${ID}")
PROJECT_NAME=$(openstack project show "$PROJECT_ID" -f value -c name)

# get the RESOURCE_CLASS from the FLAVOR
X=$(openstack server show "${ID}" -f json -c flavor | jq -r '.flavor.extra_specs' | grep CUSTOM_BAREMETAL | cut -d':' -f2 | sed 's/"//g')
RE="(CUSTOM_BAREMETAL.*)"
[[ $X =~ $RE ]] && RESOURCE_CLASS=${BASH_REMATCH[1]}

# get the underlying physical node from ironic
echo
echo "Getting the physical node ..."
export OS_PROJECT_NAME="Cloud Test - gva_baremetal_001"
LIST=$(openstack baremetal node list --limit 0 -f csv --quote minimal|grep "${ID}")
NODE_UUID=$(echo "${LIST}"|cut -d, -f1)

NODE=$(echo "${LIST}"|cut -d, -f2)
# get the host group and the environment from ai-dump
echo
echo "Getting the host group and the environment ..."
X=$(ai-foreman showhost "${HOSTNAME}.cern.ch" | grep "$HOSTNAME")
HOSTGROUP=$(echo "$X" | cut -d'|' -f3|xargs)
ENVIRONMENT=$(echo "$X" | cut -d'|' -f4|xargs)

# print all vars for potential reuse
echo
echo "export OS_CLOUD=cern"
echo "export OS_PLACEMENT_API_VERSION=1.22"
echo "export OS_COMPUTE_API_VERSION=2.79"
echo "export OS_IDENTITY_API_VERSION=3"
echo "export OS_PROJECT_NAME=\"Cloud Test - gva_baremetal_001\""
echo
echo "export HOSTNAME=$HOSTNAME"
echo "export ID=$ID"
echo "export FLAVOR=$FLAVOR"
echo "export FLAVOR_NAME=$FLAVOR_NAME"
echo "export PROJECT_NAME=\"$PROJECT_NAME\""
echo "export PROJECT_ID=$PROJECT_ID"
echo "export RESOURCE_CLASS=$RESOURCE_CLASS"
echo "export NODE=$NODE"
echo "export NODE_UUID=$NODE_UUID"
echo "export HOSTGROUP=$HOSTGROUP"
echo "export ENVIRONMENT=$ENVIRONMENT"
echo "export OS=$OS"
echo "export CUSTOM_IMAGE=$CUSTOM_IMAGE"
echo "export FORCE_RAID=$FORCE_RAID"
echo "export ADDITIONAL_PUPPET_RUN=$ADDITIONAL_PUPPET_RUN"
echo "export ADDITIONAL_REBOOT=$ADDITIONAL_REBOOT"
echo "export CHECK_EMPTY=$CHECK_EMPTY"
echo "export FORCE_HOST=$FORCE_HOST"
echo "export ROGER_STATE=$ROGER_STATE"
echo "export USERNAME=$USERNAME"
echo ""

# some simple sanity checking
if [ -z "$ID" ] || \
   [ -z "$FLAVOR" ] || \
   [ -z "$FLAVOR_NAME" ] || \
   [ -z "$PROJECT_NAME" ] || \
   [ -z "$PROJECT_ID" ] || \
   [ -z "$RESOURCE_CLASS" ] || \
   [ -z "$NODE" ] || \
   [ -z "$NODE_UUID" ] || \
   [ -z "$HOSTGROUP" ] || [ "$HOSTGROUP" == "-" ] || \
   [ -z "$ENVIRONMENT" ] || \
   [ -z "$CUSTOM_IMAGE" ] || \
   [ -z "$OS" ] || [[ "$OS" == "custom" && -z "$CUSTOM_IMAGE" ]] || [[ "$OS" == "custom" && "$CUSTOM_IMAGE" == "none" ]] || \
   [ -z "$FORCE_RAID" ] || \
   [ -z "$ADDITIONAL_PUPPET_RUN" ] || \
   [ -z "$ADDITIONAL_REBOOT" ] || \
   [ -z "$CHECK_EMPTY" ] || \
   [ -z "$FORCE_HOST" ] || \
   [ -z "$ROGER_STATE" ] || \
   [ -z "$USERNAME" ]; then
    echo "One or more variables are undefined or have invalid values"
    exit 1
fi

if [ "$CUSTOM_IMAGE" == "none" ] ; then
    IMAGE_BOOT=("--${OS}")
else
    IMAGE_BOOT=(--nova-image "${CUSTOM_IMAGE}")
fi

if [ "$CHECK_EMPTY" == "true" ] ; then
    echo "Checking if the server has allocated machines in the OS API"
    OPENSTACK_VMS=$(openstack server list -n --all --no-name-lookup --host "${HOSTNAME}.cern.ch" -c ID -f value | wc -l)
    echo "The server has $OPENSTACK_VMS instances in Nova"

    echo "Checking if the server has allocated machines in the hypervisor"
    HOST_VMS=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOSTNAME" "virsh list --all --uuid | sed '/^[[:space:]]*$/d' | wc -l")
    if [ $? -ne 0 ] && [ "$IGNORE_SSH_VM_ERROR" == "false" ]; then
        echo "It was not possible to ssh to the node to verify there are no VMs present. You can set IGNORE_SSH_VM_ERROR to true to skip this error."
        exit 2
    fi
    echo "The server has $HOST_VMS instances locally in the hypervisor"

    if [[ $OPENSTACK_VMS -gt 0 || $HOST_VMS -gt 0 ]]; then
        echo "The server is not empty. Aborting the reinstallation to avoid data loss"
        exit 2
    fi
fi

# Unsetting maintenance flag
echo "Unsetting node from maintenance"
openstack baremetal node maintenance unset "$NODE"

# Retrieve the region and cell based in the hostgroup of the instance
REGION=$(echo "$HOSTGROUP" | cut -d/ -f3)
CELL=$(echo "$HOSTGROUP" | cut -d/ -f4)
SUBCELL=$(echo "$HOSTGROUP" | cut -d/ -f5)

# Disable alarms to avoid unexpected no_contact
roger update --all_alarms=false --appstate=intervention "$HOSTNAME"

# In cells with bonding, shutoff the machine first and wait some time.
# Doing cleaning straightaway seem too fast for the "force-up" behaviour
# in the switch to kick-in.
if [ "$CELL" = "crit_project_004" ] ; then
    export OS_PROJECT_NAME="admin"
    echo
    echo "$HOSTNAME uses network bonding. Stopping the node before re-installing..."

    WAIT=0
    nova_server_status="$(openstack server show "${ID}" --column status --format value)"
    if [ "${nova_server_status}" != "SHUTOFF" ]; then
        openstack server stop "${ID}"
    fi
    while [ "${nova_server_status}" != "SHUTOFF" ]; do
        echo "Waiting for $HOSTNAME since $WAIT seconds to become SHUTOFF"
        echo "Current status is ${nova_server_status}"
        if [[ "${nova_server_status}" == "ERROR" ]]; then
            echo "Server $HOSTNAME is in ${nova_server_status} state:"
            openstack server show "${ID}"
        fi
        echo "Checking again in 60 seconds ..."
        sleep 60
        ((WAIT=WAIT+60))
        nova_server_status="$(openstack server show "${ID}" --column status --format value)"
    done
    export OS_PROJECT_NAME="Cloud Test - gva_baremetal_001"
fi



# Deleting node
echo "Deleting instance from OpenStack ..."
OS_PROJECT_NAME="admin" openstack server delete "${ID}"

# wait after the initial cleaning
echo
echo "Wait for $NODE to become available after initial cleaning ..."
WAIT=0
provision_state="$(openstack baremetal node show "$NODE" --fields provision_state --format value)"
while [ "${provision_state}" != "available" ]; do
    echo "Waiting for $NODE since $WAIT seconds to become available after initial cleaning"
    echo "Current state is ${provision_state}"
    echo "Checking again in 30 seconds ..."
    sleep 30
    ((WAIT=WAIT+30))
    provision_state="$(openstack baremetal node show "$NODE" --fields provision_state --format value)"
done

# check if the node has a RAID config, and apply a default if missing
echo
echo -n "Checking for RAID config ... "
RAID_CONFIG=$(openstack baremetal node show "$NODE" --fields target_raid_config --format value)
echo "$RAID_CONFIG"
if [ "$RAID_CONFIG" == '{}' ] || [ "$FORCE_RAID" == "true" ] ; then
    echo "Configuring software RAID for node"
    openstack baremetal node set --raid-interface agent "$NODE"

    if [ "$REGION" = "main" ] ; then
        # root RAID5, var RAID5 for gp031
        # root RAID1, var RAID10 for gs012, gs013, gs014, gs018, gs019, gs020, gp037, gp038, gp039, gp040, gp047_20cpus, gp048, cp003_dl7336860
        # root RAID1, var RAID1 for others
        if [ "$CELL" = "gva_project_031" ] ; then
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "5"}]}' "$NODE"
        elif [ "$CELL" = "crit_project_003" ] && [ "$SUBCELL" = "dl7336860" ]; then
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "1+0"}]}' "$NODE"
        elif [ "$CELL" = "gva_project_047" ] && [ "$SUBCELL" = "gva_project_047_20cpus" ]; then
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "1+0"}]}' "$NODE"
        elif [ "$CELL" = "gva_project_037" ] || [ "$CELL" = "gva_project_038" ] || [ "$CELL" = "gva_project_039" ] || [ "$CELL" = "gva_project_040" ] || \
             [ "$CELL" = "gva_project_048" ] || \
             [ "$CELL" = "gva_shared_012" ] || [ "$CELL" = "gva_shared_013" ] || [ "$CELL" = "gva_shared_014" ] || \
             [ "$CELL" = "gva_shared_018" ] || [ "$CELL" = "gva_shared_019" ] || [ "$CELL" = "gva_shared_020" ]; then
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "1+0"}]}' "$NODE"
        elif [ "$CELL" = "crit_project_004" ] ; then
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100,"controller": "software","raid_level": "1","physical_disks": [{"size": "< 3000"},{"size": "< 3000"}]}, {"size_gb": "MAX","controller": "software","raid_level": "1","physical_disks": [{"size": "< 3000"}, {"size": "< 3000"}]}]}' "$NODE"
        elif [ "$CELL" = "gva_project_046" ] ; then
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100,"controller": "software","raid_level": "1","physical_disks": [{"size": "< 1000"},{"size": "< 1000"}]}, {"size_gb": "MAX","controller": "software","raid_level": "1","physical_disks": [{"size": "< 1000"}, {"size": "< 1000"}]}]}' "$NODE"
        else
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "1"}]}' "$NODE"
        fi
    elif [ "$REGION" = "poc" ] ; then
        if [ "$CELL" = "poc_shared_001" ] ; then
            # We don't use raid on the poc_shared_001 cell in poc region
            NO_RAID=1
            openstack baremetal node set --target-raid-config '{}' "$NODE"
        else
            openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "1"}]}' "$NODE"
        fi
    else
        openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "1"}]}' "$NODE"
    fi

    echo
    echo "The node is ready for the RAID setup ..."

    # configure the RAID via cleaning
    echo
    echo "Send node into cleaning to trigger the RAID setup ..."
    openstack baremetal node maintenance unset "$NODE"
    openstack baremetal node manage "$NODE"
    openstack baremetal node provide "$NODE"

    # wait after RAID setup
    echo
    echo "Wait for $NODE to become available after RAID setup..."
    WAIT=0
    provision_state="$(openstack baremetal node show "$NODE" --fields provision_state --format value)"
    while [ "${provision_state}" != "available" ]; do
        echo "Waiting for $NODE since $WAIT seconds to become available after RAID setup"
        echo "Current state is ${provision_state}"
        echo "Checking again in 30 seconds ..."
        sleep 30
        ((WAIT=WAIT+30))
        provision_state="$(openstack baremetal node show "$NODE" --fields provision_state --format value)"
    done
fi

# wait for the allocation candidate to appear
echo
echo -n "Waiting for the allocation candidate to appear ..."
echo
WAIT=0
ALLOCATION_CANDIDATES="$(openstack allocation candidate list --resource "${RESOURCE_CLASS}"=1 -f value -c 'resource provider')"
while [[ ! "${ALLOCATION_CANDIDATES}" =~ ${NODE_UUID} ]]; do
    echo "Waiting allocation candidate ${NODE_UUID}/${RESOURCE_CLASS} since $WAIT seconds"
    echo "Checking again in 30 seconds ..."
    sleep 30
    ((WAIT=WAIT+30))
    ALLOCATION_CANDIDATES="$(openstack allocation candidate list --resource "${RESOURCE_CLASS}"=1 -f value -c 'resource provider')"
done

# wait for the server to be removed from DNS
while [[ $(host "$HOSTNAME") = *"has address"*  ]]; do
    echo "Waiting for instance to disappear from DNS - $(date)"
    sleep 30
done

echo
echo "$HOSTNAME from $HOSTGROUP is now ready for recreation."

# add svcrdeck to project and change to the project
OS_PROJECT_NAME="admin" openstack role add --user "$USERNAME" --project "$PROJECT_ID" node_selector
export OS_PROJECT_NAME="$PROJECT_NAME"

# remove from foreman
ai-kill --nova-disable "$HOSTNAME"

# prepare the user data
if [ -z "$NO_RAID" ]; then

tmpdir=$(mktemp -d -t ironic_raid.XXXXXX)
cat > "$tmpdir"/x-shellscript << 'EOF'
#!/bin/bash

# Don't hide grub loader menu
grub2-editenv - unset menu_auto_hide

# Some of the recent images come with an ens3 network interface
# This prevents the restart of networking after first configuration
if [ -f "/etc/sysconfig/network-scripts/ifcfg-ens3" ]; then
  echo "Removing unused network interface"
  rm -f /etc/sysconfig/network-scripts/ifcfg-ens3
fi

# create /var/lib/nova
#
mount_point=/var/lib/nova
mkdir -p $mount_point
# find the unmounted md device
md_devices=$(grep md /proc/mdstat|cut -d " " -f 1|xargs)
for d in $md_devices; do
    if ! grep -q $d /proc/mounts; then
        break
    fi
done
echo "found unmounted device $d"
# create an fs and mount the device
mkfs.xfs -L var_lib_nova /dev/$d
if [ $? -eq 0 ]; then
  echo "Successfully formatted the partition"
else
  echo "Could not format the partition, existing filesystem, continuing..."
fi
echo "LABEL=var_lib_nova $mount_point xfs defaults 0 0" >> /etc/fstab
mount -a
# create a swap space
#
dd if=/dev/zero of=/swapfile bs=1M count=20480
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
EOF

else

tmpdir=$(mktemp -d -t ironic_no_raid.XXXXXX)
cat > "$tmpdir"/x-shellscript << 'EOF'
#!/bin/bash

# Don't hide grub loader menu
grub2-editenv - unset menu_auto_hide

# Some of the recent images come with an ens3 network interface
# This prevents the restart of networking after first configuration
if [ -f "/etc/sysconfig/network-scripts/ifcfg-ens3" ]; then
  echo "Removing unused network interface"
  rm -f /etc/sysconfig/network-scripts/ifcfg-ens3
fi

# create a swap space
#
dd if=/dev/zero of=/swapfile bs=1M count=20480
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
EOF

fi

# re-create the server
echo
export OS_PROJECT_ID=$PROJECT_ID
EXTRA_ARGS=""
if [ "$FORCE_HOST" == "true" ] ; then
    EXTRA_ARGS=(--nova-extra-arg hypervisor_hostname="$NODE_UUID")
fi
ai-bs --nova-flavor "$FLAVOR_NAME" \
    --landb-mainuser ai-openstack-admin \
    --landb-responsible ai-openstack-admin \
    --foreman-environment "$ENVIRONMENT" \
    --foreman-hostgroup "$HOSTGROUP" \
    --no-landb-ipv6ready \
    --userdata-dir "$tmpdir" \
    "${IMAGE_BOOT[@]}" "${EXTRA_ARGS[@]}" \
    "$HOSTNAME"

# rm -f $tmpfile

# wait for the server to be ACTIVE
echo
echo "Wait for $HOSTNAME to be ACTIVE after ai-bs..."
WAIT=0
nova_server_status="$(openstack server show "$HOSTNAME" --column status --format value)"
while [ "${nova_server_status}" != "ACTIVE" ]; do
    echo "Waiting for $HOSTNAME since $WAIT seconds to become ACTIVE after ai-bs"
    echo "Current status is ${nova_server_status}"
    if [[ "${nova_server_status}" == "ERROR" ]]; then
        echo "Server $HOSTNAME is in ${nova_server_status} state:"
        openstack server show "$HOSTNAME"
    fi
    echo "Checking again in 60 seconds ..."
    sleep 60
    ((WAIT=WAIT+60))
    nova_server_status="$(openstack server show "$HOSTNAME" --column status --format value)"
done

# wait for the server to come up
while [[ $(host "$HOSTNAME") = *"not found"*  ]]; do
    echo "Waiting for instance to appear in DNS - $(date)"
    sleep 30
done

UNREACHEABLE=1
while [ $UNREACHEABLE -ne "0" ]; do
    ping -q -c 1 -W 1 "$HOSTNAME" &> /dev/null
    UNREACHEABLE=$?
    echo "Waiting for $HOSTNAME to become up - $(date)"
    sleep 30
done

# wait until the machine can be accessed through SSH
SSH_READY=1
while [ $SSH_READY -ne "0" ]; do
    klist
    kinit -R

    # check if the machine is ready for SSH
    ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOSTNAME" 'hostname'
    SSH_READY=$?
    echo "Waiting for machine to be accessible through SSH - $(date)"
    sleep 30
done

# Execute an additional puppet run to finalize the configuration
if [ "$ADDITIONAL_PUPPET_RUN" == "true" ] ; then
    # wait until puppet has finished to run it once more in interactive mode
    PUPPET_LOCK_EXISTS=0
    while [ $PUPPET_LOCK_EXISTS -eq "0" ]; do
        # check if puppet has finished
        ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOSTNAME" 'find /opt/puppetlabs/puppet/cache/state/agent_catalog_run.lock &> /dev/null'
        PUPPET_LOCK_EXISTS=$?
        echo "Waiting for machine to finish with puppet - $(date)"
        sleep 10
    done
    # Run puppet manually
    echo "Running additional puppet run - $(date)"
    ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOSTNAME" 'puppet agent -tv'
fi

# Execute an additional reboot to finalize the configuration
if [ "$ADDITIONAL_REBOOT" == "true" ] ; then
    # Rebooting the machine one more time
    echo "Run an additional reboot on the machine"
    ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOSTNAME" 'reboot'

    REACHEABLE=0
    while [ $REACHEABLE -ne "1" ]; do
        ping -q -c 1 -W 1 "$HOSTNAME" &> /dev/null
        REACHEABLE=$?
        echo "Waiting for $HOSTNAME to become down once more - $(date)"
        sleep 30
    done

    UNREACHEABLE=1
    while [ $UNREACHEABLE -ne "0" ]; do
        ping -q -c 1 -W 1 "$HOSTNAME" &> /dev/null
        UNREACHEABLE=$?
        echo "Waiting for $HOSTNAME to become up after additional reboot - $(date)"
        sleep 30
    done
fi

# Set the roger values at the end of the installation
if [ "$ROGER_STATE" == "true" ] ; then
    # Rebooting the machine one more time
    echo "Enable alarms and set roger state to production after reinstallation"
    roger update --all_alarms=true --appstate=production "$HOSTNAME"
fi

echo "Done"
