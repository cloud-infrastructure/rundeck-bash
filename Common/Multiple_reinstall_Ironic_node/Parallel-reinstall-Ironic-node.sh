#!/bin/bash

cci-multiple-reinstall-ironic-parallel -vv --auth sso \
--delay "$RD_OPTION_DELAY" \
--parallel "$RD_OPTION_PARALLEL" \
--job-id 11ff6d56-b984-4b85-afe7-2983c0b03317 \
--hosts "$RD_OPTION_HOSTS" \
--os "$RD_OPTION_OS" \
--force_raid "$RD_OPTION_FORCE_RAID" \
--additional_puppet_run "$RD_OPTION_ADDITIONAL_PUPPET_RUN" \
--additional_reboot "$RD_OPTION_ADDITIONAL_REBOOT" \
--check_empty "$RD_OPTION_CHECK_EMPTY" \
--force_host "$RD_OPTION_FORCE_HOST"