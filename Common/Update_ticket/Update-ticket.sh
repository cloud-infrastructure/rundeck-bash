#!/bin/bash

if [ -z "${RD_OPTION_SNOW_TICKET}" ]; then
  echo "[INFO] No ticket number provided, ticket not updated"
  exit 0
fi

if [ -n "$RD_OPTION_SNOW_TICKET" ]; then
  if [[ ${RD_OPTION_BEHAVIOUR} == 'dryrun' ]]; then
    echo "[INFO] DRYRUN MODE: Ticket not updated"
  else
    if [[ $RD_OPTION_SNOW_TICKET == INC* ]]; then
      if [[ $RD_OPTION_INSTANCE != 'cern' ]]; then
        echo "[INFO] Ticket: https://cerntest.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
      else
        echo "[INFO] Ticket: https://cern.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
      fi
    elif [[ $RD_OPTION_SNOW_TICKET == RQF* ]]; then
      if [[ $RD_OPTION_INSTANCE != 'cern' ]]; then
        echo "[INFO] Ticket: https://cerntest.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
      else
        echo "[INFO] Ticket: https://cern.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
      fi
    else
      exit 0 # not a Service-Now Ticket
    fi

    if [ -n "$RD_OPTION_COMMENT" ]; then
      echo "[INFO] Trying to add a new comment to $RD_OPTION_SNOW_TICKET..."
      if cci-update-ticket --ticket-number "$RD_OPTION_SNOW_TICKET" --comment "$RD_OPTION_COMMENT" --instance "$RD_OPTION_INSTANCE"; then
        echo "[INFO] $RD_OPTION_SNOW_TICKET updated"
      else
        echo "[ERROR] $RD_OPTION_SNOW_TICKET NOT updated"
        exit 1
      fi
    fi

    if [ -n "$RD_OPTION_WORKNOTE" ]; then
      echo "[INFO] Trying to add a new worknote to $RD_OPTION_SNOW_TICKET..."
      if cci-update-ticket --ticket-number "$RD_OPTION_SNOW_TICKET" --worknote "$RD_OPTION_WORKNOTE" --instance "$RD_OPTION_INSTANCE"; then
        echo "[INFO] $RD_OPTION_SNOW_TICKET updated"
      else
        echo "[ERROR] $RD_OPTION_SNOW_TICKET NOT updated"
        exit 1
      fi
    fi
  fi
fi
