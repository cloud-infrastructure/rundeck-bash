#!/bin/bash

NOTIFY_OPTIONS=""
intervention=""

# Convert the list of projects into a comma separated string
eval set -- "$RD_OPTION_PROJECTS"
for project in "$@"
do
  if [ -n "$PROJECTS" ]; then
      PROJECTS="$PROJECTS,$project"
  else
      PROJECTS=$project
  fi
done

if [[ ${RD_OPTION_NOTIFY_MEMBERS} == 'yes' ]]; then
  NOTIFY_OPTIONS+="--notify-members "
fi

if [[ ${RD_OPTION_NOTIFY_MAIN_USER} == 'yes' ]]; then
  NOTIFY_OPTIONS+="--notify-main-user "
fi

if [ -z "$RD_OPTION_OTG" ]; then
  RD_OPTION_OTG=""
fi

if [[ -n "$NOTIFY_OPTIONS" ]]; then
  printf -v intervention '{"nsc":"%s","hypervisors":"%s","projects":"%s","vms":"%s","otg":"%s","date":"%s","duration":"%s","new_date":"%s"}' "$RD_OPTION_NSC" "$RD_OPTION_HYPERVISORS" "$PROJECTS" "$RD_OPTION_VMS" "$RD_OPTION_OTG" "$RD_OPTION_DATES" "$RD_OPTION_DURATION" "$RD_OPTION_NEW_DATE"
  echo "Executing cci-notify-intervention..."
  echo "$intervention"
  # shellcheck disable=SC2086
  cci-notify-intervention -vv --"$RD_OPTION_BEHAVIOUR" --intervention "${intervention}" $NOTIFY_OPTIONS --body "$RD_OPTION_BODY" --subject "$RD_OPTION_SUBJECT"
  exit $?
else
  echo "NOT sending nofitication to users..."
fi
