#!/bin/bash

echo "Executing cci-lock-unlock-hv-servers..."
cci-lock-unlock-hv-servers --"$RD_OPTION_BEHAVIOUR" --action "$RD_OPTION_ACTION" \
--hypervisors "$RD_OPTION_HYPERVISORS"
exit $?