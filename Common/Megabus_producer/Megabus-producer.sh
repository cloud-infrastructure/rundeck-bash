#!/bin/bash

echo "Executing cci-megabus-producer..."
cci-megabus-producer --"$RD_OPTION_BEHAVIOUR" \
--intervention-time "$RD_OPTION_DATE" \
--duration "$RD_OPTION_DURATION" --hosts "$RD_OPTION_HOSTS" \
--version "$RD_OPTION_VERSION" --type "$RD_OPTION_TYPE" \
--cancel_intervention "$RD_OPTION_CANCEL_INTERVENTION" -vv
exit $?
