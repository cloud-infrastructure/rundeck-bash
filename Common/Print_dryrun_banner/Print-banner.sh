#!/bin/bash

if [[ ${RD_OPTION_BEHAVIOUR} == 'dryrun' ]]; then

  echo "***********************************"
  echo "     _                             "
  echo "  __| |_ __ _   _ _ __ _   _ _ __  "
  echo " / _  |  __| | | |  __| | | |  _ \ "
  echo "| (_| | |  | |_| | |  | |_| | | | |"
  echo " \__,_|_|   \__, |_|   \__,_|_| |_|"
  echo "            |___/                  "
  echo "***********************************"

fi