#!/bin/bash

# convert date to a valid input for "date -d" ie. American format + "/"
IFS='-' read -r -a myarray <<< "$RD_OPTION_DATE"
RD_OPTION_DATE="${myarray[1]}/${myarray[0]}/${myarray[2]}"

current_epoch=$(date +%s)
target_epoch=$(date -d "$RD_OPTION_DATE" +%s)

COMMAND="shutdown"

sleep_seconds=$((target_epoch - current_epoch))
echo "This job will sleep for $sleep_seconds seconds (until $RD_OPTION_DATE)"
sleep $sleep_seconds
echo "Waking up..!"

for HOST in $RD_OPTION_HOSTS
  do
     echo "Executing command $COMMAND on $HOST..."
     ssh -q -o StrictHostKeyChecking=no root@"$HOST" "$COMMAND"
  done