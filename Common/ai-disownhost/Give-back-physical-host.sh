#!/bin/bash

source /var/lib/rundeck/data/openrc

echo "INFO:rundeck:Executing ai-disownhost for the following hosts $RD_OPTION_HOSTS..."

RD_OPTION=''

if [ "${RD_OPTION_BEHAVIOUR}" == 'dryrun' ]; then
  RD_OPTION='--dryrun'
fi

# shellcheck disable=SC2086
ai-disownhost $RD_OPTION --roger-disable --owner "$RD_OPTION_OWNER" --hostgroup "$RD_OPTION_HOSTGROUP" --landb-pass "$OS_PASSWORD" $RD_OPTION_HOSTS
rc=$?
if [ $rc -ne 0 ]; then
  echo "ERROR:rundeck:ai-disownhost did not finish successfully: $rc"
  exit 1
fi
