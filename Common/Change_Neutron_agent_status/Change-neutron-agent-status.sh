#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes really long
export OS_REGION_NAME=cern
NEUTRON_AGENT_LIST_CERN="$(neutron agent-list)"
export OS_REGION_NAME=next
NEUTRON_AGENT_LIST_NEXT="$(neutron agent-list)"
export OS_REGION_NAME=poc
NEUTRON_AGENT_LIST_POC="$(neutron agent-list)"
export OS_REGION_NAME=pdc
NEUTRON_AGENT_LIST_PDC="$(neutron agent-list)"

if [ -n "$NEUTRON_AGENT_LIST_CERN" ] || [ -n "$NEUTRON_AGENT_LIST_NEXT" ] || [ -n "$NEUTRON_AGENT_LIST_POC" ] || [ -n "$NEUTRON_AGENT_LIST_PDC" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname

      TEMP_ID1=$(echo "$NEUTRON_AGENT_LIST_CERN" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID2=$(echo "$NEUTRON_AGENT_LIST_NEXT" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID3=$(echo "$NEUTRON_AGENT_LIST_POC"  | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID4=$(echo "$NEUTRON_AGENT_LIST_PDC"  | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')

      if [ -n "$TEMP_ID1" ]; then
        REGION_NAME="cern"
        HOST_ID=$TEMP_ID1
      elif [ -n "$TEMP_ID2" ]; then
        REGION_NAME="next"
        HOST_ID=$TEMP_ID2
      elif [ -n "$TEMP_ID3" ]; then
        REGION_NAME="poc"
        HOST_ID=$TEMP_ID3
      elif [ -n "$TEMP_ID4" ]; then
        REGION_NAME="pdc"
        HOST_ID=$TEMP_ID4
      else
        echo "[WARNING] Not found. $HOST is not a neutron host"
        ((KO++))
        continue
      fi

      export OS_REGION_NAME=$REGION_NAME
      echo "[INFO] Trying to $RD_OPTION_STATUS neutron-linuxbridge-agent on $HOST_ID..."
      if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
        if [ "${RD_OPTION_STATUS}" == 'disable' ]; then
          if neutron agent-update "$HOST_ID" --admin-state-down; then
            echo "[INFO] neutron-linuxbridge-agent successfully ${RD_OPTION_STATUS}d on $HOST."
            ((OK++))
          else
            sleep 1 # output messages order
            echo "[ERROR] Failed to $RD_OPTION_STATUS neutron-linuxbridge-agent on $HOST."
            ((KO++))
          fi
        elif neutron agent-update "$HOST_ID" --admin-state-up; then
          echo "[INFO] neutron-linuxbridge-agent successfully ${RD_OPTION_STATUS}d on $HOST."
          ((OK++))
        else
          sleep 1 # output messages order
          echo "[ERROR] Failed to $RD_OPTION_STATUS neutron-linuxbridge-agent on $HOST."
          ((KO++))
        fi

      else
        echo "[DRYRUN][INFO] neutron agent-$RD_OPTION_STATUS $HOST_INFO neutron-linuxbridge-agent"
        ((OK++))
      fi
      echo ""
    done
else
  echo "[ERROR] Something went wrong executing neutron agent-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY NEUTRON ${RD_OPTION_STATUS^^}:     %d:   %d:  %d" "$((OK + KO))" "$OK" "$KO" | column  -t -s ':'
echo ""
