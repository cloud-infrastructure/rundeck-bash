#!/bin/bash

OPTIONS=""

if [[ "${RD_OPTION_IGNORE}" == 'yes' ]]; then
  OPTIONS+="--ignore "
fi

echo "Executing cci-hypervisor-has-vms..."
# shellcheck disable=SC2086
cci-hypervisor-has-vms $OPTIONS --hypervisors "$RD_OPTION_HYPERVISORS"
exit $?
