#!/bin/sh

# getting the Kerberos ticket
if [ ! -f "$RD_OPTION_KEYTAB" ]; then
  echo "[ERROR] No keytab found for user ${RD_OPTION_USER}"
  exit 2
fi

kinit -kt "${RD_OPTION_KEYTAB}" "${RD_OPTION_USER}"

# status 1 if the credentials cache cannot be read or is expired, and with status 0 otherwise
if ! klist -s; then
  echo "[ERROR] Kinit failure"
  exit 2
fi
echo "Kerberos granting ticket for ${RD_OPTION_USER} obtained successfully"
