#!/bin/bash

export OS_CLOUD=cern

instance_id=$(openstack server list --all -n --name "$RD_OPTION_VM" -f value -c ID)
echo "Instance ID: $instance_id"
server_output=$(openstack server show "$instance_id" -f json)

hypervisor=$(echo "$server_output" | jq -r '."OS-EXT-SRV-ATTR:hypervisor_hostname"')
echo "Hypervisor: $hypervisor"
instance=$(echo "$server_output" | jq -r '."OS-EXT-SRV-ATTR:instance_name"')
echo "Instance: $instance"
image=$(echo "$server_output" | jq -r .image)
echo "Image: $image"

if [[ -z "$image" ]] || [[ "$image" = "N/A (booted from volume)" ]]; then
  echo "Instance was booted from a volume."
  boot_from_volume=true
else
  echo "Instance was not booted from a volume."
  boot_from_volume=false
fi

cat << EOF > "/tmp/backup.${instance}.sh"
BACKUP_TIMEOUT=14400
BACKUP_INTERVAL=30

timestamp() {
  date +%s
}

wait_for_backup() {
  TIMESTAMP=\$(timestamp)
  TIMEOUT=\$((TIMESTAMP + BACKUP_TIMEOUT ))

  while (( \$(timestamp) < TIMEOUT )); do

    BACKUP_STATUS=\$(virsh domjobinfo ${instance} | grep "Job type" | cut -d':' -f2 | xargs)

    if [[ \${BACKUP_STATUS} == 'None' ]]; then
      echo "Backup has been created successfully"
      return
    else
      echo "Backup is not READY yet (current: \$BACKUP_STATUS). Sleeping for \$BACKUP_INTERVAL seconds before checking again..."
      sleep "\$BACKUP_INTERVAL"
    fi
  done

  echo "Backup is not AVAILABLE after \$BACKUP_TIMEOUT seconds. Exit"
  exit 1
}

echo "Connected to $hypervisor"
mkdir -p "/var/lib/nova/dumps/$instance"
echo "Cleaning any leftovers from previous runs"
rm -f /var/lib/nova/dumps/$instance/vda.qcow2
rm -f /var/lib/nova/dumps/$instance/memorydump.elf
echo "Creating backup xml file"
cat << ENDBACKUP > "/var/lib/nova/dumps/$instance/backup.xml"
<domainbackup>
  <disks>
    <disk name='vda' type='file'>
      <target file="/var/lib/nova/dumps/$instance/vda.qcow2"/>
    </disk>
  </disks>
</domainbackup>
ENDBACKUP

echo "Creating memory dump"
virsh dump --domain "$instance" --live --file "/var/lib/nova/dumps/$instance/memorydump.elf" --memory-only

if [[ "$boot_from_volume" == false ]]; then
  echo "Starting backup process"
  virsh backup-begin "$instance" --backupxml "/var/lib/nova/dumps/$instance/backup.xml"
  echo "Waiting until the backup has completed"
  wait_for_backup
else
  echo "Instance was booted from a volume. Skipping backup-begin step."
fi

echo "END"
EOF

scp -o StrictHostKeyChecking=no "/tmp/backup.${instance}.sh" "root@${hypervisor}:/tmp/backup.${instance}.sh"
ssh -o StrictHostKeyChecking=no "root@${hypervisor}" "/bin/bash /tmp/backup.${instance}.sh"

echo "Creating target directory in security dumps"
mkdir -p "/var/security_dumps/${RD_OPTION_VM}/"
echo "Copying files to the target directory"
scp -o StrictHostKeyChecking=no "root@${hypervisor}:/var/lib/nova/dumps/${instance}/*" "/var/security_dumps/${RD_OPTION_VM}/"

echo "Cleaning files in hypervisor"
ssh -o StrictHostKeyChecking=no "root@${hypervisor}" "rm -f /var/lib/nova/dumps/${instance}/*"

echo "Removing task for backup from rundeck"
rm -f "/tmp/backup.${instance}.sh"
echo "END"
