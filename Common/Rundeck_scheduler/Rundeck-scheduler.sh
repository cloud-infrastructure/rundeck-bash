#!/bin/bash

if [ -z "$RD_OPTION_PARAMETERS" ]
    then
    PARAMETERS="{}"
else
    PARAMETERS=$RD_OPTION_PARAMETERS
fi

echo "$RD_OPTION_DATETIME"

echo "Executing cci-rundeck-scheduler..."
cci-rundeck-scheduler --auth sso --"$RD_OPTION_BEHAVIOUR" --job-id "$RD_OPTION_JOB_ID" \
--datetime "$RD_OPTION_DATETIME" --log-level "$RD_OPTION_LOG_LEVEL" \
--parameters "$PARAMETERS" --user "$RD_JOB_USERNAME"
exit $?
