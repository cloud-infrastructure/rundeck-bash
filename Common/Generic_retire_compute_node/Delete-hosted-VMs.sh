#!/bin/bash

export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_PROJECT_NAME='admin'
export OS_AUTH_TYPE=v3kerberos

echo "Executing cci-delete-hosted-vms..."
cci-delete-hosted-vms --behaviour "$RD_OPTION_BEHAVIOUR" \
--hosts "$RD_OPTION_HOSTS"
exit $?