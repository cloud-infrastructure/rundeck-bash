#!/bin/bash

if [[ $RD_OPTION_FORCE == 'yes' ]]; then
  FORCE+="--force "
fi

echo "Executing cci-clean-cattle-vms..."
# shellcheck disable=SC2086
cci-clean-cattle-vms --"$RD_OPTION_BEHAVIOUR" $FORCE --hypervisors "$RD_OPTION_HYPERVISORS"
exit $?
