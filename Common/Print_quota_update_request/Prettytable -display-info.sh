#!/bin/bash

echo "Ticket: https://$RD_OPTION_INSTANCE.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-dump-record-producer \
    quota-update \
    --ticket-number "$RD_OPTION_SNOW_TICKET" \
    --instance "$RD_OPTION_INSTANCE"

exit $?
