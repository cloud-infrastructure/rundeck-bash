#!/bin/bash

echo "Executing cci-send-calendar-invitation..."
cci-send-calendar-invitation --"$RD_OPTION_BEHAVIOUR" \
--organizer "$RD_OPTION_ORGANIZER" \
--attendees "$RD_OPTION_ATTENDEES" --title "$RD_OPTION_TITLE" \
--description "$RD_OPTION_DESCRIPTION" --date "$RD_OPTION_DATE" \
--duration "$RD_OPTION_DURATION"
exit $?