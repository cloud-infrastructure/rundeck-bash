#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes reallyyy long
export OS_REGION_NAME=cern
NOVA_SERVICE_LIST_CERN="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=next
NOVA_SERVICE_LIST_NEXT="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=poc
NOVA_SERVICE_LIST_POC="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=pdc
NOVA_SERVICE_LIST_PDC="$(nova service-list --binary nova-compute)"

echo "[INFO] Trying to stop the nova-compute service on the compute nodes $RD_OPTION_HOSTS..."
pssh -H "$RD_OPTION_HOSTS" -l root -O StrictHostKeyChecking=no 'systemctl stop openstack-nova-compute ; systemctl disable openstack-nova-compute ; puppet agent --disable "Removing node from the cloud"'

echo "[INFO] Trying to remove the following compute nodes from Nova: $RD_OPTION_HOSTS..."

if [ -n "$NOVA_SERVICE_LIST_CERN" ] || [ -n "$NOVA_SERVICE_LIST_NEXT" ] || [ -n "$NOVA_SERVICE_LIST_POC" ] || [ -n "$NOVA_SERVICE_LIST_PDC" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname

      TEMP_ID1=$(echo "$NOVA_SERVICE_LIST_CERN" | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID2=$(echo "$NOVA_SERVICE_LIST_NEXT" | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID3=$(echo "$NOVA_SERVICE_LIST_POC"  | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID4=$(echo "$NOVA_SERVICE_LIST_PDC"  | awk "/$HOST_LOWER/" | awk '{print $2}')

      if [ -n "$TEMP_ID1" ]; then
        REGION_NAME="cern"
        HOST_ID=$TEMP_ID1
      elif [ -n "$TEMP_ID2" ]; then
        REGION_NAME="next"
        HOST_ID=$TEMP_ID2
      elif [ -n "$TEMP_ID3" ]; then
        REGION_NAME="poc"
        HOST_ID=$TEMP_ID3
      elif [ -n "$TEMP_ID4" ]; then
        REGION_NAME="pdc"
        HOST_ID=$TEMP_ID4
      else
        echo "[ERROR] Not found. Has $HOST been deleted already?"
        ((KO++))
        continue
      fi

      export OS_REGION_NAME=$REGION_NAME
      echo "[INFO] Removing compute node $HOST from Nova..."
      if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
        echo "[INFO] Executing: nova service-delete $HOST_ID..."

        
        if nova service-delete "$HOST_ID"; then
          echo "[INFO] $HOST successfully deleted"
          ((OK++))
        else
          sleep 1 #log messages sync
          echo "[ERROR] Failed to delete compute node on from Nova"
          ((KO++))
        fi
      else
        echo "[DRYRUN] Would've removed compute node $HOST from Nova"
        ((OK++))
      fi
      echo ""
    done
else
  echo "[ERROR] Something went wrong executing nova service-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY SERVICE DELETE:     %d:   %d:  %d" "$((OK + KO))" "$OK" "$KO" | column  -t -s ':'
echo ""

if [ "$KO" -gt 0 ]; then
  echo "[ERROR] Not possible to remove some compute nodes from Nova. Please check logs."
  exit 2
fi
