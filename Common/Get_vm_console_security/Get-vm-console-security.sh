#!/bin/bash

source /var/lib/rundeck/data/openrc

REGIONS=$(openstack region list -f value -c Region)

for REGION in $REGIONS; do
    export OS_REGION_NAME=$REGION
    echo "[INFO] Getting ID for VM '$RD_OPTION_INSTANCE' in region '$REGION'"
    INSTANCE_ID=$(openstack server list --all-projects --name "^${RD_OPTION_INSTANCE}$" -n -c ID -f value)
    if [ ! -z "$INSTANCE_ID" ]; then
        echo "Instance '$RD_OPTION_INSTANCE' found in region '$REGION' with ID: $INSTANCE_ID"
        echo "[INFO] Getting console url for $RD_OPTION_INSTANCE..."
        CONSOLE_URL=$(openstack console url show "${INSTANCE_ID}" -c url -f value)
        ret=$?
        echo "[INFO] Console URL: ""${CONSOLE_URL}"""
        exit "$ret"
    else
        echo "[ERROR] No instance ID found with the name '$RD_OPTION_INSTANCE'. \
        Please check that the name is correct."
    fi
done