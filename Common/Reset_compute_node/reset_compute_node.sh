#!/usr/bin/env bash

#set -x

HOSTNAME=$(echo "${RD_OPTION_HOSTNAME}" | xargs -n1 | cut -f1 -d. | tr '[:upper:]' '[:lower:]')
TIMEOUT=1800
export OS_CLOUD=cern

ENDTIME=$(( $(date +%s) + TIMEOUT )) # Calculate for whole procedure

echo
echo "Resetting node $HOSTNAME - $(date)"
echo "Looking if node is in Ironic..."

# get the UUID of the server if it is available on ironic
ID=$(openstack server list --all --no-name-lookup --name "^${HOSTNAME}" -f value -c ID)

echo "Checking manufacturer..."

# Checking if it's a Quanta node if so we need to reset the BMC
MANUFACTURER=$(ai-pdb facts --fact dmi "$HOSTNAME.cern.ch" | jq .[0].value.manufacturer)

if [[ $MANUFACTURER = *"Quanta"* ]]; then
    echo "Node is a Quanta node, triggering restart of BMC"
    if [ -z "$ID" ]; then
        echo "Triggering BMC restart through ai-ipmi"
        ai-ipmi ipmitool-cmd 'bmc reset cold' "$HOSTNAME.cern.ch" | sh
    else
        echo "Triggering BMC restart through Ironic"
        openstack console url show "$ID" -f json | jq .url.ipmitool_mc_reset_cold | cut -f2 -d\" | sh
    fi
    echo "Waiting for BMC on $HOSTNAME to reset - $(date)"
    # Wait 10 seconds until the BMC is unavailable
    sleep 10

    BMC_READY=1
    CHASSIS=""
    while [ $BMC_READY -ne "0" ]; do
        if [ -z "$ID" ]; then
	    CHASSIS=$(ai-ipmi ipmitool-cmd 'chassis power status' "$HOSTNAME.cern.ch" | sh)
        else
            CHASSIS=$(openstack console url show "$ID" -f json | jq .url.ipmitool_chassis_power_status | cut -f2 -d\" | sh)
	fi
	if [[ $CHASSIS = *"Chassis Power"* ]]; then
            BMC_READY=0
	fi
        echo "Waiting for BMC on $HOSTNAME to be ready - $(date)"
        if [ "$(date +%s)" -gt "$ENDTIME" ]; then
            echo "ERROR -- The BMC did not became ready before ${TIMEOUT} seconds, exiting..."
            exit 1
        fi
        sleep 10
    done
fi

echo "Triggering reboot of the node..."

if [ -z "$ID" ]; then
    echo "Node not in Ironic, triggering ai-remote-power-control"
    ai-remote-power-control cycle "$HOSTNAME.cern.ch"
else
    echo "Node in Ironic, triggering reset through OpenStack"
    openstack server reboot --hard "$ID"
fi

REACHEABLE=1
while [ $REACHEABLE -ne "0" ]; do
    ping -q -c 1 -W 1 "$HOSTNAME" &> /dev/null
    REACHEABLE=$?
    echo "Waiting for $HOSTNAME to go down - $(date)"
    if [ "$(date +%s)" -gt "$ENDTIME" ]; then
        echo "ERROR -- The machine did not go down before ${TIMEOUT} seconds, exiting..."
        exit 1
    fi
    sleep 10
done

UNREACHEABLE=1
while [ $UNREACHEABLE -ne "0" ]; do
    ping -q -c 1 -W 1 "$HOSTNAME" &> /dev/null
    UNREACHEABLE=$?
    echo "Waiting for $HOSTNAME to become up - $(date)"
    if [ "$(date +%s)" -gt "$ENDTIME" ]; then
        echo "ERROR -- The machine did not came back before ${TIMEOUT} seconds, exiting..."
        exit 2
    fi
    sleep 30
done

SSH_READY=1
while [ $SSH_READY -ne "0" ]; do
    klist
    kinit -R

    # check if the machine is ready for SSH
    ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOSTNAME" 'hostname'
    SSH_READY=$?
    echo "Waiting for machine to be accessible through SSH - $(date)"
    if [ "$(date +%s)" -gt "$ENDTIME" ]; then
        echo "ERROR -- The machine is not accessible after the reboot before ${TIMEOUT} seconds, exiting..."
        exit 3
    fi
    sleep 30
done
