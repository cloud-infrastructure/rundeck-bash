#!/bin/bash

if [ -n "$RD_OPTION_HOSTGROUP" ]; then
  OPTIONS+="--hostgroup $RD_OPTION_HOSTGROUP"
fi

echo "Executing cci-check-node-foreman..."
# shellcheck disable=SC2086
cci-check-node-foreman --nodes "$RD_OPTION_NODES" $OPTIONS

exit $?
