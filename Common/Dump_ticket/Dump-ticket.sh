#!/bin/bash

if [ -z "${RD_OPTION_SNOW_TICKET}" ]; then
  echo "[INFO] No ticket number provided, unable to find ticket"
  exit 0
fi

if [ -n "$RD_OPTION_SNOW_TICKET" ]; then
  if [[ $RD_OPTION_SNOW_TICKET == INC* ]] || [[ $RD_OPTION_SNOW_TICKET == RQF* ]]; then
    if [[ $RD_OPTION_INSTANCE != 'cern' ]]; then
      echo "[INFO] Ticket: https://cerntest.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
    else
      echo "[INFO] Ticket: https://cern.service-now.com/service-portal?id=ticket&n=$RD_OPTION_SNOW_TICKET"
    fi
  else
    echo "[INFO] Not a valid ServiceNow ticket"
    exit 0 # not a Service-Now Ticket
  fi

  echo "[INFO] Trying to get information from the ticket $RD_OPTION_SNOW_TICKET..."
  if cci-dump-ticket --instance "$RD_OPTION_INSTANCE" --ticket-number "$RD_OPTION_SNOW_TICKET"; then
      echo "[INFO] Ticket $RD_OPTION_SNOW_TICKET found"
    else
      echo "[ERROR] Ticket $RD_OPTION_SNOW_TICKET NOT found"
      exit 1
  fi
fi
