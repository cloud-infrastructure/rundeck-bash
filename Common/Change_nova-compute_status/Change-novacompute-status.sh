#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes reallyyy long
export OS_REGION_NAME=cern
NOVA_SERVICE_LIST_CERN="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=next
NOVA_SERVICE_LIST_NEXT="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=poc
NOVA_SERVICE_LIST_POC="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=pdc
NOVA_SERVICE_LIST_PDC="$(nova service-list --binary nova-compute)"

if [[ ${RD_OPTION_REFERENCE} == "" ]]; then
  REASON_MESSAGE="${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
else
  REASON_MESSAGE="[$RD_OPTION_REFERENCE] ${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
fi

if [ -n "$NOVA_SERVICE_LIST_CERN" ] || [ -n "$NOVA_SERVICE_LIST_NEXT" ] || [ -n "$NOVA_SERVICE_LIST_POC" ] || [ -n "$NOVA_SERVICE_LIST_PDC" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname

      TEMP_ID1=$(echo "$NOVA_SERVICE_LIST_CERN" | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID2=$(echo "$NOVA_SERVICE_LIST_NEXT" | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID3=$(echo "$NOVA_SERVICE_LIST_POC"  | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID4=$(echo "$NOVA_SERVICE_LIST_PDC"  | awk "/$HOST_LOWER/" | awk '{print $2}')

      if [ -n "$TEMP_ID1" ]; then
        REGION_NAME="cern"
        HOST_ID=$TEMP_ID1
      elif [ -n "$TEMP_ID2" ]; then
        REGION_NAME="next"
        HOST_ID=$TEMP_ID2
      elif [ -n "$TEMP_ID3" ]; then
        REGION_NAME="poc"
        HOST_ID=$TEMP_ID3
      elif [ -n "$TEMP_ID4" ]; then
        REGION_NAME="pdc"
        HOST_ID=$TEMP_ID4
      else
        echo "[ERROR] Not found. Has $HOST been deleted already?"
        ((KO++))
        continue
      fi

      export OS_REGION_NAME=$REGION_NAME
      echo "[INFO] Trying to $RD_OPTION_STATUS nova-compute on $HOST_ID..."
      if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
        if [ "${RD_OPTION_STATUS}" == 'disable' ]; then
          echo "[INFO] Node scheduled for disabling of nova-compute."
          if nova service-"$RD_OPTION_STATUS" "$HOST_ID" --reason "$REASON_MESSAGE"; then
            echo "[INFO] nova-compute successfully '$RD_OPTION_STATUS'd on $HOST."
            ((OK++))
          else
            sleep 1 # output messages order
            echo "[ERROR] Failed to '$RD_OPTION_STATUS' nova-compute on $HOST."
            ((KO++))
          fi
        elif nova service-"$RD_OPTION_STATUS" "$HOST_ID"; then
          echo "[INFO] nova-compute successfully '$RD_OPTION_STATUS'd on $HOST."
          ((OK++))
        else
          sleep 1 # output messages order
          echo "[ERROR] Failed to '$RD_OPTION_STATUS' nova-compute on $HOST."
          ((KO++))
        fi
      else
        echo "[DRYRUN][INFO] nova service-$RD_OPTION_STATUS $HOST_ID --reason '$REASON_MESSAGE'"
        ((OK++))
      fi
      echo ""
    done

else
  echo "[ERROR] Something went wrong executing nova service-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[%s] SUMMARY NOVA %s:     $((OK + KO)):   $OK:  $KO" "${RD_OPTION_BEHAVIOUR^^}" "${RD_OPTION_STATUS^^}"| column  -t -s ':'
echo ""

if [ "$KO" -gt 0 ]; then
  echo "[ERROR] nova service-$RD_OPTION_STATUS did not work for some hosts. Please check logs."
  exit 2
fi
