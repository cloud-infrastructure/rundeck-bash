#!/bin/bash

source /var/lib/rundeck/data/openrc

REGIONS=$(openstack region list -f value -c Region)

for REGION in $REGIONS; do
    export OS_REGION_NAME=$REGION
    echo "Searching for instance '$RD_OPTION_VM_NAME' in region '$REGION'"
    INSTANCE_ID=$(openstack server list --all -n --name "^${RD_OPTION_VM_NAME}$" -f value -c ID)
    if [ ! -z "$INSTANCE_ID" ]; then
        echo "Instance '$RD_OPTION_VM_NAME' found in region '$REGION' with ID: $INSTANCE_ID"
        openstack server lock "$INSTANCE_ID"

        LATEST_EVENT_ACTION=$(openstack server event list $INSTANCE_ID -f value -c Action | head -n 1)
        if [ "$LATEST_EVENT_ACTION" == "lock" ]; then
            echo "Successfully locked the instance $INSTANCE_ID."
            exit 0
        else
            echo "The instance $RD_OPTION_VM_NAME could not be locked."
            exit 1
        fi
    else
        echo "ERROR: Instance '$RD_OPTION_VM_NAME' not found"
    fi
done
