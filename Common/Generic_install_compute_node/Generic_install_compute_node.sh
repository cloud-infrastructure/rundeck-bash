#!/bin/bash

export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_PROJECT_NAME='admin'
export OS_AUTH_TYPE=v3kerberos

#echo "Executing cci-wait-puppet-runs..."
#cci-wait-puppet-runs --${RD_OPTION_BEHAVIOUR} \
#--hosts "$RD_OPTION_HOSTS"

echo "#### IMPORTANT MESSAGE: OS-7107 ####"
echo "[Temporary solution] Wait for Puppet to finish after 3 hours..."
sleep 10800

exit $?
