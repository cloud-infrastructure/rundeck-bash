#!/bin/bash

echo "Executing cci-power-on-off-hv-servers..."
cci-power-on-off-hv-servers --action "$RD_OPTION_ACTION" \
--hypervisors "$RD_OPTION_HYPERVISORS" --username svcrdeck \
--mailto "$RD_OPTION_MAILTO"

exit $?