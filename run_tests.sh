#!/bin/bash

if [ "$1" != "" ]; then
    branch=$1
else
    branch=""
fi

if command -v docker &> /dev/null
then
    docker run -it -v "$(pwd):/scripts" registry.fedoraproject.org/fedora:latest bash -c "/scripts/test_internal.sh ${branch}"
elif command -v podman &> /dev/null
then
    podman run -it -v "$(pwd):/scripts:Z" registry.fedoraproject.org/fedora:latest bash -c "/scripts/test_internal.sh ${branch}"
else
    echo "Docker | Podman required to run the tests"
    exit 1
fi
