#!/bin/bash

function wrong_params {
  echo "Please, specify a list of hosts OR a SNOW ticket to execute the job"
  exit 1
}
 
# if all the options are set, we fail
if [ -n "$RD_OPTION_SNOW_TICKET" ] && [ -n "$RD_OPTION_HOSTS" ]
  then
  wrong_params
fi

# this is the base command
COMMAND="cci-check-machine "


# we built the options of the script depending on the variables passed in 
# the Rundeck job

if [ -n "$RD_OPTION_SNOW_TICKET" ]
  then
  COMMAND+="snow --ticket \"$RD_OPTION_SNOW_TICKET\" --instance \"$RD_OPTION_SNOW_INSTANCE\" --parallelize"
elif [ -n "$RD_OPTION_HOSTS" ]
  then
  COMMAND+="hosts --hosts \"$RD_OPTION_HOSTS\""
else
  # in any case the command is wrong
  wrong_params
fi

echo "Executing the following command: \"$COMMAND\""

# execute the job with the options
eval "$COMMAND"

# Save exit code to export later
exit_code=$?
echo RUNDECK:DATA:exit_code=$exit_code
exit 0
