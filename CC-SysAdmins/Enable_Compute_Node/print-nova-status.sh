#!/bin/bash

#only execute this once since it takes reallyyy long
source "$RD_OPTION_OPENRC"

export OS_REGION_NAME=cern
NOVA_SERVICE_LIST_CERN="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=next
NOVA_SERVICE_LIST_NEXT="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=poc
NOVA_SERVICE_LIST_POC="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=pdc
NOVA_SERVICE_LIST_PDC="$(nova service-list --binary nova-compute)"

for HOST in $RD_OPTION_HOSTS
  do
    #Gettting cell info
    HOST_LOWER=${HOST,,} #lower case hostname
    TEMP_ID1=$(echo "$NOVA_SERVICE_LIST_CERN" | awk "/$HOST_LOWER/" | awk '{print $2}')
    TEMP_ID2=$(echo "$NOVA_SERVICE_LIST_NEXT" | awk "/$HOST_LOWER/" | awk '{print $2}')
    TEMP_ID3=$(echo "$NOVA_SERVICE_LIST_POC"  | awk "/$HOST_LOWER/" | awk '{print $2}')
    TEMP_ID4=$(echo "$NOVA_SERVICE_LIST_PDC"  | awk "/$HOST_LOWER/" | awk '{print $2}')

    if [ -n "$TEMP_ID1" ]; then
      HOST_STATUS=$(echo "$NOVA_SERVICE_LIST_CERN" | awk "/$HOST_LOWER/" | awk '{print $10}')
      HOST_STATE=$(echo "$NOVA_SERVICE_LIST_CERN" | awk "/$HOST_LOWER/" | awk '{print $12}')
    elif [ -n "$TEMP_ID2" ]; then
      HOST_STATUS=$(echo "$NOVA_SERVICE_LIST_NEXT" | awk "/$HOST_LOWER/" | awk '{print $10}')
      HOST_STATE=$(echo "$NOVA_SERVICE_LIST_NEXT" | awk "/$HOST_LOWER/" | awk '{print $12}')
    elif [ -n "$TEMP_ID3" ]; then
      HOST_STATUS=$(echo "$NOVA_SERVICE_LIST_POC" | awk "/$HOST_LOWER/" | awk '{print $10}')
      HOST_STATE=$(echo "$NOVA_SERVICE_LIST_POC" | awk "/$HOST_LOWER/" | awk '{print $12}')
    elif [ -n "$TEMP_ID3" ]; then
      HOST_STATUS=$(echo "$NOVA_SERVICE_LIST_PDC" | awk "/$HOST_LOWER/" | awk '{print $10}')
      HOST_STATE=$(echo "$NOVA_SERVICE_LIST_PDC" | awk "/$HOST_LOWER/" | awk '{print $12}')
    else
      echo "[ERROR] $HOST Not found."
      continue
    fi
    echo "$HOST, status=$HOST_STATUS, state=$HOST_STATE"
  done