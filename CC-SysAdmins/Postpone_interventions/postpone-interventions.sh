#!/bin/bash

OPTIONS="--$RD_OPTION_BEHAVIOUR "

if [ -n "$RD_OPTION_HOSTS" ]; then
  OPTIONS+="--hosts \"$RD_OPTION_HOSTS\" "
fi

if [ -n "$RD_OPTION_TICKETS" ]; then
  OPTIONS+="--tickets \"$RD_OPTION_TICKETS\" "
fi

if [ -n "$RD_OPTION_START_DATE" ]; then
  OPTIONS+="--start-date \"$RD_OPTION_START_DATE\" "
fi

if [ -n "$RD_OPTION_USER" ]; then
  OPTIONS+="--user $RD_OPTION_USER "
fi

if [ -n "$RD_OPTION_EXECUTION_ID" ]; then
  OPTIONS+="--execution-ID \"$RD_OPTION_EXECUTION_ID\" "
fi

if [ -n "$RD_OPTION_JOB_ID" ]; then
  OPTIONS+="--job-ID $RD_OPTION_JOB_ID "
fi

echo "Executing cci-interventions-manager..."
CMD="cci-interventions-manager --auth sso $OPTIONS postpone --new-date \"$RD_OPTION_NEW_DATE\""
/bin/bash -c "$CMD"

exit $?