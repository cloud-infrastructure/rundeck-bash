#!/usr/bin/env bash

ERR=0
for HOST in $RD_OPTION_HOSTS
do
    res=$(eval cci-check-node-foreman  --nodes "$HOST" -vv 2>&1)
    echo "$res"
    if [[ $res =~ gva_shared ]]
    then
        echo "Host $HOST: proceed..."
    elif [[ $res =~ crit_project_003 ]]
    then
        echo "Host $HOST: proceed..."
    elif [[ $res =~ crit_project_004 ]]
    then
        echo "Host $HOST: proceed..."
    elif [[ $res =~ pre_stage ]]
    then
        echo "Host $HOST: proceed..."
    elif [[ $res =~ qa ]]
    then
        echo "Host $HOST: proceed..."
    else
        HYPERVISOR=$(eval cci-hypervisor-has-vms --hypervisors "$HOST" 2>&1)
        if [[ $HYPERVISOR == *"does not have VMs in any region"* ]]
        then
            echo "Compute node $HOST is empty."
        else
            ERR=$((ERR+1))
            echo "Compute node $HOST: VM Live migration operations are not supported in this cell"
            echo " The job will exit now . Please execute Step 2 (SCHEDULE)"
        fi
    fi
done

if [ $ERR -gt 0 ]; then
    exit 2
fi

exit
