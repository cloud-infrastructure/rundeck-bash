#!/bin/bash

# NO_CONTACT exit codes
#FALSE_ALARM=0
#PING_OK_SSH_KO=10
NON_CONTACTABLE=20

if [ "${RD_OPTION_EXIT_CODE}" -ne "${NON_CONTACTABLE}" ];then
    echo "[ERROR] Something wrong happened before. Cancel this workflow..."
    exit 1
fi
