#!/bin/bash

echo "[WARNING] Escalating ticket..."
cci-update-ticket --ticket-number "$RD_OPTION_SNOW_TICKET" \
--instance "$RD_OPTION_SNOW_INSTANCE" \
--worknote "Escalating ticket to SysAdmins. Executed by $RD_JOB_USERNAME. Rundeck job: $RD_JOB_URL" \
--fe "Sys Admin" --assignment-group "Sys Admin 2nd Line Support" \
--watch-list "cloud-infrastructure-3rd-level@cern.ch"
