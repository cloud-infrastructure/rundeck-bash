#!/bin/bash

# this is the base command
COMMAND="cci-check-machine "

# we built the options of the script depending on the variables passed in 
# the Rundeck job

COMMAND+="snow --ticket \"$RD_OPTION_SNOW_TICKET\" --instance \"$RD_OPTION_SNOW_INSTANCE\""

echo "Executing the following command: \"$COMMAND\""

# execute the job with the options
eval "$COMMAND"
