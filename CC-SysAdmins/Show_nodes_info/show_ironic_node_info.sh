#!/bin/bash
if [ ${RD_OPTION_CLOUD} == "cern" ]; then
    echo "[INFO] Using CERN cloud."
    export OS_CLOUD=ironic
else
    echo "[INFO] Using PDC cloud."
    export OS_CLOUD=ironic_pdc
fi

showNodeInfo () {
        local ironic_node_uuid=$1
        local NODE_INFO
        local NODE_POWER
        local NODE_MAINTENANCE
        local NODE_MAINT_REASON
        local IPMI_IP
        local NODE_STATE
        local NODE_MAC
        local NODE_CONSOLE
        local IPMI_USER
        local IPMI_PASSWD
        NODE_INFO=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show "$ironic_node_uuid" -f json -c power_state -c maintenance -c maintenance_reason -c driver_info -c provision_state)
        NODE_POWER=$(echo "$NODE_INFO" | jq .power_state)
        NODE_MAINTENANCE=$(echo "$NODE_INFO" | jq .maintenance)
        NODE_MAINT_REASON=$(echo "$NODE_INFO" | jq .maintenance_reason)
        IPMI_IP=$(echo "$NODE_INFO" | jq .driver_info.ipmi_address)
        NODE_STATE=$(echo "$NODE_INFO" | jq .provision_state)
        echo "[INFO] Power state on $ironic_node_uuid is set to $NODE_POWER, ironic node state is $NODE_STATE."
        if [ "$NODE_MAINTENANCE" = true ]; then
                echo "[INFO] Maintenance is set to True on the baremetal node $ironic_node_uuid. Reason: $NODE_MAINT_REASON."
        else
                echo "[INFO] Maintenance is set to False on the baremetal node $ironic_node_uuid."
        fi
        NODE_MAC=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal port list --node "$ironic_node_uuid" -f value -c Address)
        echo "[INFO] MAC address of $ironic_node_uuid node is $NODE_MAC."
        echo "[INFO] IPMI address of $ironic_node_uuid node is $IPMI_IP." 
        NODE_CONSOLE=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node console show -f json  "$ironic_node_uuid" -c console_info)
        IPMI_USER=$(echo "$NODE_CONSOLE" | jq .console_info.username)
        IPMI_PASSWD=$(echo "$NODE_CONSOLE" | jq .console_info.password)
        echo "[INFO] IPMI username is $IPMI_USER, IPMI password is $IPMI_PASSWD."
}

echo "[INFO] Showing info for nodes"

# remove .cern.ch and lowercase all instances passed
NORMALIZED_NAMES=$(echo "$RD_OPTION_HOSTS" | sed 's/.cern.ch//g' | awk '{print tolower($0)}')
for NAME in $NORMALIZED_NAMES
do
        INSTANCE_ID=$(openstack server list --all-projects --name "^${NAME}$" -n -c ID -f value)
        if [[ -z "${INSTANCE_ID}" ]]; then
                echo "[INFO] No instance with the name $NAME was found. Checking if it is a serial tag."
                IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show "$NAME" -f value -c uuid)
                if [[ -z "${IRONIC_NODE_UUID}" ]]; then
                        echo "[INFO] No node with the name $NAME. This not an Ironic-managed node!. Moving on to the next node..."
                        continue
                else
                        echo "[INFO] Ironic node $NAME belongs to baremetal node '$IRONIC_NODE_UUID'."
                fi
        else
                echo "[INFO] Ironic instance $NAME was found. Collecting baremetal node information..."
                NOVA_STATE=$(OS_BAREMETAL_API_VERSION=1.9 openstack server show "$INSTANCE_ID" -f value -c OS-EXT-STS:vm_state)
                echo "[INFO] Nova state of the instance $NAME is $NOVA_STATE."
                IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show --instance "$INSTANCE_ID" -f value -c uuid)
                echo "[INFO] Ironic instance $NAME belongs to baremetal node '$IRONIC_NODE_UUID'."
        fi
        if [[ -z "$IRONIC_NODE_UUID" ]]; then
                echo "[ERROR] The node doesn't exist"
                exit 1
        else
                showNodeInfo "$IRONIC_NODE_UUID"
        fi
done

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] failed for some Ironic nodes. Please check logs"
  exit 1
fi
