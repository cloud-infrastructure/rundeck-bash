#!/bin/bash

echo "EXECUTING MIGRATION CYCLE"

echo "--hosts $RD_OPTION_HOSTS --cloud $RD_OPTION_INSTANCE --power-operation reboot --compute-enable true --roger-enable true --disable-reason disabled by rundeck --skip-shutdown-vms false --skip-disabled-compute-nodes true --max-threads 1 --no-logfile"


/usr/bin/migration_cycle --hosts "$RD_OPTION_HOSTS" --cloud "$RD_OPTION_INSTANCE" --stop_at_migration_failure "False" --power-operation "reboot" --compute-enable "true" --disable-reason "disabled by rundeck" --skip-shutdown-vms "false" --skip-disabled-compute-nodes "true" --max-threads "1" --roger-enable "true" --no-logfile
ret=$?

echo "MIGRATION CYCLE FINISHED"
exit "$ret"