#!/bin/bash

if [ ${RD_OPTION_CLOUD} == "cern" ]; then
    echo "[INFO] Using CERN cloud."
    export OS_CLOUD=ironic
else
    echo "[INFO] Using PDC cloud."
    export OS_CLOUD=ironic_pdc
fi

changeBMCcreds () {
        local ironic_node_uuid=$1
        local NODE_CONSOLE
        local IPMI_USER
        local IPMI_PASSWD
        NODE_CONSOLE=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node console show -f json  "$ironic_node_uuid" -c console_info)
        IPMI_USER=$(echo "$NODE_CONSOLE" | jq .console_info.username)
        IPMI_PASSWD=$(echo "$NODE_CONSOLE" | jq .console_info.password)
        echo "[INFO] IPMI username is $IPMI_USER, IPMI password is $IPMI_PASSWD."
        if [ -n "${RD_OPTION_USERNAME}" ]; then
                OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node set --driver-info ipmi_username="$RD_OPTION_USERNAME" "$ironic_node_uuid"
                OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node set --driver-info redfish_username="$RD_OPTION_USERNAME" "$ironic_node_uuid"
                echo "[INFO] BMC username is changed to $RD_OPTION_USERNAME."
        else
                echo "[INFO] BMC username is not changed, no parameter."
        fi
        if [ -n "${RD_OPTION_PASSWORD}" ]; then
                OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node set --driver-info ipmi_password="$RD_OPTION_PASSWORD" "$ironic_node_uuid"
                OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node set --driver-info redfish_password="$RD_OPTION_PASSWORD" "$ironic_node_uuid"
                echo "[INFO] BMC password is changed to $RD_OPTION_PASSWORD."
        else
                echo "[INFO] BMC password is not changed, no parameter."
        fi
}


echo "[INFO] Looking for the node..."

# remove .cern.ch and lowercase all instances passed
HOST=$(echo "$RD_OPTION_HOST" | sed 's/.cern.ch//g' | awk '{print tolower($0)}')
INSTANCE_ID=$(openstack server list --all-projects --name "^${HOST}$" -n -c ID -f value)
if [[ -z "${INSTANCE_ID}" ]]; then
        echo "[INFO] No instance with the name $HOST was found. Checking if it is a serial tag."
        IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show "$HOST" -f value -c uuid)                                                 
        if [[ -z "${IRONIC_NODE_UUID}" ]]; then
                echo "[INFO] No node with the name $HOST. This not an Ironic-managed node!. Moving on to the next node..."                                                                         
        else
                echo "[INFO] Ironic node $HOST belongs to baremetal node '$IRONIC_NODE_UUID'."
        fi
else
        echo "[INFO] Ironic instance $HOST was found. Collecting baremetal node information..."                                                                                                    
        IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show --instance "$INSTANCE_ID" -f value -c uuid)                             
        echo "[INFO] Ironic instance $HOST belongs to baremetal node '$IRONIC_NODE_UUID'."
fi

if [[ -z "$IRONIC_NODE_UUID" ]]; then
        echo "[ERROR] The node doesn't exist"
        exit 1
else
        changeBMCcreds "$IRONIC_NODE_UUID"
fi

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] failed for some Ironic nodes. Please check logs"
  exit 1
fi
