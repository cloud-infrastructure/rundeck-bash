#!/bin/bash

if [ ${RD_OPTION_CLOUD} == "cern" ]; then
    echo "[INFO] Using CERN cloud."
    export OS_CLOUD=ironic
else
    echo "[INFO] Using PDC cloud."
    export OS_CLOUD=ironic_pdc
fi

KO=0
echo "[INFO] Looking for Ironic nodes to $RD_OPTION_MAINTENANCE maintenance..."

# remove .cern.ch and lowercase all instances passed
NORMALIZED_INSTANCES=$(echo "$RD_OPTION_HOSTS" | sed 's/.cern.ch//g' | awk '{print tolower($0)}')
for INSTANCE in $NORMALIZED_INSTANCES
do
    INSTANCE_ID=$(openstack server list --all-projects --name "^${INSTANCE}$" -n -c ID -f value)

    if [[ -z "${INSTANCE_ID}" ]]; then
      echo "[INFO] No instance with the name $HOST was found. Checking if it is a serial tag."
      IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show "$INSTANCE" -f value -c uuid)                                                 
      if [[ -z "${IRONIC_NODE_UUID}" ]]; then
        echo "[INFO] No node with the name $HOST. This not an Ironic-managed node!. Moving on to the next node..."
        continue                                            
      else
        echo "[INFO] Ironic node $HOST belongs to baremetal node '$IRONIC_NODE_UUID'."
      fi
    else
      echo "[INFO] Ironic instance $INSTANCE was found. Collecting baremetal node information..."
      IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show --instance "$INSTANCE_ID" -f value -c uuid)
      echo "[INFO] Ironic instance $INSTANCE belongs to baremetal node '$IRONIC_NODE_UUID'."
    fi

    if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
      echo "[INFO] Calling maintenance $RD_OPTION_MAINTENANCE for node '$IRONIC_NODE_UUID'..."
      if [ "${RD_OPTION_MAINTENANCE}" == 'set' ]; then
        EXIT_CODE=$(openstack baremetal node maintenance "$RD_OPTION_MAINTENANCE" --reason "$RD_OPTION_MAINTENANCE maintenance triggered by $RD_JOB_USERNAME using Rundeck ($RD_JOB_ID)" "$IRONIC_NODE_UUID")
      else
        EXIT_CODE=$(openstack baremetal node maintenance "$RD_OPTION_MAINTENANCE" "$IRONIC_NODE_UUID")
      fi

      if [[ $EXIT_CODE -ne 0 ]]; then
        ((KO++))
      else
        echo "[INFO] Maintenance was updated successfully!"
      fi

    else
      echo "[DRYRUN] Calling maintenance $RD_OPTION_MAINTENANCE for Ironic node '$IRONIC_NODE_UUID'..."
    fi

done

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] $RD_OPTION_MAINTENANCE failed for some Ironic nodes. Please check logs"
  exit 1
fi
