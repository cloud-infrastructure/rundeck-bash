#!/bin/bash

OPTIONS=""

if [ -n "$RD_OPTION_HOSTS" ]; then
  OPTIONS+="--hosts \"$RD_OPTION_HOSTS\" "
fi

if [ -n "$RD_OPTION_TICKETS" ]; then
  OPTIONS+="--tickets \"$RD_OPTION_TICKETS\" "
fi

if [ -n "$RD_OPTION_START_DATE" ]; then
  OPTIONS+="--start-date \"$RD_OPTION_START_DATE\" "
fi

if [ -n "$RD_OPTION_USER" ]; then
  OPTIONS+="--user $RD_OPTION_USER "
fi

if [ -n "$RD_OPTION_EXECUTION_ID" ]; then
  OPTIONS+="--execution-ID \"$RD_OPTION_EXECUTION_ID\" "
fi

if [ -n "$RD_OPTION_JOB_ID" ]; then
  OPTIONS+="--job-ID $RD_OPTION_JOB_ID "
fi

if [ -n "$RD_OPTION_EXCLUDE_ID" ]; then
  OPTIONS+="--exclude-ID $RD_OPTION_EXCLUDE_ID "
fi

echo "Executing cci-interventions-manager..."
CMD="cci-interventions-manager --auth sso $OPTIONS list --exit-code"
/bin/bash -c "$CMD"

exit $?
