#!/bin/bash

EXIT_CODE=@data.exit_code@

# NO_CONTACT exit codes
FALSE_ALARM=0
PING_OK_SSH_KO=10
NON_CONTACTABLE=20

if [ "$EXIT_CODE" -eq "$FALSE_ALARM" ];then
  echo "[INFO] False alarm detected for provided hosts."
  exit 0
elif [ "$EXIT_CODE" -eq "$PING_OK_SSH_KO" ];then
  echo "[WARNING] SSH KO. Escalating ticket..."
  cci-update-ticket --ticket-number "$RD_OPTION_SNOW_TICKET" \
  --instance "$RD_OPTION_SNOW_INSTANCE" \
  --worknote "Escalating ticket to SysAdmins. Executed by $RD_JOB_USERNAME. Rundeck job: $RD_JOB_URL" \
  --fe "Sys Admin" --assignment-group "Sys Admin 2nd Line Support" \
  --watch-list "cloud-infrastructure-3rd-level@cern.ch"
  exit 0
elif [ "$EXIT_CODE" -eq "$NON_CONTACTABLE" ];then
  exit $NON_CONTACTABLE
else
  echo "[ERROR] Something wrong happened. Quitting..."
  exit 1
fi
