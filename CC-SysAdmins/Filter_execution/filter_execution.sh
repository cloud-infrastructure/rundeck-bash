#!/bin/bash

echo "Executing cci-rundeck-filter-execution..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-rundeck-filter-execution -vv \
  --auth "$RD_OPTION_AUTH" \
  --project "$RD_OPTION_PROJECT" \
  --host_filter "$RD_OPTION_HOST_FILTER" \
  --interval "$RD_OPTION_INTERVAL" \
  --columns "$RD_OPTION_COLUMNS"


  exit $?
