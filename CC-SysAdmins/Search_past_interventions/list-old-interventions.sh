#!/bin/bash

OPTIONS=""

if [ -n "$RD_OPTION_WITHIN_PERIOD" ]; then
  OPTIONS+="--within-last $RD_OPTION_WITHIN_PERIOD "
fi

if [ -n "$RD_OPTION_FROM_DATE" ]; then
  OPTIONS+="--search-from-date \"$RD_OPTION_FROM_DATE\" "
fi

if [ -n "$RD_OPTION_HOSTS" ]; then
  OPTIONS+="--hosts \"$RD_OPTION_HOSTS\" "
fi

if [ -n "$RD_OPTION_TICKETS" ]; then
  OPTIONS+="--tickets \"$RD_OPTION_TICKETS\" "
fi

if [ -n "$RD_OPTION_START_DATE" ]; then
  OPTIONS+="--start-date \"$RD_OPTION_START_DATE\" "
fi

if [ -n "$RD_OPTION_USER" ]; then
  OPTIONS+="--user $RD_OPTION_USER "
fi

if [ -n "$RD_OPTION_EXECUTION_ID" ]; then
  OPTIONS+="--execution-ID $RD_OPTION_EXECUTION_ID "
fi

if [ -n "$RD_OPTION_JOB_ID" ]; then
  OPTIONS+="--job-ID $RD_OPTION_JOB_ID "
fi

echo "Executing cci-interventions-manager..."
CMD="cci-interventions-manager --auth sso --history $OPTIONS list"
/bin/bash -c "$CMD"

exit $?