#!/bin/bash

if [ ${RD_OPTION_CLOUD} == "cern" ]; then
    echo "[INFO] Using CERN cloud."
    export OS_CLOUD=ironic
else
    echo "[INFO] Using PDC cloud."
    export OS_CLOUD=ironic_pdc
fi

changeNodeMac () {
        NODE_PORTS=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal port list --node "$IRONIC_NODE_UUID"  -c Address -c UUID -f json)
        echo "[INFO] Ports before the update are $NODE_PORTS"
        PORTS_AMOUNT=$(echo "$NODE_PORTS" | jq 'length')
        if [ "$PORTS_AMOUNT" -gt 1 ]; then
                echo "[ERROR] There are multiple ports assigned to the node. Contact the Cloud team to fix it."
                exit 1
        fi
        PORT_UUID=$(echo "$NODE_PORTS" | jq -r .[].UUID)
        echo "[INFO] Setting port's mac address to $MAC"
        OS_BAREMETAL_API_VERSION=1.9 openstack baremetal port set --address "${MAC}" "${PORT_UUID}"
        if [ $? -ne 0 ]; then
                echo "[ERROR] Couldn't change the port. Check the logs."
                exit 1
        fi
        NODE_PORTS=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal port list --node "$IRONIC_NODE_UUID"  -c Address -c UUID -f json)
        echo "[INFO] Ports after the update are $NODE_PORTS"
}

echo "[INFO] Looking for the node..."

# remove .cern.ch and lowercase all instances passed
HOST=$(echo "$RD_OPTION_HOST" | sed 's/.cern.ch//g' | awk '{print tolower($0)}')
MAC=$(echo "$RD_OPTION_MAC" | sed -e 's/-/:/g' | tr 'A-F' 'a-f')
INSTANCE_ID=$(openstack server list --all-projects --name "^${HOST}$" -n -c ID -f value)
if [[ -z "${INSTANCE_ID}" ]]; then
        echo "[INFO] No instance with the name $HOST was found. Checking if it is a serial tag."
        IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show "$HOST" -f value -c uuid)                                                 
        if [[ -z "${IRONIC_NODE_UUID}" ]]; then
                echo "[INFO] No node with the name $HOST. This not an Ironic-managed node!. Moving on to the next node..."                                                                         
        else
                echo "[INFO] Ironic node $HOST belongs to baremetal node '$IRONIC_NODE_UUID'."
        fi
else
        echo "[INFO] Ironic instance $HOST was found. Collecting baremetal node information..."                                                                                                    
        IRONIC_NODE_UUID=$(OS_BAREMETAL_API_VERSION=1.9 openstack baremetal node show --instance "$INSTANCE_ID" -f value -c uuid)                             
        echo "[INFO] Ironic instance $HOST belongs to baremetal node '$IRONIC_NODE_UUID'."
fi
if [[ -z "$IRONIC_NODE_UUID" ]]; then
        echo "[ERROR] The node doesn't exist"
        exit 1
else
        changeNodeMac
fi

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] failed for some Ironic nodes. Please check logs"
  exit 1
fi
