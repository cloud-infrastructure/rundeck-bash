#!/bin/bash
if [ "$RD_OPTION_ROGER_NOVA_STATE" == "enabled" ];then
    echo "Roger and nova sates are enabled"
else
    echo "ROGER_NOVA_STATE disabled was provided. Run 'enable disable compute job' to enable the node"
    echo "enable disable compute node : 'https://cci-rundeck.cern.ch/project/Common/job/show/7d23568a-1279-4796-8639-c72745258c78'"
fi