#!/bin/bash

echo "EXECUTING MIGRATION CYCLE"

if [ "$RD_OPTION_ROGER_NOVA_STATE" == "enabled" ];then
    RD_OPTION_ROGER_NOVA_STATE=true
else
    RD_OPTION_ROGER_NOVA_STATE=false
fi


echo "--hosts $RD_OPTION_HOSTS --cloud $RD_OPTION_INSTANCE --power-operation $RD_OPTION_COMMAND --compute-enable $RD_OPTION_ROGER_NOVA_STATE --roger-enable $RD_OPTION_ROGER_NOVA_STATE --disable-reason disabled by rundeck --skip-shutdown-vms false --skip-disabled-compute-nodes false --max-threads 1 --no-logfile"


/usr/bin/migration_cycle --hosts "$RD_OPTION_HOSTS" --cloud "$RD_OPTION_INSTANCE" --stop_at_migration_failure "False" --power-operation "$RD_OPTION_COMMAND" --compute-enable "$RD_OPTION_ROGER_NOVA_STATE" --disable-reason "disabled by rundeck" --skip-shutdown-vms "false" --skip-disabled-compute-nodes "false" --max-threads "1" --roger-enable "$RD_OPTION_ROGER_NOVA_STATE" --no-logfile

echo "MIGRATION CYCLE FINISHED"

result=$(cci-hypervisor-has-vms --hypervisors "$RD_OPTION_HOSTS" 2>&1)
if [[ $result == *"does not have VMs in any region"* ]]; then
        echo "migration operations successful"
        exit 0
else
        echo "compute node still has Vms : $result"
        exit 127
fi
