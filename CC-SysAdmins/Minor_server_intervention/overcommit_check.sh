#!/bin/bash
source /var/lib/rundeck/data/openrc

# convert to lowercase
hosts=${RD_OPTION_HOSTS,,}
echo "hosts : $hosts"

for host in $hosts
do
    # check for .cern.ch. if not then add it
    domain=".cern.ch"
    if ! [[ $host == *"$domain"* ]]; then
        host+=".cern.ch"
    fi

    UUID=$(openstack resource provider list --name "$host" -f value -c uuid)
    echo "Host $host UUID : $UUID"
    if [ -z "$UUID" ]; then
        echo "Host $host: UUID not found"
        exit 127
    fi

    AVAILABLE=$(openstack resource provider inventory list "$UUID" -c resource_class -c total | grep MEMORY_MB | cut -d'|' -f3 | xargs)
    USED=$(openstack resource provider usage show "$UUID" | grep MEMORY_MB | cut -d'|' -f3 | xargs)

    if [ "$USED" -gt "$AVAILABLE" ]; then
        echo "Host $host: overcommit issues. Please contact the Cloud Infrastructure team to help evacuating this node."
        exit 127
    fi
done
