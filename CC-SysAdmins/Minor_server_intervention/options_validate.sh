#!/bin/bash
if [ "$RD_OPTION_ROGER_NOVA_STATE" == "enabled" ] && [ "$RD_OPTION_COMMAND" == "poweroff" ];then
    echo "Poweroff operation and roger+nova enabled option cannot be used together."
    echo "Please select reboot or none operation to use roger+nova option"
    echo ""
    exit 127
else
    echo ""
fi

# Check if the machine is an hyperconverged one
for HOST in $RD_OPTION_HOSTS
  do
    #append .cern.ch
    if [[ $HOST != *.cern.ch ]]; then
      HOST="$HOST.cern.ch"
    fi
    #isolate hostgroup name
    HOSTGROUP=$(ai-foreman -f "name=${HOST,,}" --no-header -z Hostgroup showhost 2> /dev/null | awk '{ print $2 }' | xargs)
    echo "HOST: $HOST HOSTGROUP: $HOSTGROUP"

    CELL=$(echo "$HOSTGROUP" | cut -d'/' -f4)
    if [[ "$CELL" = "gva_project_046" ]] || [[ "$CELL" = "crit_project_004" ]]; then
      echo "Executing command to check if the machine is ready to reboot on $HOST..."
      RESULT=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=30 -l root "$HOST" '/usr/sbin/ready_to_reboot.sh')
      if [[ "$RESULT" != "Checking if it is OK to stop 0" ]]; then
        echo "The node $HOST is not ready to be rebooted, cancelling the operation..."
        echo ""
        exit 127
      fi
    fi
  done
