#!/bin/bash

if [ "$1" != "" ]; then
    branch=$1
else
    branch=master
fi

echo "Installing required packages"
dnf install git findutils ShellCheck -y &> /dev/null

echo "Cloning branch: ${branch}"
git clone https://gitlab.cern.ch/cloud-infrastructure/rundeck-bash.git --branch "${branch}" &> /dev/null

echo "Change directory"
cd rundeck-bash || exit

echo "Running shell checker"
find . -type f -name '*.sh' -printf '"%h/%f" ' | xargs shellcheck -e SC1090,SC1091,SC2181
echo "End"
