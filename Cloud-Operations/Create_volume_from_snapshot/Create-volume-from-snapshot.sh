#!/bin/bash

if [[ $RD_OPTION_RECREATE_FROM != 'volume' ]]; then
  echo "[INFO] This step is only needed when recreating from volume. Moving on!"
  exit 0
fi

VM_NAME=$RD_OPTION_VM_NAME
NEW_PROJECT_ID=$RD_OPTION_NEW_PROJECT_ID

VOLUME_MAX_TIMEOUT=$RD_OPTION_VOLUME_TIMEOUT
VOLUME_RETRY_INTERVAL=$RD_OPTION_VOLUME_RETRY_INTERVAL

if [[ "$RD_OPTION_SUFFIX" ]]; then
  SUFFIX=$RD_OPTION_SUFFIX
else
  echo "[INFO] Image suffix name not specified. Using \$RD_JOB_EXECID..."
  SUFFIX=$RD_JOB_EXECID
fi


source_openrc() {
  echo "[INFO] Unsetting all 'OS_' environment variables..."
  while read -r var; do unset "$var"; done < <(env | grep OS_ | cut -d= -f 1)

  echo "[INFO] Exporting default v3kerberos variables..."
  export OS_PROJECT_NAME=admin
  export OS_REGION_NAME=cern
  export OS_PROJECT_DOMAIN_ID=default
  export OS_IDENTITY_API_VERSION=3
  export OS_AUTH_TYPE=v3fedkerb
  export OS_IDENTITY_PROVIDER=sssd
  export OS_PROTOCOL=kerberos
  export OS_AUTH_URL=https://keystone.cern.ch/v3
}

timestamp() {
  date +%s
}

get_snapshot_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_snapshot_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack image show $1..."
    openstack image show "$1"
  fi
}

get_vm_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_vm_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack server list --all-p --name $1..."
    openstack server list --all-p --name "^$1$"
  fi
}

get_volume_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VOLUME name to 'get_volume_info'. Exiting...."
    exit 1
  else
    #echo "[INFO] Executing: openstack volume show $1..."
    openstack volume show "$1"
  fi
}

wait_for_volume() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VOLUME name"
    exit 1
  fi

  TIMESTAMP=$(timestamp)
  TIMEOUT=$(( TIMESTAMP + VOLUME_MAX_TIMEOUT ))

  while (( $(timestamp) < TIMEOUT )); do

    VOLUME_INFO=$(get_volume_info "$1")
    VOLUME_STATUS=$( echo "$VOLUME_INFO" | grep -wF '| status' | awk '{print $4}' )

    if [[ ${VOLUME_STATUS,,} == 'available' ]]; then
      echo "[INFO] $1 has been CREATED successfully"
      return
    else
      echo "[INFO] Volume $1 not available yet (current: $VOLUME_STATUS). Sleeping for $VOLUME_RETRY_INTERVAL seconds before checking again..."
      sleep "$VOLUME_RETRY_INTERVAL"
    fi
  done

  echo "Volume $1 not AVAILABLE after $VOLUME_MAX_TIMEOUT seconds. Exit"
  exit 1
}

# openrc as admin
source_openrc

VM_INFO=$(get_vm_info "$VM_NAME")
VM_ID=$( echo "$VM_INFO" | grep -iwF " $VM_NAME " | awk '{print $2}' )
VM_DETAILED_INFO=$( openstack server show "$VM_ID" -f yaml)
PROJECT_ID=$( echo "$VM_DETAILED_INFO" | grep project_id | cut -d ':' -f2 | sed -e 's/^[[:space:]]*//')
IMAGE_ID=$( echo "$VM_DETAILED_INFO" | grep image | cut -d ':' -f2 | sed -e 's/^[[:space:]]*//')

if [[ $IMAGE_ID == "''" ]]; then
  echo "[INFO] Skipping creation of volume as the machine is a boot from volume one ..."
  exit 0
fi

SNAPSHOT_NAME="${VM_NAME,,}_snap_$SUFFIX"

SNAPSHOT_INFO=$(get_snapshot_info "$SNAPSHOT_NAME")
SNAPSHOT_ID=$( echo "$SNAPSHOT_INFO" | grep -wF "| id" | awk '{print $4}')
SNAPSHOT_SIZE=$( echo "$SNAPSHOT_INFO" | grep -wF "| size" | awk '{print $4}')
SNAPSHOT_MIN_DISK=$( echo "$SNAPSHOT_INFO" | grep -wF "| min_disk" | awk '{print $4}')

FLAVOR=$( echo "$VM_DETAILED_INFO" | grep flavor | cut -d ':' -f2 | sed -e 's/^[[:space:]]*//')
FLAVOR_SIZE=$(openstack flavor list --all | grep -wF "| $FLAVOR " | awk '{print $8}')

TO_GB=$(( 1024*1024*1024 ))
# Round up result. Trick found here http://stackoverflow.com/a/24253318/333348
SNAPSHOT_SIZE=$(( (SNAPSHOT_SIZE + TO_GB - 1) / TO_GB ))

# Get largest between SNAPSHOT_MIN_DISK & FLAVOR_SIZE & SNAPSHOT_SIZE
VOLUME_SIZE=$(printf "%s\n" "$SNAPSHOT_MIN_DISK" "$FLAVOR_SIZE" "$SNAPSHOT_SIZE" | sort -n -r | head -n 1)

echo "[INFO] Changing image properties..."
openstack image set --property "hypervisor_type=qemu" --property "hw_cpu_max_sockets=2" --min-disk 0 "$SNAPSHOT_ID"

unset OS_PROJECT_NAME
if [ -z "$NEW_PROJECT_ID" ]; then
  export OS_PROJECT_ID=$PROJECT_ID
else
  # If the project is specified via parameter, use it
  export OS_PROJECT_ID=$NEW_PROJECT_ID
fi

VOLUME_NAME="${VM_NAME,,}_vol_$SUFFIX"
VOLUME_TYPE=$RD_OPTION_VOLUME_TYPE
echo "[INFO] Creating volume..."
echo "[INFO] Type $VOLUME_TYPE selected"
openstack volume create --size "$VOLUME_SIZE" --image "$SNAPSHOT_ID" --type "$RD_OPTION_VOLUME_TYPE" "$VOLUME_NAME"
wait_for_volume "$VOLUME_NAME"
