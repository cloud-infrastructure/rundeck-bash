#!/bin/bash

echo "[INFO] Mapping 'Cloud Probe - ${RD_OPTION_CELL}' project to new cell '${RD_OPTION_CELL}'..."

export OS_CLOUD=cern openstack project set --property cells_mapping="${RD_OPTION_CELL}" "Cloud Probe - ${RD_OPTION_CELL}"
export OS_CLOUD=pdc  openstack project set --property cells_mapping="${RD_OPTION_CELL}" "Cloud Probe - ${RD_OPTION_CELL}"
export OS_CLOUD=next openstack project set --property cells_mapping="${RD_OPTION_CELL}" "Cloud Probe - ${RD_OPTION_CELL}"
export OS_CLOUD=poc  openstack project set --property cells_mapping="${RD_OPTION_CELL}" "Cloud Probe - ${RD_OPTION_CELL}"
exit $?