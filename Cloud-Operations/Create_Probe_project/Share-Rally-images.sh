#!/bin/bash

echo "[INFO] Sharing Rally images for project 'Cloud Probe - ${RD_OPTION_CELL}'"

export OS_CLOUD="${RD_OPTION_REGION}"

PROJECT_ID=$(openstack project list -f value | grep "Cloud Probe - ${RD_OPTION_CELL}" | awk '{print $1}')
echo "[INFO] Project ID is ${PROJECT_ID}"

# Rally CirrOS 0.6.0-x86_64
openstack image add project 21f0fede-7cde-4aef-b755-b4982c442f5b "${PROJECT_ID}"
glance member-update 21f0fede-7cde-4aef-b755-b4982c442f5b "${PROJECT_ID}" accepted

# Rally CirrOS 0.6.0-aarch64
openstack image add project e18278b4-fa7d-4d07-b4fa-7d62437b5a0a "${PROJECT_ID}"
glance member-update e18278b4-fa7d-4d07-b4fa-7d62437b5a0a "${PROJECT_ID}" accepted

exit $?
