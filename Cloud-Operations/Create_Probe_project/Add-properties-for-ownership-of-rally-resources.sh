#!/bin/bash

echo "[INFO] Add properties for ownership of rally resources to 'Cloud Probe - ${RD_OPTION_CELL}' project..."

OS_CLOUD=cern openstack project set --property 'landb-responsible=cloud-infrastructure-infra-admin' --property 'landb-mainuser=cloud-infrastructure-infra-admin' "Cloud Probe - ${RD_OPTION_CELL}"
OS_CLOUD=poc  openstack project set --property 'landb-responsible=cloud-infrastructure-infra-admin' --property 'landb-mainuser=cloud-infrastructure-infra-admin' "Cloud Probe - ${RD_OPTION_CELL}"
OS_CLOUD=pdc  openstack project set --property 'landb-responsible=cloud-infrastructure-infra-admin' --property 'landb-mainuser=cloud-infrastructure-infra-admin' "Cloud Probe - ${RD_OPTION_CELL}"
OS_CLOUD=next openstack project set --property 'landb-responsible=cloud-infrastructure-infra-admin' --property 'landb-mainuser=cloud-infrastructure-infra-admin' "Cloud Probe - ${RD_OPTION_CELL}"
exit $?