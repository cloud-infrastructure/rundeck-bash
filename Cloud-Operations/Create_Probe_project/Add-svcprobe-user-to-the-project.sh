#!/bin/bash

echo "[INFO] Adding svcprobe user to 'Cloud Probe - ${RD_OPTION_CELL}' project..."

OS_CLOUD=cern openstack role add --user svcprobe --project "Cloud Probe - ${RD_OPTION_CELL}" probe
OS_CLOUD=pdc  openstack role add --user svcprobe --project "Cloud Probe - ${RD_OPTION_CELL}" probe
OS_CLOUD=poc  openstack role add --user svcprobe --project "Cloud Probe - ${RD_OPTION_CELL}" probe
OS_CLOUD=next openstack role add --user svcprobe --project "Cloud Probe - ${RD_OPTION_CELL}" probe
exit $?
