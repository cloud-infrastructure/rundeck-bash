#!/bin/bash

echo "[INFO] Enabling Rally-specific flavors for 'Cloud Probe - ${RD_OPTION_CELL}' project..."

export OS_CLOUD="${RD_OPTION_REGION}"

echo rally.{pico,nano,micro,tiny} | xargs -n1 openstack flavor set --project "Cloud Probe - ${RD_OPTION_CELL}"
exit $?
