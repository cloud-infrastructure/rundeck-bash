#!/bin/bash

echo "Executing cci-delete-project..."
message="Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"
cci-delete-project -vv --"$RD_OPTION_EXECUTION_MODE" --"$RD_OPTION_CHECK_RESOURCES" --execution-reference "$message" from-snow \
--instance "$RD_OPTION_INSTANCE" --ticket-number "$RD_OPTION_SNOW_TICKET" --resolver "$RD_JOB_USER_NAME"
exit $?