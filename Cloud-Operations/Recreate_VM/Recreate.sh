#!/bin/bash

if [[ "$RD_OPTION_SUFFIX" ]]; then
  SUFFIX=$RD_OPTION_SUFFIX
else
  echo "[INFO] Suffix name not specified. Using \$RD_JOB_EXECID..."
  SUFFIX=$RD_JOB_EXECID
fi

cci-recreate-instance -vv \
  --vm "$RD_OPTION_VM_NAME" \
  --recreate-from "$RD_OPTION_RECREATE_FROM" \
  --suffix "$SUFFIX" \
  --new-project-id "$RD_OPTION_NEW_PROJECT_ID" \
  --flavor_map "$RD_OPTION_FLAVOR_MAP" \
  --new_name "$RD_OPTION_NEW_NAME" \
  --volume-type "$RD_OPTION_VOLUME_TYPE" \
  --sysprep "$RD_OPTION_SYSPREP" \
  --sysprep-snapshot "$RD_OPTION_SYSPREP_SNAPSHOT" \
  --sysprep-params "$RD_OPTION_SYSPREP_PARAMS" \
  --start-from "$RD_OPTION_START_FROM"
