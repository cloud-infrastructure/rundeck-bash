#!/bin/bash

export OS_CLOUD=cern

VM_NAME=$RD_OPTION_VM_NAME
echo "[INFO] VM info before recreating..."
VM_ID=$(openstack server list --all --no-name --name "^${VM_NAME}\$" -f value -c ID)
openstack server show "$VM_ID"
