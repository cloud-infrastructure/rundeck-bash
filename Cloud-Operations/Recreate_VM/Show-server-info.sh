#!/bin/bash

export OS_CLOUD=cern
VM_NAME=$RD_OPTION_VM_NAME
VM_ID=$(openstack server list --all --no-name --name "^${VM_NAME}\$" -f value -c ID)

echo "[INFO] Listing information about newly recreated VM..."
openstack server show "$VM_ID"
echo "[INFO] $VM_NAME is up and running"
