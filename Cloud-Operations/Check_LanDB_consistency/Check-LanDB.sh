#!/bin/bash

# [Temporarily Disabled] 499a055a-b3af-45cc-876e-9890160b5f33

echo "Executing landb check..."
echo "Executed by Rundeck"
if [[ -n ${RD_OPTION_CELLS} ]] && [[ -n ${RD_OPTION_CLUSTERS} ]]; then
    echo "insert cells or clusters"
elif [[ -n ${RD_OPTION_CELLS} ]]; then
    echo "Executing on cells $RD_OPTION_CELLS"
    landb check --cell "$RD_OPTION_CELLS"
else
    echo "Executing on cluster $RD_OPTION_CLUSTERS"
    landb check --cluster "$RD_OPTION_CLUSTERS"
fi
exit $?