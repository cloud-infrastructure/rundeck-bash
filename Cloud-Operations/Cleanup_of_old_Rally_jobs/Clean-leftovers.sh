#!/bin/bash

echo "Executing cci-rally-cleanup..."
message="Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-rally-cleanup -vv --timeout "${RD_OPTION_TIMEOUT}" --execution-reference "$message" --instance "${RD_OPTION_INSTANCE}"
exit $?
