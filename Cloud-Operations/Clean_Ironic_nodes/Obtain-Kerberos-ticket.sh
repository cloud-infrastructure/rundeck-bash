#!/bin/bash

export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_AUTH_TYPE=v3kerberos

export OS_PROJECT_ID=d749b260-2b53-427a-af5e-7f539007a693
export OS_PROJECT_NAME="Cloud Test - gva_baremetal_001"

export OS_BAREMETAL_API_VERSION=1.31
export IRONIC_API_VERSION=1.31

echo "Executing ironic-nodes-cleanup..."
openstack baremetal node list | grep "clean failed" | awk '{print $4}'
for i in $(openstack baremetal node list | grep "clean failed" | awk '{print $2}'); do openstack baremetal node maintenance unset "$i"; openstack baremetal node manage "$i"; openstack baremetal node provide "$i"; done
exit $?