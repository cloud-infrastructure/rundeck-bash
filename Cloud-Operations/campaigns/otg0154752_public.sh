#!/bin/bash

echo "Executing Job 'Update list of VMs for HW replacement campaign migration'..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

GIT_REPO='https://svcrdeck@gitlab.cern.ch:8443/cloud-infrastructure/interventions/otg0154752.git'
DESTINATION='/tmp/otg0154752_public'
REFRESH_SCRIPT='refresh.sh'
VM_LIST='full_report.txt'

exit_on_err() {
    if [[ $1 != 0 ]]; then
        if [[ -n "$2" ]]; then
            echo "$2"
        fi
        echo "ERROR - stop job"
        exit "$1"
    fi
}

git_commit() {
    # Add a commit with the new changes
    # And the current date
    date=$(date)
    git add .
    echo "Commiting latest updates"
    git commit -am "Update from Rundeck ($date)"
    exit_on_err $?
    echo "Pushing to $1"
    git push -q origin "$1"
    exit_on_err $?
}

repo_clone() {
    # Clone repo if doesn't exist
    if [ ! -d "$2" ]; then
        echo "Cloning repository in $2"
        git clone -q "$1" "$2"
        exit_on_err $?
        cd "$2" || exit
        exit_on_err $?
    else
        cd "$DESTINATION" || { echo "Failure"; exit 1; }
        exit_on_err $?
        echo "Refresh cloned repo"
        git pull --rebase
        exit_on_err $?
    fi
}

refresh_and_check_vms_list() {
    # Run refresh.sh and check if
    # The list of VMs is empty
    cd "$DESTINATION" || { echo "Failure"; exit 1; }
    exit_on_err $?
    checksum_before=$(md5sum "$2")
    echo "Running $1"
    . "$1"
    exit_on_err $?
    sleep 30
    checksum_after=$(md5sum "$2")
    if [ "$checksum_before" != "$checksum_after" ] ; then
        echo "File $2 has changed, committing changes..."
        git_commit 'master'
        exit_on_err $?
    else
        echo "File $2 has not changed, skipping..."
        exit
    fi
}

remove_repo() {
    cd /tmp || { echo "Failure"; exit 1; }
    echo "Remove: $1"
    rm -rf "$1"
    exit_on_err $?
}

source /var/lib/rundeck/data/openrc

repo_clone "$GIT_REPO" "$DESTINATION"
refresh_and_check_vms_list "$REFRESH_SCRIPT" "$VM_LIST"
remove_repo "$DESTINATION"

exit $?
