#!/bin/bash

echo "Executing Job 'Notify Users for HW replacement campaign migration'..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

GIT_REPO='https://svcrdeck@gitlab.cern.ch:8443/cloud-infrastructure/campaigns/otg0154752.git'
DESTINATION='/tmp/otg0154752_notify'
NOTIFY_SCRIPT='notify.sh'

exit_on_err() {
    if [[ $1 != 0 ]]; then
        if [[ -n "$2" ]]; then
            echo "$2"
        fi
        echo "ERROR - stop job"
        exit "$1"
    fi
}

repo_clone() {
    # Clone repo if doesn't exist
    if [ ! -d "$2" ]; then
        echo "Cloning repository in $2"
        git clone -q "$1" "$2"
        exit_on_err $?
        cd "$2" || exit
        exit_on_err $?
    else
        cd "$DESTINATION" || { echo "Failure"; exit 1; }
        exit_on_err $?
        echo "Refresh cloned repo"
        git pull --rebase
        exit_on_err $?
    fi
}

notify() {
    # Run notify.sh
    cd "$DESTINATION" || { echo "Failure"; exit 1; }
    exit_on_err $?
    echo "Running $1"
    . "$1"
    exit_on_err $?
    sleep 30
}

remove_repo() {
    cd /tmp || { echo "Failure"; exit 1; }
    echo "Remove: $1"
    rm -rf "$1"
    exit_on_err $?
}

source /var/lib/rundeck/data/openrc

repo_clone "$GIT_REPO" "$DESTINATION"
notify "$NOTIFY_SCRIPT"
remove_repo "$DESTINATION"

exit $?
