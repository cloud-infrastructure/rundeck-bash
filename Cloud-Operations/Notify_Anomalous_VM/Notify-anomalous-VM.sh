#!/bin/bash

echo "Executing cci-notify-anomalous-vm..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-notify-anomalous-vm -vv \
  --"$RD_OPTION_BEHAVIOUR" \
  --force_execution "$RD_OPTION_FORCE_EXECUTION" \
  --vm "$RD_OPTION_VM" \
  --issue "$RD_OPTION_ISSUE" \
  --extra_info "$RD_OPTION_EXTRA_INFO" \
  --mail_from "$RD_OPTION_MAIL_FROM" \
  --mail_cc "$RD_OPTION_MAIL_CC" \
  --mail_bcc "$RD_OPTION_MAIL_BCC" \
  --mail_content_type "$RD_OPTION_MAIL_CONTENT_TYPE" \
  --instance "$RD_OPTION_INSTANCE"


  exit $?
