#!/bin/bash

cci-recreate-instance -vv --vm "$RD_OPTION_VM_NAME" --recreate-from "$RD_OPTION_RECREATE_FROM" --suffix "$RD_OPTION_SUFFIX" --new-project-id "$RD_OPTION_NEW_PROJECT_ID" --flavor_map "$RD_OPTION_FLAVOR_MAP" --new_name "$RD_OPTION_NEW_NAME"
