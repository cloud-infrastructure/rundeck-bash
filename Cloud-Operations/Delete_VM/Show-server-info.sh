#!/bin/bash

# only exec steps if the VM wil be recreated
if [[ $RD_OPTION_RECREATE_FROM != '-' ]]; then
  export OS_PROJECT_NAME=admin
  export OS_REGION_NAME=cern
  export OS_PROJECT_DOMAIN_ID=default
  export OS_IDENTITY_API_VERSION=3
  export OS_AUTH_TYPE=v3fedkerb
  export OS_IDENTITY_PROVIDER=sssd
  export OS_PROTOCOL=kerberos
  export OS_AUTH_URL=https://keystone.cern.ch/v3

  VM_NAME=$RD_OPTION_VM_NAME
  VM_ID=$(openstack server list --all --no-name --name "^${VM_NAME}\$" -f value -c ID)

  echo "[INFO] Listing information about newly recreated VM..."
  openstack server show "$VM_ID"
  echo "[INFO] $VM_NAME is up and running"
fi
