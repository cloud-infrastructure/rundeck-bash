#!/bin/bash

cci-multiple-recreate-parallel -vv --auth sso --vms "$RD_OPTION_VMS" \
--recreate-from "$RD_OPTION_RECREATE_FROM" \
--job-id 2f4daf8f-163e-4167-8d3f-5e9cfc62385b \
--parallel "$RD_OPTION_PARALLEL" \
--delay "$RD_OPTION_DELAY" \
--new_project_id "$RD_OPTION_NEW_PROJECT_ID" \
--volume_type "$RD_OPTION_VOLUME_TYPE" \
--sysprep "$RD_OPTION_SYSPREP" \
--sysprep_params "$RD_OPTION_SYSPREP_PARAMS" \
--sysprep_snapshot "$RD_OPTION_SYSPREP_SNAPSHOT"