#!/bin/bash

export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_AUTH_TYPE=v3kerberos


if [ "$RD_OPTION_ENV" == 'prod' ]; then
  export OS_PROJECT_ID=d749b260-2b53-427a-af5e-7f539007a693
  export OS_PROJECT_NAME="Cloud Test - gva_baremetal_001"
else
  export OS_PROJECT_ID=144b7c3d-df4e-4b45-8434-0817f3c6c135
  export OS_PROJECT_NAME="Cloud Test - gva_baremetal_qa"
fi

export OS_BAREMETAL_API_VERSION=1.31
export IRONIC_API_VERSION=1.31

CMD="cci-enroll-physical-nodes"

if [ "$RD_OPTION_BEHAVIOUR" == 'debug' ]; then
  CMD="$CMD --debug"
else
  CMD="$CMD --dryrun"
fi

if [ "$RD_OPTION_PARALLEL" == 'yes' ]; then
  CMD="$CMD --parallel"
fi

if [ -n "$RD_OPTION_HOSTS" ] ; then
  CMD="$CMD --hosts $RD_OPTION_HOSTS"
fi

if [ -n "$RD_OPTION_HOSTGROUP" ]; then
  CMD="$CMD --hostgroup $RD_OPTION_HOSTGROUP"
fi

echo "$CMD"
${CMD}