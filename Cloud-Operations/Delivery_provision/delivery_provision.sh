#!/bin/bash

if [ ${RD_OPTION_CLOUD} == "cern" ]; then
    echo "[INFO] Using CERN cloud."
    export OS_CLOUD=ironic
    BAREMETAL_PROJECT=d749b260-2b53-427a-af5e-7f539007a693
    PLACEMENT_AGGREGATE_UUID=cd0fa194-4bbd-42bc-bbf2-df2fadf2319d
else
    echo "[INFO] Using PDC cloud."
    export OS_CLOUD=ironic_pdc
    BAREMETAL_PROJECT=8ea82887-5f94-4796-8610-ca5c1b5d861b
    PLACEMENT_AGGREGATE_UUID=6a6a4c44-8321-46da-912a-f2e07d816388
fi
exit_code=0

#
# Collecting the nodes
#
if [[ -n ${RD_FILE_NODES_NAMES} ]]; then
    echo "[INFO] Collecting nodes from the file $RD_FILE_NODES_NAMES."
    sed -i -e '/^[\ ]*$/d' ${RD_FILE_NODES_NAMES}
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group | grep -if ${RD_FILE_NODES_NAMES} > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
else
    echo "[INFO] Collecting nodes from the delivery $RD_OPTION_DELIVERY_ID."
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group| grep -i ${RD_OPTION_DELIVERY_ID} > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
fi
if [ ! -s "/tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt" ]; then
    echo "[ERROR] There are no nodes found."
    exit 1
fi

#
# Setting the resource class
#
echo "[INFO] Setting the resource class."
if [[ -z ${RD_OPTION_FLAVOR_PREFIX} ]]; then
    if [ ${RD_OPTION_ADD_RACK_IN_FLAVOR} == "true" ]; then
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 cci-assign-resource-class-ironic --add_rack --node | sh -x
    else
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 cci-assign-resource-class-ironic --node | sh -x
    fi
else
    if [ ${RD_OPTION_ADD_RACK_IN_FLAVOR} == "true" ]; then
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 cci-assign-resource-class-ironic --add_rack --flavor_prefix ${RD_OPTION_FLAVOR_PREFIX} --node | sh -x
    else
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 cci-assign-resource-class-ironic --flavor_prefix ${RD_OPTION_FLAVOR_PREFIX} --node | sh -x
    fi
fi

#
# Setting the conductor group
#
if [[ $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep group_000 | wc -l) -ne 0 ]]; then
    echo "[INFO] Assigning nodes to conductor groups."
    if [[ -n ${RD_FILE_NODES_NAMES} ]]; then
        echo "[INFO] Collecting nodes without the group set from the file $RD_FILE_NODES_NAMES."
        openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group | grep -if ${RD_FILE_NODES_NAMES} | grep 'group_000' > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
    else
        echo "[INFO] Collecting nodes without the group set from the delivery $RD_OPTION_DELIVERY_ID."
        openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group| grep -i ${RD_OPTION_DELIVERY_ID} | grep 'group_000' > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
    fi
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group | grep -v group_000 | grep group | cut -d '|' -f 7 | sort | uniq -c | sort -n | awk '{print $2}' > /tmp/conductor_groups
    groups_amount=$(cat /tmp/conductor_groups | wc -l)
    nodes_amount=$(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | wc -l)
    group_size=$(( (nodes_amount + groups_amount - 1) / groups_amount ))
    for i in $(seq $groups_amount); do
        conductor_group=$(awk "NR==$i" /tmp/conductor_groups)
        awk "NR >= $(( i * group_size - group_size + 1)) && NR <= $(( i * group_size))" /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt  | awk '{print $4}' | xargs -r -n1 openstack baremetal node set --conductor-group $conductor_group
    done
fi

if [[ -n ${RD_FILE_NODES_NAMES} ]]; then
    echo "[INFO] Recollecting nodes from the file $RD_FILE_NODES_NAMES."
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group | grep -if ${RD_FILE_NODES_NAMES} > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
else
    echo "[INFO] Recollecting nodes from the delivery $RD_OPTION_DELIVERY_ID."
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group| grep -i ${RD_OPTION_DELIVERY_ID} > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
fi
if [[ $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep group_000 | wc -l) -ne 0 ]]; then
    echo "[ERROR] Not all nodes are assigned to the conductor groups. Check manually if conductors are misbehaving."
    exit 1
fi

#
# Setting the driver
#
if [ ${RD_OPTION_DRIVER} == "redfish" ]; then
    if [[ $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep ipmi | wc -l) -ne 0 ]]; then
        echo "[INFO] Setting the driver to redfish." 
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node console disable
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node set --driver redfish --console-interface cern-webconsole-redfish --management-interface redfish --power-interface redfish --vendor-interface no-vendor
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node console enable
    fi
else
    if [[ $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep redfish | wc -l) -ne 0 ]]; then
        echo "[INFO] Setting the driver to ipmi." 
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node console disable
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node set --driver ipmi --console-interface cern-webconsole --management-interface ipmitool --power-interface ipmitool --vendor-interface ipmitool
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node console enable
    fi
fi

#
# Cleaning nodes
#
if [[ $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep -v available | wc -l) -ne 0 ]]; then
    echo "[INFO] Starting the cleaning on the not available nodes and waiting for 20 min."
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep -v available | awk '{print $4}' | xargs -r -n1 openstack baremetal node provide
    sleep 20m
fi

if [[ -n ${RD_FILE_NODES_NAMES} ]]; then
    echo "[INFO] Recollecting nodes from the file $RD_FILE_NODES_NAMES."
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group | grep -if ${RD_FILE_NODES_NAMES} > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
else
    echo "[INFO] Recollecting nodes from the delivery $RD_OPTION_DELIVERY_ID."
    openstack baremetal node list --limit 0 --fields uuid name provision_state resource_class management_interface conductor_group| grep -i ${RD_OPTION_DELIVERY_ID} > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
fi
if [[ $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | wc -l) == $(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt| grep available | wc -l) ]]; then
    echo "[INFO] All nodes are succesfully cleaned. Keep on going!"
else
    echo "[ERROR] Not all nodes are succesfully cleaned. Check manually."
    exit 1
fi

#
# Mapping to the cell (only meyrin cloud)
#
if [ ${RD_OPTION_CLOUD} == "cern" ]; then
    echo "[INFO] Mapping nodes to cells."
    CELL_UUID=715795fc-f18e-4630-bdbf-e8824b0a14e2
    nova_cell=$(ai-foreman --filter cloud_compute/level0/backend/main showhost  | grep master  | head -1 | cut -d '|' -f 2 | tr -d ' ')
    ssh -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 root@"$nova_cell" nova-manage cern discover_ironic_hosts --cell_uuid ${CELL_UUID}
    exit_code=$?
    if [ ${exit_code} -ne 0 ]; then 
        echo "[ERROR] Discovery nodes by nova has failed."
    fi
fi

cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | cut -d '|' -f 2 > /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-UUIDs
echo "[INFO] Adding the resource provider to placement aggregate."
while read i; do GENERATION=$(openstack resource provider show -c generation -f value $i); openstack resource provider aggregate set --generation $GENERATION --aggregate ${PLACEMENT_AGGREGATE_UUID} $i; done < /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-UUIDs

#
# Assign owner if a an owner is set
#
if [ ${RD_OPTION_SET_OWNER} == "true" ]; then
    if [[ $(echo $RD_OPTION_PROJECTS_UUID | wc -w) == 1 ]]; then
        echo "[INFO] Setting the owner of the nodes to $RD_OPTION_PROJECTS_UUID"
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-UUIDs | xargs -n1 openstack baremetal node set --owner $RD_OPTION_PROJECTS_UUID
    elif [[ $(echo $RD_OPTION_PROJECTS_UUID | wc -w) == 0 ]]; then
        echo "[ERROR] Can't set the owner, the project is not specifed."
        exit_code=1
    else
        echo "[ERROR] Can't set the owner because there are more than 1 projects."
        exit_code=2
    fi
fi

#
# Define flavors and assign to target projects
#
NODE=$(head -1 /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | cut -d '|' -f 2)
echo "[INFO] Extracting hardware properties from the node $NODE."
PROPERTIES=$(openstack baremetal node show --fit -c properties -f json ${NODE})
VCPUS=$(echo "$PROPERTIES" | jq -r .properties.cpus)
DISK=$(echo "$PROPERTIES" | jq -r .properties.local_gb)
RAM=$(echo "$PROPERTIES" | jq -r .properties.memory_mb)
ARCH=$(echo "$PROPERTIES" | jq -r .properties.cpu_arch)

readarray -t RC_array < <(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | cut -d '|' -f 5 | sort | uniq)
for RESOURCE_NAME in ${RC_array[@]}; do
    FLAVOR_NAME=$(echo "$RESOURCE_NAME" | sed -E 's/BAREMETAL_([A-Z]1)_([A-Z]{2}.*)_(\w+)_(\w+)_(\w+)/\L\1.\2\E\l.\3-\4-\5/; s/_/-/g')
    if [[ $(openstack flavor list | grep ${FLAVOR_NAME} | wc -l) == 0 ]]; then
        echo "[INFO] Creating a flavor $FLAVOR_NAME with $VCPUS cores, $DISK disk and $RAM ram."
        openstack flavor create --ram ${RAM} --disk ${DISK} --vcpus ${VCPUS} --private $FLAVOR_NAME
        echo "[INFO] Assosiating a flavor $FLAVOR_NAME to the $RESOURCE_NAME."
        openstack flavor set \
                         --property cern:physical=true \
                         --property architecture=$ARCH \
                         --property capabilities:cpu_arch=$ARCH \
                         --property resources:MEMORY_MB=0 \
                         --property resources:VCPU=0 \
                         --property resources:DISK_GB=0 \
                         --property resources:CUSTOM_$RESOURCE_NAME=1 $FLAVOR_NAME
        openstack flavor set --project ${BAREMETAL_PROJECT} ${FLAVOR_NAME}
    else
        echo "[INFO] Flavor $FLAVOR_NAME already exists"
    fi
    if [[ $(echo $RD_OPTION_PROJECTS_UUID | wc -w) == 1 ]]; then
        cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt
        max_instances=$(cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | grep $RESOURCE_NAME | wc -l)
        echo "[INFO] Setting max instances to ${max_instances} for the project ${RD_OPTION_PROJECTS_UUID[0]}."
        for cloud in 'cern' 'next' 'pdc' 'poc'; do OS_CLOUD=$cloud openstack project set --property max-instances_$FLAVOR_NAME=$max_instances ${RD_OPTION_PROJECTS_UUID[0]}; done
    elif [[ $(echo $RD_OPTION_PROJECTS_UUID | wc -w) == 0 ]]; then
        echo "[ERROR] Can't set max instances as there is no project specified"
    else
        echo "[ERROR] Can't set max instances as there is more than 1 project"
    fi
    for project in ${RD_OPTION_PROJECTS_UUID}; do
        echo "[INFO] Assosiating a flavor $FLAVOR_NAME with the project $project."
        openstack flavor set --project $project $FLAVOR_NAME
        if [[ -n ${RD_OPTION_HEP_SCORE_06} ]]; then 
            echo "[INFO] Setting HS06 values."
            for cloud in 'cern' 'next' 'pdc' 'poc'; do OS_CLOUD=$cloud openstack project set --property hs06_$FLAVOR_NAME=$RD_OPTION_HEP_SCORE_06 $project; done
        fi
        if [[ -n ${RD_OPTION_HEP_SCORE_23} ]]; then 
            echo "[INFO] Setting HS23 values."
            for cloud in 'cern' 'next' 'pdc' 'poc'; do OS_CLOUD=$cloud openstack project set --property hs23_$FLAVOR_NAME=$RD_OPTION_HEP_SCORE_23 $project; done   
        fi
    done
done

if [ ${RD_OPTION_SET_RAID} == "None" ]; then
    echo "[INFO] Not setting RAID."
elif [ ${RD_OPTION_SET_RAID} == "RAID-0" ]; then
    echo "[INFO] Setting RAID-0."
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node set --target-raid-config "{\"logical_disks\": [{\"size_gb\": 100, \"controller\": \"software\", \"raid_level\": \"1\"}, {\"size_gb\": \"MAX\", \"controller\":\"software\", \"raid_level\": \"0\"}]}"
    echo "[INFO] Starting the cleaning on the nodes to apply the RAID-0 configuration."
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -r -n1 openstack baremetal node manage
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -r -n1 openstack baremetal node provide
    sleep 20m
else
    echo "[INFO] Setting RAID-1."
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -n1 openstack baremetal node set --target-raid-config "{\"logical_disks\": [{\"size_gb\": 100, \"controller\": \"software\", \"raid_level\": \"1\"}, {\"size_gb\": \"MAX\", \"controller\":\"software\", \"raid_level\": \"1\"}]}"
    echo "[INFO] Starting the cleaning on the nodes to apply the RAID-1 configuration."
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -r -n1 openstack baremetal node manage
    cat /tmp/${RD_OPTION_DELIVERY_ID}-${RD_JOB_EXECID}-list.txt | awk '{print $4}' | xargs -r -n1 openstack baremetal node provide
    sleep 20m
fi


#
# Compute quota for those projects that use baremetal flavors exclusively
#
if [ ${RD_OPTION_SET_QUOTA:-false} == "true" ]; then
    if [[ $(echo $RD_OPTION_PROJECTS_UUID | wc -w) == 0 ]]; then
        echo "[ERROR] Can't set the quota, the project is not specifed."
        exit_code=1
    else
        for project in ${RD_OPTION_PROJECTS_UUID}; do
            echo "[INFO] Computing quota in $project"
            flavor_list=$(openstack project show "${project}" |grep max |cut -d\|  -f2 |tr -d " ")

            total_ram=0
            total_cpu=0
            total_ins=0

            for flavor in ${flavor_list}; do
                flavor_name=$(echo $flavor | cut -d\_ -f2)
                # Check if this flavor exists in this region
                echo "[INFO] Checking if the flavor $flavor_name exists in $RD_OPTION_CLOUD cloud"
                openstack flavor show $flavor_name
                if [ $? -eq 0 ]; then
                    flavor_ram=$(openstack flavor show $flavor_name -f value -c ram)
                    flavor_cpu=$(openstack flavor show $flavor_name -f value -c vcpus)
                    flavor_ins=$(openstack project show "${project}" -f json |jq -r ".[\"${flavor}\"]")
                    flavor_ram_quota=$(( $flavor_ram * $flavor_ins ))
                    flavor_cpu_quota=$(( $flavor_cpu * $flavor_ins ))
                    total_ram=$(( $flavor_ram_quota + $total_ram ))
                    total_cpu=$(( $flavor_cpu_quota + $total_cpu ))
                    total_ins=$(( $flavor_ins + $total_ins ))
                else
                    echo "[INFO] The flavor $flavor_name does not exists in $RD_OPTION_CLOUD cloud"
                fi
            done

            echo "[INFO] Updating quota in $project with ram: $total_ram, cpus: $total_cpu, instances: $total_ins"
            openstack quota set --instances $total_ins --ports $total_ins --ram $total_ram --cores $total_cpu "${project}"
        done
    fi
fi

if [ ${exit_code} -ne 0 ]; then
    echo "[ERROR] Something did not work, please, check the logs."
    exit 1
fi
