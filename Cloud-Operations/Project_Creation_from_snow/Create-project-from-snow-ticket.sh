#!/bin/bash

echo "Executing cci-create-project..."
message="Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"
cci-create-project -vv --"$RD_OPTION_EXECUTION_MODE" --execution-reference "$message" from-snow \
--instance "$RD_OPTION_INSTANCE" --ticket-number "$RD_OPTION_SNOW_TICKET"
exit $?
