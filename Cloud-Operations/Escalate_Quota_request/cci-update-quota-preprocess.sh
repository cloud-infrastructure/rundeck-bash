#!/bin/bash

cci-update-quota --"$RD_OPTION_BEHAVIOUR" --instance "$RD_OPTION_INSTANCE" --ticket-number "$RD_OPTION_SNOW_TICKET" preprocess --fe "$RD_OPTION_FUNCTIONAL_ELEMENT" --assignment-group "$RD_OPTION_ASSIGNMENT_GROUP"

CCI_UPDATE_EXIT=$?

if [ $CCI_UPDATE_EXIT -eq 0 ]; then
  if [ "$RD_OPTION_BEHAVIOUR" == 'perform' ]; then
    echo "[INFO] $RD_OPTION_SNOW_TICKET worknote updated"
    echo "[INFO] $RD_OPTION_SNOW_TICKET sucesfully assigned to $RD_OPTION_FUNCTIONAL_ELEMENT ($RD_OPTION_ASSIGNMENT_GROUP)"
  else
    echo "[DRYRUN] Please verify ticket values before running it in perform mode."
  fi
fi

exit $CCI_UPDATE_EXIT