#!/bin/bash

echo "Executing cci-jira-ticket-cleanup..."

if [ "$RD_OPTION_JIRA_DRYRUN" == 'False' ]; then
  RD_OPTION='--perform'
else
  RD_OPTION='--dryrun'
fi

cci-jira-ticket-cleanup -v --jql "${RD_OPTION_JIRA_JQL}" "${RD_OPTION}"
exit $?
