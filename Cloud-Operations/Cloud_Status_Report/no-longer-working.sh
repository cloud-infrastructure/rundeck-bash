#!/bin/bash

# This job is no longer working and therefore it will be stopped
# More info: https://its.cern.ch/jira/browse/OS-3425

# source the openrc
echo "[INFO] Exporting env variables from $RD_OPTION_OPENRC..."
source "$RD_OPTION_OPENRC"

echo "[INFO] Executing embedded perl script..."
OUTPUT=$(/usr/bin/perl -x "$0")

# shellcheck disable=SC2217
echo -n << '__END__' > /dev/null 2>&1
#!/usr/bin/perl -wl

package OpenStack::Project;

sub List(;$){
    my $filter = shift @_;
    my %project = ();
    if (open(KS, "/usr/bin/openstack project list --domain default --column Name --column ID 2>/dev/null |")){
        while(<KS>){
            next if /^\+/;
            s/\s*\|\s*/\|/g;
            my (undef,$id,$name) = split(/\|/,$_);
            next if $id eq "ID";

            if (not defined $filter){
            $project{$id} = $name;
            next;
            }

            if ($name =~ /$filter/){
            $project{$id} = $name;
            next;
            }

            if ($id =~ /$filter/){
            $project{$id} = $name;
            next;
            }

    }
    close(KS);
}
return %project;
}

package OpenStack::Nova;

sub QuotaShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack quota show $opt";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Quota";
	$list{$key} = $value;
    }
    close(F);

    return %list;
}

sub AbsoluteLimits($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/nova --os-auth-url 'https://keystone.cern.ch/main/v2.0/' absolute-limits $opt";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Name";
	$list{$key} = $value;
    }
    close(F);

    return %list;
}

sub HypervisorShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack hypervisor show $opt";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        #print;
        next if /^\+/;
	s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Name";
	$list{$key} = $value;
    }
    close(F);

    return %list;
}

sub FlavorList(){
    my $cmd = "/usr/bin/openstack flavor list --all";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }
    my %list = ();
    my $instances = my $cores = my $ram = undef;
    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($id,$name,$ram,$disk,$ephemeral,$swap,$cores) = (split(/\|/,$_))[1,2,3,4,5,6,7,8];
        next if $name =~  /^Name/;
        %{$list{$name}} =
            (id        => $id,
	     ram       => $ram,
             disk      => $disk,
             ephemeral => ($ephemeral || 0),
             swap      => ($swap || 0),
             cores     => $cores,
            );

        #print "$name,$ram,$disk,$ephemeral,$swap,$cores\n";exit;
    }
    close(F);
    return %list;
}

sub CellList(){
    my %cell = ();
    my $cmd = "/usr/bin/openstack compute service list --service nova-cells";
    if (not open(NOVA,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    while(<NOVA>){
	next if /^\+/;
	s/\s+//g;
	my ($service,$host,$status,$state) = (split(/\|/,$_))[2,3,5,6];
	next if not defined $service;
	next if $host eq "Host";	# skip header
	if ($service ne "nova-cells"){ # older version of phython-novaclient
	    ($host,$status,$state) = (split(/\|/,$_))[2,4,5];
	    next if $host eq "Host";	# skip header
	}
	next if $status ne "enabled";
	next if $state  ne "up";
	next if $host !~ /^father\!(cell\d+)@/;
	$cell{$1}++;
    }
    close(NOVA);

    return sort keys %cell;
}

sub BEGIN {
    use Cwd 'abs_path';
    use File::Basename 'dirname';
    push(@INC,dirname(abs_path($0)));
}

use strict;
use diagnostics;
use Data::Dumper;
use Getopt::Long;
use Text::TabularDisplay;
#use OpenStack::Nova;
#use OpenStack::Project;

#
# Check environment
#
if (not exists $ENV{OS_USERNAME}){
    print STDERR "[ERROR] No environment variable \"\$OS_USERNAME\" found\n";
    exit 1;
}

my $debug = my $verbose = my $summary = 0;
my $shared = my $personal = 0;
my %opts = (
    debug    => \$debug,
    verbose  => \$verbose,
    summary  => \$summary,
    shared   => \$shared,
    personal => \$personal,
    );
#
# Parse the options
#
my $rc = Getopt::Long::GetOptions(
    \%opts,
    "debug","verbose",
    "summary",
    "shared",
    "personal",
    #"help"          => sub { HelpMessage() },
    );

#my @cell = @ARGV;
#if (not @cell){
#    Usage("Mandatory parameters \"cell01 cell02 ...\" missing");
#    exit 1;
#}

if (not $shared and not $personal){
    $shared = $personal = 1;
}

#
# Get cell data
#
my @cell =  OpenStack::Nova::CellList();
#@cell =qw(cell02 cell06 cell09);
print "[D] Found ".scalar(@cell)." cells, \@cell \"@cell\"\n" if $debug;

#
# Get flavor data
#
print "[VERB] Get all flavors...\n" if $verbose;
my %flavor = OpenStack::Nova::FlavorList();
map {$flavor{$_}{ram} /= 1024} keys %flavor;
map {%{$flavor{$flavor{$_}{id}}} = %{$flavor{$_}}} keys %flavor;
if (scalar(keys %flavor) == 0){
    print STDERR "[ERROR] No Nova flavors found.\n";
    exit 1;
}
print "[VERB] Found ".scalar(keys %flavor)." Nova flavors\n" if $verbose;

print "[DEBUG] " . Dumper(\%flavor) if $debug;
#exit;

#
# project names
#
print "[VERB] Get all projects...\n" if $verbose;
my $filter = undef; # XXX
my %project = OpenStack::Project::List($filter);

%project = OpenStack::Project::List($filter) if scalar(keys %project) == 0; # retry (just in case)

if (scalar(keys %project) == 0){
    print STDERR "[ERROR] No Keystone projects found.\n";
    exit 1;
}
print "[VERB] Found ".scalar(keys %project)." Keystone projects\n" if $verbose;

print "[DEBUG] " . Dumper(\%project) if $debug;

my $table = Text::TabularDisplay->new("Project","Instances","Cores","RAM");
my %total = my %data = ();

for my $cell (@cell){
    my $nova = "/usr/bin/nova --os-auth-url 'https://keystonecell.cern.ch/main/v2.0' --os-region-name $cell";

    my $cmd = "$nova list --all-tenants 1 --fields name,user_id,tenant_id,flavor";
    print "[D] Cell $cell - getting \%osdata using \"$cmd\"\n" if $debug;

    open(NOVA,"$cmd 2>/dev/null |") or die "aargh";
    while(<NOVA>){
	next unless my ($id,$vm,$owner,$project_id,$flavor_id) = /^\|\s+(\S+)\s+\|\s+(.*?)\s+\|\s+(\S+)\s+\|\s+(\S+)\s+\|\s+(\d+)\s+\|\s*$/;
	next if $id eq "id";
#	print ">>> ($vm,$flavor,$project_id,$owner - $project{$project_id}\n";
#    (node_name, number_of_cores, amount_of_memory,  associated_project/tenant,  owner )
	my $project = $project{$project_id};
#	print ">> $flavor_id --- \n" if not exists $flavor{$flavor_id};
	if ($summary){
	    $data{$project}{instances}++;
	    $data{$project}{cores} += $flavor{$flavor_id}{cores};
	    $data{$project}{ram}   += $flavor{$flavor_id}{ram};
	}else{
	    print join(",",$vm,$flavor{$flavor_id}{cores},$flavor{$flavor_id}{ram},$project,$owner);
	}
    }
    close(NOVA);
}

#
# Summary
#
if ($summary){
    for my $project (sort {$data{$b}{cores} <=> $data{$a}{cores}} keys %data){
	next if $project =~ /^Personal / and not $personal;
	next if $project !~ /^Personal / and not $shared;
	$data{$project}{ram} = sprintf("%d",$data{$project}{ram});
	$table->add($project,map {$data{$project}{$_}} qw(instances cores ram));
	map {$total{$_} += $data{$project}{$_}} qw(instances cores ram);
    }
}

if ($summary){
    $table->add("TOTAL",$total{instances},$total{cores},$total{ram});
    print $table->render . "\n";
}

__END__

if [ -n "$OUTPUT" ]; then
  if echo -e "$OUTPUT" | mailx -r noreply@cern.ch -s "$RD_OPTION_SUBJECT" "$RD_OPTION_MAIL_TO"; then
    echo "[INFO] email sent to $RD_OPTION_MAIL_TO"
  fi
else
  echo -e "Ooops something went wrong getting the data from the Cloud" | mailx -r noreply@cern.ch -s "Failed execution: $RD_OPTION_SUBJECT" "$RD_OPTION_MAIL_TO"
  echo "[ERROR] Script failed. Please check logs"
  exit 1
fi
