#!/bin/bash

echo "Executing cci-escalate-project..."
cci-escalate-project -vv --"$RD_OPTION_EXECUTION_MODE" \
--fe "$RD_OPTION_FUNCTIONAL_ELEMENT" --assignment-group "$RD_OPTION_ASSIGNMENT_GROUP" \
--instance "$RD_OPTION_INSTANCE" --ticket-number "$RD_OPTION_SNOW_TICKET"
exit $?