#!/bin/bash

echo "Executing cci-accounting-usage..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

cci-accounting-usage -vv \
  --"$RD_OPTION_BEHAVIOUR" \
  --to "$RD_OPTION_TO" \
  --subject "$RD_OPTION_SUBJECT" \
  --body "$RD_OPTION_BODY"

  exit $?
