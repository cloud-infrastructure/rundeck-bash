#!/bin/bash

echo "Executing Report resource usage per project..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL . Please check Rundeck's log."

# Download the script to execute it locally
SCRIPT_URL="https://gitlab.cern.ch/cloud-infrastructure/cci-scripts/-/raw/master/reports/create-projects-accounting-reports.pl"
DOWNLOAD_FOLDER="/tmp"
FILE=/tmp/create-projects-accounting-reports.pl
OUTPUT=/tmp/cloud_report.txt
OPTIONS=""
# Set comma as delimiter for chargegroups list
IFS=','

kinit -kt /etc/rundeck/svcrdeck.keytab svcrdeck@CERN.CH
source /var/lib/rundeck/data/openrc
wget -q -N -P "$DOWNLOAD_FOLDER" "$SCRIPT_URL"

if [ -n "$RD_OPTION_CHARGEGROUP" ]; then
  if [ "$RD_OPTION_CHARGEGROUP" == 'ALL' ]; then
    OPTIONS+="--charge-group '' "
  else
    for chargegroup in $RD_OPTION_CHARGEGROUP
    do
      OPTIONS+="--charge-group \"$chargegroup\" "
    done
  fi
fi

if [ -n "$RD_OPTION_ORGUNIT" ]; then
  if [ "$RD_OPTION_ORGUNIT" == 'ALL' ]; then
    OPTIONS+="--orgunit '' "
  else
    OPTIONS+="--orgunit \"$RD_OPTION_ORGUNIT\" "
  fi
fi

if [ "$RD_OPTION_ALL_FUNCTIONAL_ELEMENTS" == 'yes' ]; then
  OPTIONS+="--all-functional-elements "
fi

if [ "$RD_OPTION_EXCLUDE_PERSONAL_PROJECTS" == 'yes' ]; then
  OPTIONS+="--exclude-personal-projects "
fi


if [ "$RD_OPTION_MATCHING" == 'yes' ]; then
  OPTIONS+="--matching "
fi

if [ "$RD_OPTION_SHOW_STORAGE" == 'yes' ]; then
  OPTIONS+="--show-storage "
fi

if [ "$RD_OPTION_DEBUG" == 'yes' ]; then
  OPTIONS+="--debug "
fi

if [ "$RD_OPTION_VERBOSE" == 'yes' ]; then
  OPTIONS+="--verbose "
fi

CMD="perl -w ${FILE} ${OPTIONS}"

if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
  echo "[INFO] Running '${CMD}'..."
  if [ "$RD_OPTION_PRINT_OUTPUT" == 'yes' ]; then
    # Print output and redirect it to $OUTPUT
    /bin/bash -c "$CMD" 2>&1 | tee $OUTPUT
  else
    # Only redirect to $OUTPUT
    # shellcheck disable=SC2069
    /bin/bash -c "$CMD" 2>&1 > $OUTPUT
  fi
  if [ -s "$OUTPUT" ]; then
    echo "[INFO] Sending Report to \"$RD_OPTION_MAIL_TO\" ..."
    # shellcheck disable=SC2086
    echo "$RD_OPTION_MAIL_BODY" | mail -r "$RD_OPTION_MAIL_FROM" -s \"$RD_OPTION_SUBJECT\" -a "$OUTPUT" "$RD_OPTION_MAIL_TO"
  else
    echo "[ERROR] File $OUTPUT not found"
  fi
else
  echo "[DRYRUN] Running: /bin/bash -c $CMD > $OUTPUT ... No email will be sent"
  # Run the script without sending the output by email
  /bin/bash -c "$CMD"
  echo "[DRYRUN] echo \"$RD_OPTION_MAIL_BODY\" | mail -r \"$RD_OPTION_MAIL_FROM\" -s \"$RD_OPTION_SUBJECT\" -a \"$OUTPUT\" \"$RD_OPTION_MAIL_TO\""
fi

exit $?
