#! /bin/bash

# Identify compute nodes that host VMs in a 'deleting' task state,
# and restart nova-compute to unblock such cases

# Set up the environment for the svckey account

source "${RD_OPTION_OPENRC}"

# get all VMs from those cells
echo Listing of all VMs in deleting state...

TMPFILE=$(mktemp)

for cell in $CELLS ; do
    echo Processing "${cell}"...
    nova list --all-tenants --status deleting --fields name,status,task_state,power_state,host >> "${TMPFILE}" 2>&1
done

stuck=$(grep -w deleting "${TMPFILE}" | awk '{print $12}' | sort | uniq | grep -cvw None)

if [ "${stuck}" == 0 ]
then
    echo "No VMs in 'deleting' task state found. Nothing to do! "
else
    echo "VMs in 'deleting' task state:"
    grep -w deleting "${TMPFILE}" | grep -vw None

    # restart nova-compute on the compute nodes hosting 'deleting' VMs
    echo Restart nova-compute service on the compute nodes hosting \'deleting\' VMs
    HOSTS=$(grep -w deleting "${TMPFILE}" | awk '{print $12}' | sort | uniq | grep -vw None)
    pssh -H "$HOSTS" -l root -O StrictHostKeyChecking=no -t 0 "service openstack-nova-compute restart"
fi

/bin/rm "${TMPFILE}"
exit