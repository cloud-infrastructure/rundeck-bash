#!/bin/bash

HOSTS=$(ai-foreman -f "${RD_OPTION_HOSTGROUP}" --no-header -z Name showhost | sed -e '1d' -e '$d' | cut -d'|' -f2)
pssh -H "$HOSTS" -l root -O StrictHostKeyChecking=no -t 0 "systemctl restart shibd.service"