#!/bin/bash

OPTIONS=""

if [ "$RD_OPTION_FORCE_COLD" == 'yes' ]; then
  OPTIONS="--force-cold $OPTIONS"
fi

if [ "$RD_OPTION_BEHAVIOUR" == 'dryrun' ]; then
  OPTIONS="--dryrun $OPTIONS"
fi

if [ "$RD_OPTION_TARGET" != "" ]; then
  OPTIONS="--target $RD_OPTION_TARGET $OPTIONS"
fi



if vmmigrator "$OPTIONS" --source "$RD_OPTION_SOURCE" --vms "$RD_OPTION_VMS" --mp "$RD_OPTION_MP" --mph "$RD_OPTION_MPH"; then
  echo "ERROR:rundeck:vmmigrator did not finish successfully"
  exit 1
fi