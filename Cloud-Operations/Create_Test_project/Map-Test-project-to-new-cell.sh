#!/bin/bash

echo "[INFO] Mapping 'Cloud Test - ${RD_OPTION_CELL}' project to new cell '${RD_OPTION_CELL}'..."

source /var/lib/rundeck/data/openrc

openstack project set --property cells_mapping="${RD_OPTION_CELL}" "Cloud Test - ${RD_OPTION_CELL}"
exit $?