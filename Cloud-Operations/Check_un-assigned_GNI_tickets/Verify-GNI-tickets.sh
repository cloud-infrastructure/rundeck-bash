#!/bin/bash

echo "Executing cci-verify-gni-tickets..."
cci-verify-gni-tickets -vv --reference "$RD_JOB_URL"  \
--group "$RD_OPTION_GROUP" --assignee "$RD_JOB_USERNAME" \
--instance "$RD_OPTION_INSTANCE"
exit $?