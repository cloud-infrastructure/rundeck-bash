#!/bin/bash

VM_NAME=$1

OPENRC_PATH='/var/lib/rundeck/data/openrc'

VOLUME_MAX_TIMEOUT=36000 #10h
VOLUME_RETRY_INTERVAL=300
RUNDECK_ID=$RD_OPTION_RUNDECKID

source_openrc() {
  echo "[INFO] Unsetting all 'OS_' environment variables..."
  unset "$(env | grep OS_ | cut -d= -f 1)"

  if [[ ${1,,} == 'v2' ]]; then
    echo "[INFO] Exporting variables stored in $OPENRC_PATH..."
	source $OPENRC_PATH
	export OS_VOLUME_API_VERSION=2
  else
    echo "[INFO] Exporting default v3kerberos variables..."
    export OS_PROJECT_NAME=admin
    export OS_PROJECT_DOMAIN_ID=default
    export OS_IDENTITY_API_VERSION=3
    export OS_AUTH_TYPE=v3kerberos
    export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
    export OS_VOLUME_API_VERSION=2
  fi
}

timestamp() {
  date +%s
}

get_snapshot_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_snapshot_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack image show $1..." 
    openstack image show "$1"
  fi
}

get_vm_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_vm_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack server list --all-p --name $1..." 
    openstack server list --all-p --name "^$1$"
  fi
}

get_volume_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VOLUME name to 'get_volume_info'. Exiting...."
    exit 1
  else
    #echo "[INFO] Executing: openstack volume show $1..." 
    openstack volume show "$1"
  fi
}

wait_for_volume() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VOLUME name"
    exit 1
  fi

  TIMESTAMP=$(timestamp)
  TIMEOUT=$( $TIMESTAMP + $VOLUME_MAX_TIMEOUT )

  while ( $TIMESTAMP < "$TIMEOUT" ); do
  
    VOLUME_INFO=$(get_volume_info "$1")
    VOLUME_STATUS=$( echo "$VOLUME_INFO" | grep -wF '| status ' | awk '{print $4}' )
  
    if [[ ${VOLUME_STATUS,,} == 'available' ]]; then
      echo "[INFO] $1 has been CREATED successfully"
      return
    else
      echo "[INFO] Volume $1 not available yet (current: $VOLUME_STATUS). Sleeping for $VOLUME_RETRY_INTERVAL seconds before checking again..." 
      sleep $VOLUME_RETRY_INTERVAL
    fi
  done

  echo "Volume $1 not AVAILABLE after $VOLUME_MAX_TIMEOUT seconds. Exit"
  exit 1
}

# openrc as admin
source_openrc

VM_INFO=$(get_vm_info "$VM_NAME")
VM_ID=$( echo "$VM_INFO" | grep -iwF " $VM_NAME " | awk '{print $2}' )
VM_DETAILED_INFO=$( openstack server show "$VM_ID")
PROJECT_ID=$( echo "$VM_DETAILED_INFO" | grep project_id | awk '{print $4}')

SNAPSHOT_NAME="${VM_NAME,,}_snap_$RUNDECK_ID"

SNAPSHOT_INFO=$(get_snapshot_info "$SNAPSHOT_NAME")
SNAPSHOT_ID=$( echo "$SNAPSHOT_INFO" | grep -wF "| id" | awk '{print $4}')
SNAPSHOT_SIZE=$( echo "$SNAPSHOT_INFO" | grep -wF "| size" | awk '{print $4}')
SNAPSHOT_MIN_DISK=$( echo "$SNAPSHOT_INFO" | grep -wF "| min_disk" | awk '{print $4}')

FLAVOR=$( echo "$VM_DETAILED_INFO" | grep -wF "| flavor" | awk '{print $4}')
FLAVOR_SIZE=$(openstack flavor list --all | grep -wF "| $FLAVOR " | awk '{print $8}')

TO_GB=$(( 1024*1024*1024 ))
# Round up result. Trick found here http://stackoverflow.com/a/24253318/333348
SNAPSHOT_SIZE=$(( (SNAPSHOT_SIZE + TO_GB - 1) / TO_GB ))

# Get largest between SNAPSHOT_MIN_DISK & FLAVOR_SIZE & SNAPSHOT_SIZE
VOLUME_SIZE=$(printf "%s\n" "$SNAPSHOT_MIN_DISK" "$FLAVOR_SIZE" "$SNAPSHOT_SIZE" | sort -n -r | head -n 1)

# openstackclient liberty does not provide command to update image properties. Using glanceclient (v2) instead
source_openrc v2
echo "[INFO] Changing image properties..."
glance image-update --property "hypervisor_type=qemu" --property "hw_cpu_max_sockets=2" --min-disk 0 "$SNAPSHOT_ID"
# end of setting image properties block

source_openrc
unset OS_PROJECT_NAME
export OS_PROJECT_ID=$PROJECT_ID

VOLUME_NAME="${VM_NAME,,}_vol_$RUNDECK_ID"
echo "[INFO] Creating volume..."
openstack volume create --size "$VOLUME_SIZE" --image "$SNAPSHOT_ID" "$VOLUME_NAME"
wait_for_volume "$VOLUME_NAME"
