#!/bin/bash

VM_NAME=$1

OPENRC_PATH='/var/lib/rundeck/data/openrc'

SNAPSHOT_MAX_TIMEOUT=36000 #10h
SNAPSHOT_RETRY_INTERVAL=300
RUNDECK_ID=$RD_OPTION_RUNDECKID

# shellcheck disable=2120
source_openrc() {
  echo "[INFO] Unsetting all 'OS_' environment variables..."
  unset "$(env | grep OS_ | cut -d= -f 1)"

  if [[ ${1,,} == 'v2' ]]; then
    echo "[INFO] Exporting variables stored in $OPENRC_PATH..."
	source $OPENRC_PATH
  else
    echo "[INFO] Exporting default v3kerberos variables..."
    export OS_PROJECT_NAME=admin
    export OS_PROJECT_DOMAIN_ID=default
    export OS_IDENTITY_API_VERSION=3
    export OS_AUTH_TYPE=v3kerberos
    export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
  fi
}

timestamp() {
  date +%s
}

get_snapshot_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_snapshot_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack image show $1..." 
    openstack image show "$1"
  fi
}

get_vm_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_vm_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack server list --all-p --name $1..." 
    openstack server list --all-p --name "^$1$"
  fi
}

wait_for_snapshot() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one SNAPSHOT name to 'wait_for_snapshot'. Exiting..."
    exit 1
  fi

  TIMESTAMP=$(timestamp)
  TIMEOUT=$( $TIMESTAMP + $SNAPSHOT_MAX_TIMEOUT )

  while ( $TIMESTAMP < "$TIMEOUT" ); do
  
    SNAPSHOT_INFO=$(get_snapshot_info "$1")
    SNAPSHOT_STATUS=$( echo "$SNAPSHOT_INFO" | grep '| status ' | awk '{print $4}' )
  
    if [[ ${SNAPSHOT_STATUS,,} == 'active' ]]; then
      echo "[INFO] $1 has been created successfully"
      return
    else
      echo "[INFO] Snapshot $1 not ACTIVE yet (current: $SNAPSHOT_STATUS). Sleeping for $SNAPSHOT_RETRY_INTERVAL seconds before checking again..." 
      sleep $SNAPSHOT_RETRY_INTERVAL
    fi
  done

  echo "[ERROR] Snapshot $1 not ACTIVE after $SNAPSHOT_MAX_TIMEOUT seconds. Exit"
  exit 1
}

# openrc as admin
# shellcheck disable=2119
source_openrc

VM_INFO=$(get_vm_info "$VM_NAME")
VM_ID=$( echo "$VM_INFO" | grep -iwF " $VM_NAME " | awk '{print $2}' )
VM_DETAILED_INFO=$( openstack server show "$VM_ID")
PROJECT_ID=$( echo "$VM_DETAILED_INFO" | grep project_id | awk '{print $4}')

SNAPSHOT_NAME="${VM_NAME,,}_snap_$RUNDECK_ID"

echo "[INFO] Checking existing snapshot..."

unset OS_PROJECT_NAME
export OS_PROJECT_ID=$PROJECT_ID

wait_for_snapshot "$SNAPSHOT_NAME"
