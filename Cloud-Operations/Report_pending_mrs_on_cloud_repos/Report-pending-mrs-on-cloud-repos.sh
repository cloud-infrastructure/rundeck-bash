#!/bin/bash

echo "Executing cci-report-gitlab..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

# shellcheck disable=SC2086
cci-report-gitlab -vv \
  --"$RD_OPTION_BEHAVIOUR" \
  $RD_OPTION_FILTER \
  --to "$RD_OPTION_TO" \
  --subject "$RD_OPTION_SUBJECT" \
  --body "$RD_OPTION_BODY"

  exit $?
