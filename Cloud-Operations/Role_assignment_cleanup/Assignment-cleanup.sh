#!/bin/bash

echo "Executing cci-assignment-cleanup..."
cci-assignment-cleanup -vv --missing --personal --clean
exit $?
