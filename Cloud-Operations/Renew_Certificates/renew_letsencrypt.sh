#!/bin/bash
declare -A cert_path=(
["openstacknext.cern.ch"]="/etc/letsencrypt/live/openstacknext.cern.ch/"
["openstackpdc.cern.ch"]="/etc/letsencrypt/live/openstackpdc.cern.ch/"
["openstackpoc.cern.ch"]="/etc/letsencrypt/live/openstackpoc.cern.ch/"
["keystonepdc.cern.ch"]="/etc/letsencrypt/live/keystonepdc.cern.ch/"
["keystonepoc.cern.ch"]="/etc/letsencrypt/live/keystonepoc.cern.ch/"
["keystonenext.cern.ch"]="/etc/letsencrypt/live/keystonenext.cern.ch/"
)

declare -A bundle_tbag_hg=(
["openstacknext.cern.ch"]="cloud_infra/loadbalancer/openstacknext"
["openstackpdc.cern.ch"]="cloud_infra/loadbalancer/openstackpdc"
["openstackpoc.cern.ch"]="cloud_infra/loadbalancer/openstackpoc"
["keystonepdc.cern.ch"]="cloud_infra/loadbalancer/keystonepdc"
["keystonepoc.cern.ch"]="cloud_infra/loadbalancer/keystonepoc"
["keystonenext.cern.ch"]="cloud_infra/loadbalancer/keystonenext"
)

declare -A bundle_tbag_key=(
["openstacknext.cern.ch"]="openstacknext_cert_bundle"
["openstackpdc.cern.ch"]="openstackpdc_cert_bundle"
["openstackpoc.cern.ch"]="openstackpoc_cert_bundle"
["keystonepdc.cern.ch"]="keystonepdc_cert_bundle"
["keystonepoc.cern.ch"]="keystonepoc_cert_bundle"
["keystonenext.cern.ch"]="keystonenext_cert_bundle"
)

declare -A standalone_tbag_hg=(
["openstacknext.cern.ch"]="cloud_dashboard/backend/next cloud_dashboard/maintenance/next"
["openstackpdc.cern.ch"]="cloud_dashboard/backend/pdc cloud_dashboard/maintenance/pdc"
["openstackpoc.cern.ch"]="cloud_dashboard/backend/poc cloud_dashboard/maintenance/poc"
["keystonepoc.cern.ch"]="cloud_identity/backend/poc"
["keystonepdc.cern.ch"]="cloud_identity/backend/pdc"
["keystonenext.cern.ch"]="cloud_identity/backend/next"
)

declare -A standalone_cacert_tbag_key=(
["openstacknext.cern.ch"]="horizon_ssl_ca_certs"
["openstackpdc.cern.ch"]="horizon_ssl_ca_certs"
["openstackpoc.cern.ch"]="horizon_ssl_ca_certs"
["keystonepoc.cern.ch"]="keystone_ssl_ca_certs"
["keystonepdc.cern.ch"]="keystone_ssl_ca_certs"
["keystonenext.cern.ch"]="keystone_ssl_ca_certs"
)

declare -A standalone_cert_tbag_key=(
["openstacknext.cern.ch"]="horizon_ssl_certfile"
["openstackpdc.cern.ch"]="horizon_ssl_certfile"
["openstackpoc.cern.ch"]="horizon_ssl_certfile"
["keystonepoc.cern.ch"]="keystone_ssl_certfile"
["keystonepdc.cern.ch"]="keystone_ssl_certfile"
["keystonenext.cern.ch"]="keystone_ssl_certfile"
)

declare -A standalone_key_tbag_key=(
["openstacknext.cern.ch"]="horizon_ssl_keyfile"
["openstackpdc.cern.ch"]="horizon_ssl_keyfile"
["openstackpoc.cern.ch"]="horizon_ssl_keyfile"
["keystonepoc.cern.ch"]="keystone_ssl_keyfile"
["keystonepdc.cern.ch"]="keystone_ssl_keyfile"
["keystonenext.cern.ch"]="keystone_ssl_keyfile"
)

for key in "${!cert_path[@]}"; do
  echo "Creating temporary folder for $key if needed"
  /usr/bin/mkdir -p "/tmp/tls-renewal/$key/"

  echo "Fetching certificate on $key"
  /usr/bin/scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "root@$RD_OPTION_RENEWAL_HOST:${cert_path[$key]}*.pem"    "/tmp/tls-renewal/$key/"

  echo "Verifying certificates"
  if ! /usr/bin/openssl rsa  -in  "/tmp/tls-renewal/$key/privkey.pem" -check -noout; then
    echo "Private key for $key is not valid"
    continue
  fi

  if ! /usr/bin/openssl x509 -in "/tmp/tls-renewal/$key/cert.pem" -noout; then
    echo "Certificate for $key is not valid"
    continue
  fi

  MD5_KEY=$(/usr/bin/openssl x509 -noout -modulus -in "/tmp/tls-renewal/$key/cert.pem"     | /usr/bin/openssl md5)
  MD5_CERT=$(/usr/bin/openssl rsa  -noout -modulus -in "/tmp/tls-renewal/$key/privkey.pem" | /usr/bin/openssl md5)
  if [ "$MD5_KEY" != "$MD5_CERT" ]; then
    echo "MD5 for $key does not match"
    continue
  fi

  # Check that the certificate is not going to expire in less than 20 days
  if ! /usr/bin/openssl x509 -checkend 1728000 -noout -in "/tmp/tls-renewal/$key/cert.pem"; then
    echo "Certificate for $key is noy going to expire in 20 days"
    continue
  fi

  if [ -n "${bundle_tbag_hg[$key]}" ]; then
    # The key is used in a bundle in haproxy
    echo "Generating new bundle for $key"
    /usr/bin/cat "/tmp/tls-renewal/$key/cert.pem" "/tmp/tls-renewal/$key/privkey.pem" "/tmp/tls-renewal/$key/chain.pem" > "/tmp/tls-renewal/$key/bundle_new.pem"
    echo "Fetching old bundle from teigi for $key, iterate over hostgroups if there is more than one"
    read -r -a hostgroups <<< "${bundle_tbag_hg[$key]}"
    for hostgroup in "${hostgroups[@]}"; do
      tbag show --hg "${hostgroup}" "${bundle_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/bundle_${hostgroup//\//_}_old.pem" > /dev/null
      if ! /usr/bin/diff "/tmp/tls-renewal/$key/bundle_${hostgroup//\//_}_old.pem" "/tmp/tls-renewal/$key/bundle_new.pem" -s -q; then
        echo "Storing new secret for bundle $key in tbag in $hostgroup"
        tbag set --hg "${hostgroup}" "${bundle_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/bundle_new.pem"
      fi
    done
  fi

  if [ -n "${standalone_tbag_hg[$key]}" ]; then
    # The key is used in a bundle in apache
    echo "Fetching old ca from teigi for $key, iterate over hostgroups if there is more than one"
    read -r -a hostgroups <<< "${standalone_tbag_hg[$key]}"
    for hostgroup in "${hostgroups[@]}"; do
      tbag show --hg "${hostgroup}" "${standalone_cacert_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/chain_${hostgroup//\//_}_old.pem" > /dev/null
      if ! /usr/bin/diff "/tmp/tls-renewal/$key/chain_${hostgroup//\//_}_old.pem" "/tmp/tls-renewal/$key/chain.pem" -s -q; then
        echo "Storing new secret chain $key in tbag in $hostgroup"
        tbag set --hg "${hostgroup}" "${standalone_cacert_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/chain.pem"
      fi
      echo "Fetching old cert from teigi for $key in $hostgroup"
      tbag show --hg "${hostgroup}" "${standalone_cert_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/cert_${hostgroup//\//_}_old.pem" > /dev/null
      if ! /usr/bin/diff "/tmp/tls-renewal/$key/cert_${hostgroup//\//_}_old.pem" "/tmp/tls-renewal/$key/cert.pem" -s -q; then
        echo "Storing new secret cert $key in tbag in $hostgroup"
        tbag set --hg "${hostgroup}" "${standalone_cert_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/cert.pem"
      fi
      echo "Fetching old key from teigi for $key in $hostgroup"
      tbag show --hg "${hostgroup}" "${standalone_key_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/privkey_${hostgroup//\//_}_old.pem" > /dev/null
      if ! /usr/bin/diff "/tmp/tls-renewal/$key/privkey_${hostgroup//\//_}_old.pem" "/tmp/tls-renewal/$key/privkey.pem" -s -q; then
        echo "Storing new secret key $key in tbag in $hostgroup"
        tbag set --hg "${hostgroup}" "${standalone_key_tbag_key[$key]}" --file "/tmp/tls-renewal/$key/privkey.pem"
      fi
    done
  fi

  echo "Removing temporary directory for $key"
  /usr/bin/rm -rf "/tmp/tls-renewal/$key"
done
