#!/bin/bash

VM_NAME=$RD_OPTION_VM_NAME

SHUTDOWN_MAX_TIMEOUT=$RD_OPTION_SHUTDOWN_TIMEOUT
SHUTDOWN_RETRY_INTERVAL=$RD_OPTION_SHUTDOWN_RETRY_INTERVAL

source_openrc() {
  echo "[INFO] Unsetting all 'OS_' environment variables..."
  while read -r var; do unset "$var"; done < <(env | grep OS_ | cut -d= -f 1)

  echo "[INFO] Exporting default v3kerberos variables..."
  export OS_PROJECT_NAME=admin
  export OS_REGION_NAME=cern
  export OS_PROJECT_DOMAIN_ID=default
  export OS_IDENTITY_API_VERSION=3
  export OS_AUTH_TYPE=v3fedkerb
  export OS_IDENTITY_PROVIDER=sssd
  export OS_PROTOCOL=kerberos
  export OS_AUTH_URL=https://keystone.cern.ch/v3
}

timestamp() {
  date +%s
}

get_vm_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_vm_info'. Exiting..."
    exit 1
  else
    #echo "[INFO] Executing: openstack server list --all-p --name $VM_NAME..."
    openstack server list --all-p --name "^$1$"
  fi
}

wait_for_shutdown() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'wait_for_shutdown'. Exiting..."
    exit 1
  fi

  TIMESTAMP=$(timestamp)
  TIMEOUT=$(( TIMESTAMP + SHUTDOWN_MAX_TIMEOUT ))

  while (( $(timestamp) < TIMEOUT )); do

    VM_INFO=$(get_vm_info "$1")
    VM_STATUS=$( echo "$VM_INFO" | grep -iwF " $1 " | awk '{print $6}' )

   if [[ ${VM_STATUS,,} == 'suspended' ]] || [[ ${VM_STATUS,,} == 'shutoff' ]]; then
      echo "[INFO] $1 has been SHUTDOWN successfully"
      return
    else
      echo "[INFO] VM $1 not off yet. Sleeping for $SHUTDOWN_RETRY_INTERVAL seconds before checking again..."
      sleep "$SHUTDOWN_RETRY_INTERVAL"
    fi
  done

  echo "[ERROR] Could not SHUTDOWN $1 after $SHUTDOWN_MAX_TIMEOUT seconds. Exit"
  exit 1
}

# openrc v3
source_openrc

VM_INFO=$(get_vm_info "$VM_NAME")
VM_ID=$( echo "$VM_INFO" | grep -iwF " $VM_NAME " | awk '{print $2}' )

echo "[INFO] Shuting down $VM_NAME ($VM_ID)..."
openstack server stop "$VM_ID"

wait_for_shutdown "$VM_NAME"

get_vm_info "$VM_NAME"
