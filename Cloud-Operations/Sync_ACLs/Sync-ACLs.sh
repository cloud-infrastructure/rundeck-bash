#!/bin/bash
targets=($(echo $RD_OPTION_TARGET | tr "," "\n"))

for target in "${targets[@]}"; do
    echo "Sync assignments from ${RD_OPTION_SOURCE} to ${target}"
    cci-sync-assignments -vv --domain default --source ${RD_OPTION_SOURCE} --target ${target}
done
