#!/bin/bash

echo "Executing cci-delete-project..."
message="Executed by Rundeck, user: ${RD_JOB_USER_NAME} job: ${RD_JOB_URL}"
cci-delete-project -vv --"${RD_OPTION_EXECUTION_MODE}" --"${RD_OPTION_CHECK_RESOURCES}" --execution-reference "$message" from-input \
--project-name "$RD_OPTION_PROJECT"  \

exit $?