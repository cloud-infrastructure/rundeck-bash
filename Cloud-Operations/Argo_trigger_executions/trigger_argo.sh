#!/bin/bash

DATEQUERY=$(date +"%Y.%m.%d %-H:00")

echo "Executing Job 'Trigger cold migrations in Argo' looking for migrations at ${DATEQUERY}"
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

export KUBECONFIG=/etc/openstack/.novanet/config
GITLAB_API_URL="https://gitlab.cern.ch/api/v4/projects/cloud-infrastructure%2Fcold_migration/repository/files/ThePlan%2Ecsv?ref=master"

curl -s --request GET --header "PRIVATE-TOKEN: $RD_OPTION_GITLAB_TOKEN_ARGO" --header "Content-Type: application/json" "$GITLAB_API_URL" | jq .content -r | base64 -d | grep -w "$DATEQUERY" | cut -d',' -f2 | tr '[:upper:]' '[:lower:]' | xargs -r -n1 argo submit --from clusterworkflowtemplate/vm-migration -l owner=auto --name

exit $?
