#!/bin/bash

echo "Deploying changes in $RD_OPTION_HOSTGROUP machines..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

if [ "$RD_OPTION_PACKAGE" == 'None' ]; then
  COMMAND="puppet agent -tv"
elif [[ "$RD_OPTION_PACKAGE" == 'python3-hzrequestspanel' || "$RD_OPTION_PACKAGE" == 'python3-django-horizon' ]]; then
  COMMAND="if rpm -q $RD_OPTION_PACKAGE > 2&>1 ; then dnf upgrade --refresh $RD_OPTION_PACKAGE -y; puppet agent -tv; echo Restarting httpd service...; systemctl restart httpd; sleep 30; else echo 'Package $RD_OPTION_PACKAGE was not found'; fi"
else
  COMMAND="if rpm -q $RD_OPTION_PACKAGE > 2&>1 ; then dnf upgrade --refresh $RD_OPTION_PACKAGE -y; else echo 'Package $RD_OPTION_PACKAGE was not found'; fi"
fi

HOSTS=$(ai-foreman -g "${RD_OPTION_HOSTGROUP}" --no-header -z Name showhost | sed -e '1d' -e '$d' | cut -d'|' -f2)

for HOST in $HOSTS
do
  echo "Connecting and executing command '$COMMAND' on $HOST..."
  ssh -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 root@"$HOST" "$COMMAND"
  if [[ $? -ne 0 ]]; then
    echo "ERROR: Not able to connect to $HOST"
  fi
done
