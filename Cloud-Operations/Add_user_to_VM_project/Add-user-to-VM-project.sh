#!/bin/bash

VM_NAME=$RD_OPTION_VM_NAME
NEW_PROJECT_ID=$RD_OPTION_NEW_PROJECT_ID

SERVICE_ACCOUNT=$RD_OPTION_USERNAME

source_openrc() {
  echo "[INFO] Unsetting all 'OS_' environment variables..."
  while read -r var; do unset "$var"; done < <(env | grep OS_ | cut -d= -f 1)

  echo "[INFO] Exporting default v3kerberos variables..."
  export OS_PROJECT_NAME=admin
  export OS_REGION_NAME=cern
  export OS_PROJECT_DOMAIN_ID=default
  export OS_IDENTITY_API_VERSION=3
  export OS_AUTH_TYPE=v3fedkerb
  export OS_IDENTITY_PROVIDER=sssd
  export OS_PROTOCOL=kerberos
  export OS_AUTH_URL=https://keystone.cern.ch/v3
}

get_vm_info() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one VM name to 'get_vm_info'. Exiting..."
    exit 1
  else
    openstack server list --all-p --name "^$1$"
  fi
}

# 1) Add service account as project member

# openrc as admin
source_openrc

VM_INFO=$(get_vm_info "$VM_NAME")
if [ -n "$VM_INFO" ]; then
  VM_ID=$( echo "$VM_INFO" | grep -iwF " $VM_NAME " | awk '{print $2}' )
  echo "[INFO] Retrieving info from $VM_ID"
  VM_DETAILED_INFO=$( openstack server show "$VM_ID")
  PROJECT_ID=$( echo "$VM_DETAILED_INFO" | grep project_id | awk '{print $4}')

  echo "[INFO] Adding user $SERVICE_ACCOUNT as member of project $PROJECT_ID..."
  openstack role add --project "$PROJECT_ID" --user "$SERVICE_ACCOUNT" member
  # Add new project id
  if [ -n "$NEW_PROJECT_ID" ]; then
    echo "[INFO] Adding user $SERVICE_ACCOUNT as member of project $NEW_PROJECT_ID..."
    openstack role add --project "$NEW_PROJECT_ID" --user "$SERVICE_ACCOUNT" member
  fi
else
  echo "[ERROR] Cannot find VM $VM_NAME. Exiting..."
  exit 1
fi
