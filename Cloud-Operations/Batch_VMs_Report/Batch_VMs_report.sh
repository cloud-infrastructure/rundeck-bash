#!/bin/bash

# source the openrc
echo "[INFO] Exporting env variables from $RD_OPTION_OPENRC..."
source "$RD_OPTION_OPENRC"

echo "[INFO] Executing embedded perl script..."
OUTPUT="/usr/bin/perl -x $0"

# shellcheck disable=SC2217
echo -n << '__END__' > /dev/null 2>&1
#!/usr/bin/perl -wl

package OpenStack::Project;

sub List(;$){
    my $filter = shift @_;
    my %project = ();
    if (open(KS, "/usr/bin/openstack project list --domain default --column Name --column ID 2>/dev/null |")){
        while(<KS>){
            next if /^\+/;
            s/\s*\|\s*/\|/g;
            my (undef,$id,$name) = split(/\|/,$_);
            next if $id eq "ID"; 
            #print "$id \"$name\" $domain_id - $description ($enabled)\n";

            if (not defined $filter){
            $project{$id} = $name;
            next;
            }           

            if ($name =~ /$filter/){
            $project{$id} = $name;
            next;
            }

            if ($id =~ /$filter/){
            $project{$id} = $name;
            next;
            }

    }
    close(KS);
}
return %project;
}

package OpenStack::Nova;

sub QuotaShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack quota show $opt";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Quota";
	$list{$key} = $value;
    }
    close(F);

    return %list;
}

sub AbsoluteLimits($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack limits show --absolute --project $opt";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Name";
	$list{$key} = $value;
    }
    close(F);

    return %list;
}

sub HypervisorShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack hypervisor show $opt";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Name";
	$list{$key} = $value;
    }
    close(F);

    return %list;
}

sub FlavorList(){
    my $cmd = "/usr/bin/openstack flavor list --all";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }
    my %list = ();
    my $instances = my $cores = my $ram = undef;
    while(<F>){
        next if /^\+/;
	s/\s+//g;
        my ($id,$name,$ram,$disk,$ephemeral,$swap,$cores) = (split(/\|/,$_))[1,2,3,4,5,6,7,8];
        next if $name =~  /^Name/;
        %{$list{$name}} =
            (id        => $id,
	     ram       => $ram,
             disk      => $disk,
             ephemeral => ($ephemeral || 0),
             swap      => ($swap || 0),
             cores     => $cores,
            );

        #print "$name,$ram,$disk,$ephemeral,$swap,$cores\n";exit;
    }
    close(F);
    return %list;
}

sub CellList(){
    my %cell = ();
    my $cmd = "/usr/bin/openstack compute service list --service nova-cells";
    if (not open(NOVA,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    while(<NOVA>){
	next if /^\+/;
	s/\s+//g;
	my ($service,$host,$status,$state) = (split(/\|/,$_))[2,3,5,6];
	next if not defined $service;
	next if $host eq "Host";	# skip header
	if ($service ne "nova-cells"){ # older version of phython-novaclient
	    ($host,$status,$state) = (split(/\|/,$_))[2,4,5];
	    next if $host eq "Host";	# skip header
	}
	next if $status ne "enabled";
	next if $state  ne "up";
	next if $host !~ /^father\!(cell\d+)@/;
	$cell{$1}++;
    }
    close(NOVA);

    return sort keys %cell;
}


sub BEGIN {
    use Cwd 'abs_path';
    use File::Basename 'dirname';
    push(@INC,dirname(abs_path($0)));
}

use strict;
use diagnostics;
use Data::Dumper;
use Getopt::Long;
#use OpenStack::Nova;
#use OpenStack::Project;

#
# Check environment
#
if (not exists $ENV{OS_USERNAME}){
    print STDERR "[ERROR] No environment variable \"\$OS_USERNAME\" found\n";
    exit 1;
}

my $debug = my $verbose = 0;
my %opts = (
    debug    => \$debug,
    verbose  => \$verbose,
    );
#
# Parse the options
#
my $rc = Getopt::Long::GetOptions(
    \%opts,
    "debug","verbose",
    );

#
# Get flavor data
#
print "[VERB] Get all flavors...\n" if $verbose;
my %flavor = OpenStack::Nova::FlavorList();
map {$flavor{$_}{ram} /= 1024} keys %flavor;
map {%{$flavor{$flavor{$_}{id}}} = %{$flavor{$_}}} keys %flavor;
if (scalar(keys %flavor) == 0){
    print STDERR "[ERROR] No Nova flavors found.\n";
    exit 1;
}
print "[VERB] Found ".scalar(keys %flavor)." Nova flavors\n" if $verbose;

print "[DEBUG] " . Dumper(\%flavor) if $debug;

#
# project names
#
print "[VERB] Get all projects...\n" if $verbose;
my $filter = "^IT-Batch"; # XXX
my %project = OpenStack::Project::List($filter);

%project = OpenStack::Project::List($filter) if scalar(keys %project) == 0; # retry (just in case)

if (scalar(keys %project) == 0){
    print STDERR "[ERROR] No Keystone projects found.\n";
    exit 1;
}
print "[VERB] Found ".scalar(keys %project)." Keystone projects\n" if $verbose;

print "[DEBUG] " . Dumper(\%project) if $debug;

for my $tenant_id (sort keys %project){
    my $cmd = "/usr/bin/nova list --all-tenants 1 --tenant $tenant_id --fields name,user_id,flavor,host";
    print "[D] Project $project{$tenant_id} - getting using \"$cmd\"\n" if $debug; 

    open(NOVA,"$cmd 2>/dev/null |") or die "aargh";
    while(<NOVA>){
	next unless my ($id,$vm,$owner,$flavor_id,$host) = /^\|\s+(\S+)\s+\|\s+(.*?)\s+\|\s+(\S+)\s+\|\s+(\d+)\s+\|\s+(\S+)\s+\|\s*$/;
	next if $id eq "id"; 
	print join(",",$vm,$flavor{$flavor_id}{cores},$flavor{$flavor_id}{ram},$project{$tenant_id},$owner,$host);
    }
    close(NOVA);
}

__END__


if [ -n "$OUTPUT" ]; then
    aklog 
    DATE=$(date +%Y%m%d)
    echo Saving file in "$RD_OPTION_OUTPUT-$DATE"
    echo "$OUTPUT" > "$RD_OPTION_OUTPUT-$DATE"
    # shellcheck disable=SC2320
    if [ $? -eq 0 ]; then
      echo "[INFO] Output saved"
    else
      echo "[ERROR] Impossible to save output in $RD_OPTION_OUTPUT"
      exit 1
    fi
else
  echo "[ERROR] Perl script returned empty output. Please check logs"
  exit 1
fi
