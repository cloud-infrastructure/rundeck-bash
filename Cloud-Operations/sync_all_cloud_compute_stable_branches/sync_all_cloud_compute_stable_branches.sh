#!/bin/bash

OUTPUT='/var/lib/rundeck/cloud_compute_staging'
OUTPUT_HOSTGROUP='/var/lib/rundeck/cloud_compute_staging/it-puppet-hostgroup-cloud_compute'
OUTPUT_MODULE='/var/lib/rundeck/cloud_compute_staging/it-puppet-module-nova'
GIT_REPO_HOSTGROUP='https://svcrdeck@gitlab.cern.ch:8443/ai/it-puppet-hostgroup-cloud_compute.git'
GIT_REPO_MODULE='https://svcrdeck@gitlab.cern.ch:8443/ai/it-puppet-module-nova.git'

exit_on_err() {
	if [[ $1 != 0 ]]; then
		if [[ -n $2 ]]; then
			echo "$2"
		fi
		echo "ERROR - stop job"
		exit "$1"
	fi
}

git_merge() {
    echo "Merging: $2 into $1"
    git checkout -q "$1"
    exit_on_err $?
    git merge "$2"
    exit_on_err $?
    echo "Push: $1"
    git push -q origin "$1"
    exit_on_err $?
}

clean_output() {
    if [ -d "$1" ]; then
        echo "There is already a local copy if the repo. Removing $1..."
        rm -rf "$1"
        exit_on_err $?
    fi

    #create output directory
    mkdir -p "$1"
    exit_on_err $?
}

repo_clone() {
    #clone the repo
    echo "Cloning: $1"
    git clone -q "$1" "$2"
    exit_on_err $?
    cd "$2" || exit
    exit_on_err $?
}

merge_branches() {
    #merge branches
    echo "Checkout branch master"
    git checkout -q master
    exit_on_err $?
    echo "Checkout branch master_1"
    git checkout -q master_1
    exit_on_err $?
    echo "Checkout branch master_2"
    git checkout -q master_2
    exit_on_err $?
    echo "Checkout branch master_3"
    git checkout -q master_3
    exit_on_err $?
    git_merge 'master_3' 'master'
    git_merge 'master_2' 'master'
    git_merge 'master_1' 'master'
}

remove_output() {
    cd "$OUTPUT" || exit
    echo "Remove: $1"
    rm -rf "$1"
    exit_on_err $?
}

kinit -kt /etc/rundeck/svcrdeck.keytab svcrdeck@CERN.CH

clean_output "$OUTPUT_HOSTGROUP"
repo_clone "$GIT_REPO_HOSTGROUP" "$OUTPUT_HOSTGROUP"
merge_branches
remove_output "$OUTPUT_HOSTGROUP"

clean_output "$OUTPUT_MODULE"
repo_clone "$GIT_REPO_MODULE" "$OUTPUT_MODULE"
merge_branches
remove_output "$OUTPUT_MODULE"
