#!/bin/bash

if [[ $RD_OPTION_SYSPREP == 'no' ]]; then
  echo "[INFO] This step is only needed when sysprepping the instance. Moving on!"
  exit 0
fi

VM_NAME=$RD_OPTION_VM_NAME

SNAPSHOT_MAX_TIMEOUT=$RD_OPTION_SNAPSHOT_TIMEOUT #10h
SNAPSHOT_RETRY_INTERVAL=$RD_OPTION_SNAPSHOT_RETRY_INTERVAL

if [[ "$RD_OPTION_SUFFIX" ]]; then
  SUFFIX=$RD_OPTION_SUFFIX
else
  echo "[INFO] Image suffix name not specified. Using \$RD_JOB_EXECID..."
  SUFFIX=$RD_JOB_EXECID
fi

source_openrc() {
  echo "[INFO] Unsetting all 'OS_' environment variables..."
  while read -r var; do unset "$var"; done < <(env | grep OS_ | cut -d= -f 1)

  echo "[INFO] Exporting default v3kerberos variables..."
  export OS_PROJECT_NAME=admin
  export OS_REGION_NAME=cern
  export OS_PROJECT_DOMAIN_ID=default
  export OS_IDENTITY_API_VERSION=3
  export OS_AUTH_TYPE=v3fedkerb
  export OS_IDENTITY_PROVIDER=sssd
  export OS_PROTOCOL=kerberos
  export OS_AUTH_URL=https://keystone.cern.ch/v3
}

timestamp() {
  date +%s
}

wait_for_instance_snapshot() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one SNAPSHOT name to 'wait_for_instance_snapshot'. Exiting..."
    exit 1
  fi

  TIMESTAMP=$(timestamp)
  TIMEOUT=$((TIMESTAMP + SNAPSHOT_MAX_TIMEOUT ))

  while (( $(timestamp) < TIMEOUT )); do

    SNAPSHOT_STATUS=$( openstack image show "$1" -f value -c status )

    if [[ ${SNAPSHOT_STATUS,,} == 'active' ]]; then
      echo "[INFO] $1 has been created successfully"
      return
    else
      echo "[INFO] Instance snapshot $1 not ACTIVE yet (current: $SNAPSHOT_STATUS). Sleeping for $SNAPSHOT_RETRY_INTERVAL seconds before checking again..."
      sleep "$SNAPSHOT_RETRY_INTERVAL"
    fi
  done

  echo "[ERROR] Instance snapshot $1 not ACTIVE after $SNAPSHOT_MAX_TIMEOUT seconds. Exit"
  exit 1
}

wait_for_volume_snapshot() {
  if [ -z "$1" ]; then
    echo "[ERROR] Need to provide one SNAPSHOT name to 'wait_for_volume_snapshot'. Exiting..."
    exit 1
  fi

  TIMESTAMP=$(timestamp)
  TIMEOUT=$((TIMESTAMP + SNAPSHOT_MAX_TIMEOUT ))

  while (( $(timestamp) < TIMEOUT )); do

    SNAPSHOT_STATUS=$( openstack volume snapshot show "$1" -f value -c status )

    if [[ ${SNAPSHOT_STATUS,,} == 'available' ]]; then
      echo "[INFO] $1 has been created successfully"
      return
    else
      echo "[INFO] Volume snapshot $1 not AVAILABLE yet (current: $SNAPSHOT_STATUS). Sleeping for $SNAPSHOT_RETRY_INTERVAL seconds before checking again..."
      sleep "$SNAPSHOT_RETRY_INTERVAL"
    fi
  done

  echo "[ERROR] Volume snapshot $1 not AVAILABLE after $SNAPSHOT_MAX_TIMEOUT seconds. Exit"
  exit 1
}

# openrc as admin
source_openrc

VM_ID=$(openstack server list --all-p --no-name --name "^${VM_NAME}$" -n -c ID -f value)
VM_DETAILED_INFO=$( openstack server show "$VM_ID" -f yaml)
PROJECT_ID=$( echo "$VM_DETAILED_INFO" | grep project_id | cut -d ':' -f2 | sed -e 's/^[[:space:]]*//')
IMAGE_ID=$( echo "$VM_DETAILED_INFO" | grep image | cut -d ':' -f2 | sed -e 's/^[[:space:]]*//')

unset OS_PROJECT_NAME
export OS_PROJECT_ID=$PROJECT_ID

if [[ $RD_OPTION_SYSPREP_SNAPSHOT == 'yes' ]]; then
  SNAPSHOT_NAME="${VM_NAME,,}_base_$SUFFIX"
  if [[ $IMAGE_ID == "''" ]]; then
    echo "[INFO] Creating a volume snapshot for sysprep ..."
    VOLUME_ID=$(openstack volume list -c ID -c 'Attached to' | grep "$VM_NAME" | grep '/dev/vda' | cut -d'|' -f2 | sed -e 's/^[[:space:]]*//')
    openstack volume snapshot create --volume "$VOLUME_ID" --force "$SNAPSHOT_NAME"
    wait_for_volume_snapshot "$SNAPSHOT_NAME"
  else
    echo "[INFO] Creating an instance snapshot for sysprep ..."
    openstack server image create --name "$SNAPSHOT_NAME" "$VM_ID"
    wait_for_instance_snapshot "$SNAPSHOT_NAME"
  fi
else
  echo "[INFO] Sysprep snapshot not required, skipping..."
fi

# Start the sysprep operation
echo "[INFO] Starting sysprep operation"
source_openrc

VM_SERVER=$(openstack server list --all --name "^$VM_NAME$" --long --no-name-lookup -c Host -f value)
VM_DOMAIN=$(ssh -o StrictHostKeyChecking=no root@"$VM_SERVER" virsh list --all --uuid --name | grep "$VM_ID" | cut -d ' ' -f2)

#Fix for centos8 machines
FILE=/tmp/appliance-1.40.1.tar.xz
if [ ! -f "$FILE" ]; then
    echo "$FILE does not exist. recreating it"
    wget -P /tmp/ http://download.libguestfs.org/binaries/appliance/appliance-1.40.1.tar.xz
fi
scp -o StrictHostKeyChecking=no "$FILE" root@"$VM_SERVER":/tmp
ssh -o StrictHostKeyChecking=no root@"$VM_SERVER" tar xvfJ "$FILE" -C /tmp/

ssh -o StrictHostKeyChecking=no root@"$VM_SERVER" LIBGUESTFS_PATH=/tmp/appliance/ virt-sysprep "$RD_OPTION_SYSPREP_PARAMS" -d "$VM_DOMAIN"
