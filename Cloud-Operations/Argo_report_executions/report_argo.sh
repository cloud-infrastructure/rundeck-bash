#!/bin/bash

echo "Executing Job 'Report cold migrations on Argo'..."
echo "Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"

export KUBECONFIG=/etc/openstack/.novanet/config

cat >mail.txt <<EOF
Hello Cloud admin,

As part of cold migration campaign we are informing you about the status of the migration.
This is the current status for the jobs in the last day

$(argo list --since 1d)

Kind regards,
CERN Cloud Infrastructure Team
EOF

mail -s '[Cloud Cold Migration] Report of workflows triggered during last day' -r "noreply@cern.ch" "cloud-infrastructure-migration-campaign@cern.ch" < mail.txt

exit $?
