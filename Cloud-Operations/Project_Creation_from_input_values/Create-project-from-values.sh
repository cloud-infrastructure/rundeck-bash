#!/bin/bash

echo "Executing cci-create-project..."
message="Executed by Rundeck, user: $RD_JOB_USER_NAME job: $RD_JOB_URL"
cci-create-project -vv --"$RD_OPTION_EXECUTION_MODE" \
	--execution-reference "$message" from-input \
	--project-name "$RD_OPTION_PROJECT" \
	--description "$RD_OPTION_DESCRIPTION" \
	--owner "$RD_OPTION_OWNER" \
	--egroup "$RD_OPTION_EGROUP" \
	--cores "$RD_OPTION_CORES" \
	--instances "$RD_OPTION_INSTANCES" \
	--ram "$RD_OPTION_RAM" \
	--chargegroup "$RD_OPTION_CHARGEGROUP" \
	--chargerole "$RD_OPTION_CHARGEROLE" \
	--project-type "$RD_OPTION_PROJECT_TYPE" \
	--cp1-gigabytes "$RD_OPTION_CP1_GIGABYTES" \
	--cp1-volumes "$RD_OPTION_CP1_VOLUMES" \
        --cpio1-gigabytes "$RD_OPTION_CPIO1_GIGABYTES" \
        --cpio1-volumes "$RD_OPTION_CPIO1_VOLUMES" \
        --io1-gigabytes "$RD_OPTION_IO1_GIGABYTES" \
        --io1-volumes "$RD_OPTION_IO1_VOLUMES" \
	--standard-gigabytes "$RD_OPTION_STANDARD_GIGABYTES" \
	--standard-volumes "$RD_OPTION_STANDARD_VOLUMES" \
	--vault-100-gigabytes "$RD_OPTION_VAULT_100_GIGABYTES" \
	--vault-100-volumes "$RD_OPTION_VAULT_100_VOLUMES" \
	--vault-500-gigabytes "$RD_OPTION_VAULT_500_GIGABYTES" \
	--vault-500-volumes "$RD_OPTION_VAULT_500_VOLUMES" \
        --meyrin-shares "$RD_OPTION_MEYRIN_SHARES" \
        --meyrin-gigabytes "$RD_OPTION_MEYRIN_GIGABYTES" \
        --geneva_testing-shares "$RD_OPTION_GENEVA_TESTING_SHARES" \
        --geneva_testing-gigabytes "$RD_OPTION_GENEVA_TESTING_GIGABYTES" \
	--s3-buckets "$RD_OPTION_S3_BUCKETS" \
	--s3-gigabytes "$RD_OPTION_S3_GIGABYTES" \
	--loadbalancer "$RD_OPTION_LOADBALANCER" \
	--floatingip "$RD_OPTION_FLOATING_IP" \
	--cell-mapping "$RD_OPTION_CELL_MAPPING" \
	--flavor-mapping "$RD_OPTION_FLAVOR_MAPPING"
exit $?
