#!/bin/bash

echo "Executing cci-health-report-puppet..."
cci-health-report-puppet --threshold "$RD_OPTION_PUPPET_THRESHOLD" \
--instance "$RD_OPTION_INSTANCE" --mail-to "$RD_OPTION_MAIL_TO"
exit $?