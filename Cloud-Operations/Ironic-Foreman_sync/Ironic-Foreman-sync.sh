#!/bin/bash
if [ "${RD_OPTION_BEHAVIOUR}" == 'perform' ]; then
  cci-foreman-credentials-sync
else
  cci-foreman-credentials-sync --dryrun
fi